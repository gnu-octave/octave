<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt_LT">
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/files-dock-widget.cc" line="+141"/>
        <source>Invalid filename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.cc" line="+1045"/>
        <source>Difference to the default size</source>
        <translation>Skirtumas nuo numatyto dydžio</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Background color, magenta (255, 0, 255) means default</source>
        <translation>Fono spalva; šviesi alyvinė spalva (255,0,255) nurodo, kad naudoti numatytąją spalvą</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>b</source>
        <comment>short form for bold</comment>
        <translation type="unfinished">r</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>i</source>
        <comment>short form for italic</comment>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>u</source>
        <comment>short form for underlined</comment>
        <translation type="unfinished">b</translation>
    </message>
</context>
<context>
    <name>QTerminal</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/QTerminal.cc" line="+119"/>
        <source>Edit &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help on &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Documentation on &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+165"/>
        <source>Copy</source>
        <translation>Kopijuoti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste</source>
        <translation>Įklijuoti</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>Pažymėti viską</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run Selection</source>
        <translation type="unfinished">Vykdyti pažymėtą kodą</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help on selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Documentation on selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Clear Window</source>
        <translation>Išvalyti langą</translation>
    </message>
    <message>
        <location line="-233"/>
        <source>Edit %1 at line %2</source>
        <translation>%2 eilutėje tvarkyti %1</translation>
    </message>
</context>
<context>
    <name>QWinTerminalImpl</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/win32/QWinTerminalImpl.cpp" line="+1859"/>
        <source>copied selection to clipboard</source>
        <translation>pažymėta sritis nukopijuota į atminties mainų sritį (clipboard)</translation>
    </message>
</context>
<context>
    <name>QsciLexerBash</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerbash.cpp" line="+203"/>
        <source>Default</source>
        <translation>Numatytasis teksas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error</source>
        <translation>Klaida</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Skaičius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Raktinis žodis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>&quot;&quot; eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>&apos;&apos; eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Matematinis operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Indentifikatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scalar</source>
        <translation>Skaliaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Parameter expansion</source>
        <translation>Rodiklių išplėtimas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks</source>
        <translation>Atvirkščių kabučių eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Here document delimiter</source>
        <translation>HEREDOC tipo eilutės aprašymo pradžios žymeklis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted here document</source>
        <translation>HEREDOC tipo eilutė</translation>
    </message>
</context>
<context>
    <name>QsciLexerBatch</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerbatch.cpp" line="+174"/>
        <source>Default</source>
        <translation>Numatytasis teksas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Raktinis žodis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>&quot;goto&quot; žymė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide command character</source>
        <translation>Ženklas, nurodantis neišvesti komandų į ekraną</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>External command</source>
        <translation>Išorinė komanda</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable</source>
        <translation>Kintamasis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Matematinis operatorius</translation>
    </message>
</context>
<context>
    <name>QsciLexerCPP</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexercpp.cpp" line="+364"/>
        <source>Default</source>
        <translation>Numatytasis tekstas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive default</source>
        <translation>Numatytasis tekstas, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C comment</source>
        <translation>/* */ komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C comment</source>
        <translation>/* */ komentaras, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C++ comment</source>
        <translation>// komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C++ comment</source>
        <translation>// komentaras esantis, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style C comment</source>
        <translation>/** */ (JavaDoc stiliaus) komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style C comment</source>
        <translation>/** */ (JavaDoc stiliaus) komentaras, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Skaičius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive number</source>
        <translation>Skaičius esantis, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Raktinis žodis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive keyword</source>
        <translation>Raktinis žodis, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>&quot;&quot; eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive double-quoted string</source>
        <translation>&quot;&quot; eilutė, esanti # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>&apos;&apos; eilutė kabučių</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive single-quoted string</source>
        <translation>&apos;&apos; eilutė, esanti # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>IDL UUID</source>
        <translation>IDL UUID</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive IDL UUID</source>
        <translation>IDL UUID, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pre-processor block</source>
        <translation># sakinys (prasideda &quot;#&quot; ženklu)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive pre-processor block</source>
        <translation># sakinys, esanti kitame # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Matematinis operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive operator</source>
        <translation>Matematinis operatorius, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Indentifikatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive identifier</source>
        <translation>Indentifikatorius, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unclosed string</source>
        <translation>Eilutė neturinti &quot;uždarančios&quot; kabutės</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive unclosed string</source>
        <translation>&quot;Uždarančios&quot; kabutės neturinti eilutė, esanti # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C# verbatim string</source>
        <translation>C# @&quot;&quot; tipo eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C# verbatim string</source>
        <translation>C# @&quot;&quot; tipo eilutė, esanti # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaScript regular expression</source>
        <translation>JavaScript reguliarioji išraiška</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaScript regular expression</source>
        <translation>JavaScript reguliarioji išraiška, esanti # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style C++ comment</source>
        <translation>/// (JavaDoc stiliaus) komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style C++ comment</source>
        <translation>/// (JavaDoc stiliaus) komentaras, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Secondary keywords and identifiers</source>
        <translation>Antriniai raktiniai žodžiai ir indentifikatoriai</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive secondary keywords and identifiers</source>
        <translation>Antriniai raktiniai žodžiai ir indentifikatoriai, esantys # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc keyword</source>
        <translation>JavaDoc raktinis žodis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc keyword</source>
        <translation>JavaDoc raktinis žodis, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc keyword error</source>
        <translation>Neteisingas JavaDoc raktinis žodis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc keyword error</source>
        <translation>Neteisingas JavaDoc raktinis žodis, esantis # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Global classes and typedefs</source>
        <translation>Visuotinos klasės ir tipų apibėžtys</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive global classes and typedefs</source>
        <translation>Visuotinos klasės ir tipų apibėžtys, esančios # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C++ raw string</source>
        <translation>C++ R&quot;&quot; tipo eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C++ raw string</source>
        <translation>C++ R&quot;&quot; tipo eilutė, esanti # sakinyje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vala triple-quoted verbatim string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive Vala triple-quoted verbatim string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pike hash-quoted string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive Pike hash-quoted string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pre-processor C comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive pre-processor C comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style pre-processor comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style pre-processor comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>User-defined literal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive user-defined literal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Task marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive task marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Escape sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive escape sequence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QsciLexerDiff</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerdiff.cpp" line="+106"/>
        <source>Default</source>
        <translation>Numatytasis teksas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command</source>
        <translation>Komanda</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Header</source>
        <translation>Antraštė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Position</source>
        <translation>Padėtis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed line</source>
        <translation>Panaikinta eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added line</source>
        <translation>Pridėta eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Changed line</source>
        <translation>Pakeista eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added adding patch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed adding patch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added removing patch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed removing patch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QsciLexerMatlab</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexermatlab.cpp" line="+133"/>
        <source>Default</source>
        <translation>Numatytasis teksas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command</source>
        <translation>Komanda</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Skaičius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Raktinis žodis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>&apos;&apos; eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Matematinis operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Kintamojo, komandos ar funkcijos pavadinimas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>&quot;&quot; eilutė</translation>
    </message>
</context>
<context>
    <name>QsciLexerPerl</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerperl.cpp" line="+328"/>
        <source>Default</source>
        <translation>Numatytasis teksas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error</source>
        <translation>Klaida</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Komentaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>POD</source>
        <translation>POD tipo eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Skaičius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Raktinis žodis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>&quot;&quot; eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>&apos;&apos; eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Matematinis operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Kintamojo, funkcijos, klasės ar modulio pavadinimas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scalar</source>
        <translation>Skaliaras</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Array</source>
        <translation>Masyvas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hash</source>
        <translation>Asocijuotas masyvas arba žodynas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol table</source>
        <translation>Ženklų lentelė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Regular expression</source>
        <translation>Reguliarioji išraiška (&quot;m&quot; operatorius)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Substitution</source>
        <translation>Pakeitimas (&quot;s&quot; operatorius)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks</source>
        <translation>`` eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data section</source>
        <translation>Datos sritis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Here document delimiter</source>
        <translation>HEREDOC tipo eilutės aprašymo pradžios žymeklis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted here document</source>
        <translation>&apos;&apos; HEREDOC tipo eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted here document</source>
        <translation>&quot;&quot; HEREDOC tipo eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backtick here document</source>
        <translation>`` HEREDOC tipo eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (q)</source>
        <translation>&quot;q&quot; operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qq)</source>
        <translation>&quot;qq&quot; operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qx)</source>
        <translation>&quot;qx&quot; operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qr)</source>
        <translation>&quot;qr&quot; operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qw)</source>
        <translation>&quot;qw&quot; operatorius</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>POD verbatim</source>
        <translation>&quot;POD verbatim&quot; tipo eilutė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Subroutine prototype</source>
        <translation>Paprogramės prototipas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Format identifier</source>
        <translation>Formato pavadinimas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Format body</source>
        <translation>Formato aprašas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string (interpolated variable)</source>
        <translation>Kintamasis dvigubų kabučių eilutėje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Translation</source>
        <translation>Vertimas (&quot;tr&quot; operatorius)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Regular expression (interpolated variable)</source>
        <translation>Kintamasis reguliarioje išraiškoje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Substitution (interpolated variable)</source>
        <translation>Kintamasis &quot;s&quot; operatoriuje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks (interpolated variable)</source>
        <translation>Kintamasis `` eilutėje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted here document (interpolated variable)</source>
        <translation>Kintamsis &quot;&quot; HEREDOC tipo eilutėje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backtick here document (interpolated variable)</source>
        <translation>Kintamsis `` HEREDOC tipo eilutėje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qq, interpolated variable)</source>
        <translation>Kintamasis &quot;qq&quot; operatoriuje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qx, interpolated variable)</source>
        <translation>Kintamasis &quot;qx&quot; operatoriuje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qr, interpolated variable)</source>
        <translation>Kintamasis &quot;qr&quot; operatoriuje</translation>
    </message>
</context>
<context>
    <name>QsciScintilla</name>
    <message>
        <location filename="build_ts/octave-qsci/qsciscintilla.cpp" line="+4478"/>
        <source>&amp;Undo</source>
        <translation>At&amp;šaukti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>At&amp;statyti</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cu&amp;t</source>
        <translation>Iškirp&amp;ti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopijuoti</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>Įk&amp;lijuoti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Pašalinti</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>Pažymėti viską</translation>
    </message>
</context>
<context>
    <name>UrlFilter</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/unix/Filter.cpp" line="+630"/>
        <source>Open Link</source>
        <translation>Atidaryti nuorodą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Link Address</source>
        <translation>Kopijuoti nuorodos adresą</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Send Email To...</source>
        <translation>Siųsti e-paštą...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Email Address</source>
        <translation>Kopijuoti e-pašto adresą</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Edit %1 at line %2</source>
        <translation>%2 eilutėje tvarkyti %1</translation>
    </message>
</context>
<context>
    <name>annotation_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/graphics/annotation-dialog.ui" line="+17"/>
        <source>Annotation</source>
        <translation>Apibūdinimas</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Text</source>
        <translation>Tekstas</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>String</source>
        <translation>Ženklų eilutė</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Font</source>
        <translation>Šriftas</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>bold</source>
        <translation>paryškintas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>b</source>
        <translation>r</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>italic</source>
        <translation>pasviręs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>i</source>
        <translation>s</translation>
    </message>
    <message>
        <location line="+428"/>
        <source>color</source>
        <translation>spalva</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>left</source>
        <translation>kairėje</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+29"/>
        <source>middle</source>
        <translation>viduryje</translation>
    </message>
    <message>
        <location line="-24"/>
        <source>right</source>
        <translation>dešinėje</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Horizontal alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Vertical alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>top</source>
        <translation>viršuje</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>bottom</source>
        <translation>apačioje</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Position</source>
        <translation>Padėtis</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Width</source>
        <translation>Plotis</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Height</source>
        <translation>Aukštis</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>units</source>
        <translation>vienetai</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>normalized</source>
        <translation>normalizuoti</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Fit to box</source>
        <translation>Numatytasis teksto rėmelio dydis</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Box</source>
        <translation>Rėmelis</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Background</source>
        <translation>Fono spalva</translation>
    </message>
    <message>
        <location line="+444"/>
        <source>Edge</source>
        <translation>Linijų / kraštinių spalva</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Line style</source>
        <translation>Linijų tipas</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>none</source>
        <translation>joks</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Line width</source>
        <translation>Linijų storis</translation>
    </message>
</context>
<context>
    <name>octave::ListDialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/dialog.cc" line="+454"/>
        <source>Select All</source>
        <translation type="unfinished">Pažymėti viską</translation>
    </message>
</context>
<context>
    <name>octave::command_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/command-widget.cc" line="+76"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation type="unfinished">Sustoti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Continue</source>
        <translation type="unfinished">Tęsti scenarijau vykdymą</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Command Widget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::community_news</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/community-news.cc" line="+70"/>
        <source>Octave Community News</source>
        <translation type="unfinished">Octave bendruomenės naujienos</translation>
    </message>
</context>
<context>
    <name>octave::console_lexer</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/console-lexer.cc" line="+48"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation type="unfinished">Klaida</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prompt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::documentation</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation.cc" line="+135"/>
        <location line="+14"/>
        <location line="+715"/>
        <source>Octave Documentation</source>
        <translation type="unfinished">Octave dokumentacija</translation>
    </message>
    <message>
        <location line="-728"/>
        <source>Could not copy help collection to temporary
file. Search capabilities may be affected.
%1</source>
        <translation type="unfinished">Nepavyko nukopijuoti pagalbos rinkinio į laikinąją
bylą. Paieškos galimybės gali būti įtakotos.
%1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Could not setup the data required for the
documentation viewer. Maybe the Qt SQlite
module is missing?
Only help text in the Command Window will
be available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Contents</source>
        <translation type="unfinished">Turinys</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter text to search function index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+51"/>
        <source>Search</source>
        <translation type="unfinished">Paieška</translation>
    </message>
    <message>
        <location line="-35"/>
        <source>Function Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Go home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Go back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Previous pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Go forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Next pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Find</source>
        <translation type="unfinished">Ieškoti</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Zoom In</source>
        <translation type="unfinished">Padidinti</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom Out</source>
        <translation type="unfinished">Sumažinti</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom Original</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Bookmark current page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+424"/>
        <source>Unable to register help file %1.</source>
        <translation type="unfinished">Nepavyksta užregistruoti pagalbos failo &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>octave::documentation_bookmarks</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation-bookmarks.cc" line="+102"/>
        <source>
No documentation bookmarks loaded!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Octave: Loading Documentation Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter text to search the bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Filter</source>
        <translation type="unfinished">Atranka</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+104"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remo&amp;ve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Add Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Hide &amp;Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show &amp;Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+66"/>
        <source>Octave: Saving Documentation Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to write file %1:
%2.

Documentation bookmarks are not saved!
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Unable to read file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No start element found in %1.
Invalid bookmark file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The file
%1
is not a valid XBEL file version 1.0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Unknown title</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::documentation_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation-dock-widget.cc" line="+40"/>
        <source>Documentation</source>
        <translation type="unfinished">Dokumentacija</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>See the documentation for help.</source>
        <translation type="unfinished">Žiūrėkite dokumentaciją, kai reikia pagalbos.</translation>
    </message>
</context>
<context>
    <name>octave::dw_main_window</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/dw-main-window.cc" line="+53"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Uždaryti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close &amp;All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close &amp;Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Switch to &amp;Left Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Switch to &amp;Right Widget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::external_editor_interface</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/external-editor-interface.cc" line="+68"/>
        <location line="+50"/>
        <source>Octave Editor</source>
        <translation type="unfinished">Octave tvarkyklė</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Could not start custom file editor
%1</source>
        <translation type="unfinished">Nepavyko paleisti vartotojo bylų tvarkyklės
&quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>There is no custom editor configured yet.
Do you want to open the preferences?</source>
        <translation type="unfinished">Dar nesutvarkyti vartotojo tvarkyklės nustatymai.
Ar norite atidaryti nustatymų langą?</translation>
    </message>
</context>
<context>
    <name>octave::file_editor</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/file-editor.cc" line="+194"/>
        <source>Continue</source>
        <translation type="unfinished">Tęsti scenarijau vykdymą</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Save File and Run</source>
        <translation type="unfinished">Išsaugoti bylą ir vykdyti</translation>
    </message>
    <message>
        <location line="+1002"/>
        <location line="+29"/>
        <location line="+565"/>
        <location line="+18"/>
        <location line="+25"/>
        <source>Octave Editor</source>
        <translation type="unfinished">Octave tvarkyklė</translation>
    </message>
    <message>
        <location line="-636"/>
        <source>File not saved! A file with the selected name
%1
is already open in the editor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The associated file editor tab has disappeared.</source>
        <translation type="unfinished">Dingo susijusių bylų tvarkyklės kortelė.</translation>
    </message>
    <message>
        <location line="+565"/>
        <source>Could not open file
%1
for reading: %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>File
%1
does not exist. Do you want to create it?</source>
        <translation type="unfinished">Nėra &quot;%1&quot;
bylos. Ar norite tokią sukurti?</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Create</source>
        <translation type="unfinished">Sukurti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation type="unfinished">Atšaukti</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not open file
%1
for writing: %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+271"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Byla</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Recent Editor Files</source>
        <translation type="unfinished">&amp;Paskutinės tvarkytos bylos</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Edit Function</source>
        <translation type="unfinished">&amp;Tvarkyti funkciją</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Save File</source>
        <translation type="unfinished">Iš&amp;saugoti bylą</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save File &amp;As...</source>
        <translation type="unfinished">Išs&amp;augoti bylą kaip...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Uždaryti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close All</source>
        <translation type="unfinished">Uždaryti viską</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close Other Files</source>
        <translation type="unfinished">Uždaryti kitas bylas</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Print...</source>
        <translation type="unfinished">Spausdinti...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">&amp;Tvarkyti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation type="unfinished">At&amp;statyti</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished">Iškirp&amp;ti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Find and Replace...</source>
        <translation type="unfinished">&amp;Rasti ir pakeisti...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find &amp;Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Find &amp;Previous</source>
        <translation type="unfinished">Surasti &amp;ankstesnį</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Commands</source>
        <translation type="unfinished">&amp;Komandos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy Line</source>
        <translation type="unfinished">Kopijuoti eilutę</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cut Line</source>
        <translation type="unfinished">Iškirpti eilutę</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete to Start of Word</source>
        <translation type="unfinished">Trinti iki žodžio pradžios</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to End of Word</source>
        <translation type="unfinished">Trinti iki žodžio pabaigos</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to Start of Line</source>
        <translation type="unfinished">Trinti iki eilutės pradžios</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to End of Line</source>
        <translation type="unfinished">Trinti iki eilutės pabaigos</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Duplicate Selection/Line</source>
        <translation type="unfinished">Dvigubinti pažymėtą sritį/eilutę</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transpose Line</source>
        <translation type="unfinished">Sukeisti eilutes</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Show Completion List</source>
        <translation type="unfinished">Rodyti automatinio užbaigimo &amp;sąrašą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Format</source>
        <translation type="unfinished">&amp;Formatas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Uppercase Selection</source>
        <translation type="unfinished">Pažymėtos srities raides pakeisti &amp;didžiosiomis raidėmis</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Lowercase Selection</source>
        <translation type="unfinished">Pažymėtos srities raides pakeisti &amp;mažosiomis raidėmis</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Comment</source>
        <translation type="unfinished">&amp;Užkomentuoti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Uncomment</source>
        <translation type="unfinished">&amp;Atkomentuoti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Comment (Choosing String)</source>
        <translation type="unfinished">Užkomentuoti nurodant komentavimo ženklą</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Indent Selection Rigidly</source>
        <translation type="unfinished">Pažymėtą sritį padaryti su &amp;įtraukomis</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Unindent Selection Rigidly</source>
        <translation type="unfinished">&amp;Šalinti pažymėtos srities įtraukas</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Indent Code</source>
        <translation type="unfinished">Kodą padaryti su įtraukomis</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Convert Line Endings to &amp;Windows (CRLF)</source>
        <translation type="unfinished">Eilučių pabaigos ženklai pagal &amp;Windows OS (CR LF)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Convert Line Endings to &amp;Unix (LF)</source>
        <translation type="unfinished">Eilučių pabaigos ženklai pagal &amp;Unix OS (LF)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Convert Line Endings to Legacy &amp;Mac (CR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Navi&amp;gation</source>
        <translation type="unfinished">Teksto ž&amp;ymeklio padėtys</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Go &amp;to Line...</source>
        <translation type="unfinished">Perei&amp;ti į eilutę...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Move to Matching Brace</source>
        <translation type="unfinished">Pereiti į priešingą skliaustelių poros pusę</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select to Matching Brace</source>
        <translation type="unfinished">Pažymėti skliaustelių poros turinį</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Next Bookmark</source>
        <translation type="unfinished">Seka&amp;nti eilutė su žymele</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Pre&amp;vious Bookmark</source>
        <translation type="unfinished">&amp;Ankstesnė eilutė su žymele</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Toggle &amp;Bookmark</source>
        <translation type="unfinished">Pridėti/nuimti &amp;žymelę</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Remove All Bookmarks</source>
        <translation type="unfinished">&amp;Pašalinti visas žymeles</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Preferences...</source>
        <translation type="unfinished">&amp;Nustatymai...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Styles Preferences...</source>
        <translation type="unfinished">&amp;Vaizdavimo nustatymai...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Vaizdas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Editor</source>
        <translation type="unfinished">&amp;Tvarkyklė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show &amp;Line Numbers</source>
        <translation type="unfinished">Rodyti eilučių &amp;numerius</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Whitespace Characters</source>
        <translation type="unfinished">Rodyti &amp;tarpo ženklus</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Line &amp;Endings</source>
        <translation type="unfinished">Rodyti eilutės &amp;pabaigos ženklus</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Indentation Guides</source>
        <translation type="unfinished">Rodyti įtraukų l&amp;inijas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Long Line &amp;Marker</source>
        <translation type="unfinished">Rodyti &amp;lapo pločio liniją</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;Toolbar</source>
        <translation type="unfinished">Rodyti į&amp;rankių juostą</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Statusbar</source>
        <translation type="unfinished">Rodyti &amp;būvio eilutę</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Horizontal Scrollbar</source>
        <translation type="unfinished">Rodyti &amp;horizontalią slinkties juostą</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Zoom &amp;In</source>
        <translation type="unfinished">&amp;Didinti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Zoom &amp;Out</source>
        <translation type="unfinished">&amp;Mažinti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Normal Size</source>
        <translation type="unfinished">&amp;Numatytasis priartinimas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Sort Tabs Alphabetically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Debug</source>
        <translation type="unfinished">&amp;Derinti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Toggle &amp;Breakpoint</source>
        <translation type="unfinished">Pridėti/nuimti sustojimo &amp;tašką</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Next Breakpoint</source>
        <translation type="unfinished">&amp;Sekantis sustojimo taškas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pre&amp;vious Breakpoint</source>
        <translation type="unfinished">&amp;Ankstesnis sustojimo taškas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Remove All Breakpoints</source>
        <translation type="unfinished">&amp;Pašalinti visus sustojimo taškus</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Run</source>
        <translation type="unfinished">&amp;Vykdyti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save File and Run/Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Run &amp;Selection</source>
        <translation type="unfinished">Vykdyti &amp;pažymėtos srities kodą</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save File and Run All &amp;Tests</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save File and Run All &amp;Demos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Pagalba</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Help on Keyword</source>
        <translation type="unfinished">&amp;Pagalba pagal raktinį žodį</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Documentation on Keyword</source>
        <translation type="unfinished">&amp;Dokumentacija apie raktinį žodį</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Recent Files</source>
        <translation type="unfinished">Paskutinės atvertos bylos</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Copy Full File &amp;Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Editor</source>
        <translation type="unfinished">Tvarkyklė</translation>
    </message>
</context>
<context>
    <name>octave::file_editor_tab</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/file-editor-tab.cc" line="+161"/>
        <source>line:</source>
        <translation type="unfinished">eilutė:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>col:</source>
        <translation type="unfinished">stulpelis:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>encoding:</source>
        <translation type="unfinished">koduotė:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>eol:</source>
        <translation type="unfinished">eilutės pabaiga:</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Breakpoint condition</source>
        <translation type="unfinished">Sustojimo taško būvis</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>ERROR: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+77"/>
        <location line="+1260"/>
        <location line="+152"/>
        <location line="+20"/>
        <location line="+447"/>
        <location line="+111"/>
        <location line="+103"/>
        <location line="+38"/>
        <location line="+60"/>
        <location line="+57"/>
        <location line="+36"/>
        <source>Octave Editor</source>
        <translation type="unfinished">Octave tvarkyklė</translation>
    </message>
    <message>
        <location line="-2283"/>
        <source>Cannot add breakpoint to modified or unnamed file.
Save and add breakpoint, or cancel?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+924"/>
        <source>Goto line</source>
        <translation type="unfinished">Pereiti į eilutę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Line number</source>
        <translation type="unfinished">Eilutės numeris</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Comment selected text</source>
        <translation type="unfinished">Užkomentuoti pažymėtą sritį</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment string to use:
</source>
        <translation type="unfinished">Komentavimui naudoti šiuos ženklus:
</translation>
    </message>
    <message>
        <location line="+140"/>
        <location line="+57"/>
        <source>&lt;unnamed&gt;</source>
        <translation type="unfinished">&lt;bevardis&gt;</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Do you want to cancel closing, save, or discard the changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The file

  %1

is about to be closed but has been modified.  %2</source>
        <translation type="unfinished">Jūs bandote uždaryti bylą
&quot;%1&quot;,
kurios turinys buvo pakeistas, bet neišsaugotas.
%2</translation>
    </message>
    <message>
        <location line="+152"/>
        <source>Unable to read file &apos;%1&apos;
with selected encoding &apos;%2&apos;: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>There were problems reading the file
%1
with the selected encoding %2.

Modifying and saving the file might cause data loss!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Edit anyway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+45"/>
        <source>Chan&amp;ge encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-43"/>
        <location line="+36"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Uždaryti</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Select new default encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Please select a new encoding
for reloading the current file.

This does not change the default encoding.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Debug or Save</source>
        <translation type="unfinished">Derinti ar saugoti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This file is currently being executed.
Quit debugging and save?</source>
        <translation type="unfinished">Jūs esate bylos kodo derinimo režime.
Baigti derinimą ir išsaugoti?</translation>
    </message>
    <message>
        <location line="+214"/>
        <source>Could not open file %1 for writing:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+111"/>
        <source>The changes could not be saved to the file
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Octave Files (*.m)</source>
        <translation type="unfinished">Octave bylos (*.m)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>All Files (*)</source>
        <translation type="unfinished">Visos bylos (*)</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>&quot;%1&quot;
is not a valid identifier.

If you keep this filename, you will not be able to
call your script using its name as an Octave command.

Do you want to choose another name?</source>
        <translation type="unfinished">Neteisingas bylos &quot;%1&quot; pavadinimas.

Jei palikstite tokį bylos pavadinimą, negalėsite bylos
pavadinimo naudoti kaip Octave komandos.

Ar norite pakeisti bylos pavadinimą?</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>The current editor contents can not be encoded
with the selected encoding %1.
Using it would result in data loss!

Please select another one!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-98"/>
        <source>%1
 already exists
Do you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+155"/>
        <source>It seems that &apos;%1&apos; has been modified by another application. Do you want to reload it?</source>
        <translation type="unfinished">Atrodo, kad bylos &quot;%1&quot; turinys buvo pakeistas naudojant kitą programą. Ar norite iš naujo įkleti bylos turinį?</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>

Warning: The contents in the editor is modified!</source>
        <translation type="unfinished">

Įspėjimas: bylos turinys tvarkyklėje buvo pakeistas!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>It seems that the file
%1
has been deleted or renamed. Do you want to save it now?%2</source>
        <translation type="unfinished">Atrodo, kad byla
&quot;%1&quot;
buvo pervadinta ar ištrinta. Ar norite ją išsaugoti?%2</translation>
    </message>
</context>
<context>
    <name>octave::files_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/files-dock-widget.cc" line="-4"/>
        <source>Could not rename file &quot;%1&quot; to &quot;%2&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+101"/>
        <source>File Browser</source>
        <translation type="unfinished">Bylų naršyklė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse your files</source>
        <translation type="unfinished">Naršykite</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>File size</source>
        <translation type="unfinished">Bylos dydis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File type</source>
        <translation type="unfinished">Bylos tipas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date modified</source>
        <translation type="unfinished">Pakeitimo data</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show hidden</source>
        <translation type="unfinished">Rodyti paslėptas bylas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alternating row colors</source>
        <translation type="unfinished">Kitokia kas antros eilutės fono splava</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Enter the path or filename</source>
        <translation type="unfinished">Įveskite aplanko kelią arba bylos pavadinimą</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>One directory up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Octave directory</source>
        <translation type="unfinished">Rodyti Octave aplanką</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go to current Octave directory</source>
        <translation type="unfinished">Pereiti į numatytąjį Octave darbo aplanką</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Octave directory</source>
        <translation type="unfinished">Nustatyti Octave darbo aplanką</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set Octave directory to current browser directory</source>
        <translation type="unfinished">Einamąjį aplanką padaryti Octave darbo aplanku</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Actions on current directory</source>
        <translation type="unfinished">Veiksmai einamajame aplanke</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Home Directory</source>
        <translation type="unfinished">Rodyti namų aplanką</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Set Browser Directory...</source>
        <translation type="unfinished">Parinkti naršyklės aplanką...</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+438"/>
        <source>Find Files...</source>
        <translation type="unfinished">Rasti bylas...</translation>
    </message>
    <message>
        <location line="-434"/>
        <location line="+446"/>
        <source>New File...</source>
        <translation type="unfinished">Nauja byla...</translation>
    </message>
    <message>
        <location line="-443"/>
        <location line="+445"/>
        <source>New Directory...</source>
        <translation type="unfinished">Naujas aplankas...</translation>
    </message>
    <message>
        <location line="-390"/>
        <source>Double-click to open file/folder, right click for alternatives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Rename...</source>
        <translation type="unfinished">Pervadinti...</translation>
    </message>
    <message>
        <location line="+317"/>
        <source>Open</source>
        <translation type="unfinished">Atidaryti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in System File Explorer</source>
        <translation type="unfinished">Atidaryti OS bylų naršyklėje</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in Text Editor</source>
        <translation type="unfinished">Atidaryti Octave tvarkyklėje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy Selection to Clipboard</source>
        <translation type="unfinished">Pažymėtas bylas/aplankus kopijuoti į atminties mainų sritį (clipboard)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Run</source>
        <translation type="unfinished">Vykdyti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Load Data</source>
        <translation type="unfinished">Įkelti duomenis</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set Current Directory</source>
        <translation type="unfinished">Parinkti einamąjį aplanką</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add to Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+7"/>
        <source>Selected Directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-5"/>
        <location line="+7"/>
        <source>Selected Directories and Subdirectories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Remove from Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Delete...</source>
        <translation type="unfinished">Ištrinti...</translation>
    </message>
    <message>
        <location line="+140"/>
        <location line="+11"/>
        <location line="+17"/>
        <source>Delete file/directory</source>
        <translation type="unfinished">Ištrinti bylą/aplanką</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Are you sure you want to delete all %1 selected files?
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Are you sure you want to delete
</source>
        <translation type="unfinished">Jūs įsitikinęs, kad norite ištrinti
</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Can not delete a directory that is not empty</source>
        <translation type="unfinished">Negaliu ištrinti netuščio aplanko</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Deletion error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Could not delete file &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+216"/>
        <source>Set directory of file browser</source>
        <translation type="unfinished">Aplanko parinkimas bylų naršyklei</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Create File</source>
        <translation type="unfinished">Sukurti bylą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Create file in
</source>
        <comment>String ends with 
!</comment>
        <translation type="unfinished">Byla bus sukurta
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New File.txt</source>
        <translation type="unfinished">Nauja_byla.txt</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Create Directory</source>
        <translation type="unfinished">Sukurti aplanką</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Create folder in
</source>
        <comment>String ends with 
!</comment>
        <translation type="unfinished">Aplankas bus sukuras
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New Directory</source>
        <translation type="unfinished">Naujo aplanko pavadinimas</translation>
    </message>
</context>
<context>
    <name>octave::final_page</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="+318"/>
        <source>Enjoy!</source>
        <translation type="unfinished">Mėgaukitės!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Previous</source>
        <translation type="unfinished">Ankstesnis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Finish</source>
        <translation type="unfinished">Užbaigti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation type="unfinished">Atšaukti</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;We hope you find Octave to be a useful tool.&lt;/p&gt;
&lt;p&gt;If you encounter problems, there are a number of ways to get help, including commercial support options, a discussion board, a wiki, and other community-based support channels.
You can find more information about each of these by visiting &lt;a href=&quot;https://octave.org/support.html&quot;&gt;https://octave.org/support.html&lt;/a&gt; (opens in external browser).&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;For more information about Octave:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Visit &lt;a href=&quot;https://octave.org&quot;&gt;https://octave.org&lt;/a&gt; (opens in external browser)&lt;/li&gt;
&lt;li&gt;Get the documentation online in &lt;a href=&quot;https://www.gnu.org/software/octave/doc/interpreter/index.html&quot;&gt;HTML&lt;/a&gt; or &lt;a href=&quot;https://www.gnu.org/software/octave/octave.pdf&quot;&gt;PDF&lt;/a&gt; format (links open in external browser)&lt;/li&gt;
&lt;li&gt;Open the documentation browser of the Octave GUI with the help menu&lt;/li&gt;
&lt;/ul&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::find_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/find-dialog.cc" line="+93"/>
        <source>Editor: Find and Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Find:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter search text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Re&amp;place:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter replacement text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Match &amp;case</source>
        <translation type="unfinished">Skirti didžiasias ir mažasias &amp;raides</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search from &amp;start</source>
        <translation type="unfinished">Paiešką pradėti &amp;nuo bylos pradžios</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Wrap while searching</source>
        <translation type="unfinished">Paieška ra&amp;tu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Find &amp;Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Pre&amp;vious</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Replace</source>
        <translation type="unfinished">Pa&amp;keisti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Replace &amp;All</source>
        <translation type="unfinished">Pakeisti &amp;visus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;More...</source>
        <translation type="unfinished">&amp;Daugiau...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Whole words</source>
        <translation type="unfinished">Pilnas &amp;žodis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Regular &amp;expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search &amp;backward</source>
        <translation type="unfinished">Ieškoti atbuliniu &amp;eiliškumu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search se&amp;lection</source>
        <translation type="unfinished">Ieškoti pa&amp;žymėtoje srityje</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>Search from end</source>
        <translation type="unfinished">Paiešką pradėti nuo bylos pabaigos</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search from start</source>
        <translation type="unfinished">Paiešką pradėti nuo bylos pradžio</translation>
    </message>
    <message>
        <location line="+290"/>
        <source>Replace Result</source>
        <translation type="unfinished">Pakeitimų duomenys</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 items replaced</source>
        <translation type="unfinished">Atlikta %1 pakeitimų</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Find Result</source>
        <translation type="unfinished">Paieškos duomenys</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No more matches found</source>
        <translation type="unfinished">Daugiau atitikmenų nerasta</translation>
    </message>
</context>
<context>
    <name>octave::find_files_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-files-dialog.cc" line="+61"/>
        <source>Find Files</source>
        <translation type="unfinished">Rasti bylas</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Named:</source>
        <translation type="unfinished">Pavadinimas:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start in:</source>
        <translation type="unfinished">Paiešką pradėti čia:</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter the start directory</source>
        <translation type="unfinished">Įveskite aplanko, kuriame bus ieškoma, pavadinimą</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Browse...</source>
        <translation type="unfinished">Naršyti...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse for start directory</source>
        <translation type="unfinished">Parinkti aplanką, kuriame bus ieškoma</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Current Dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set start directory to current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Search subdirectories</source>
        <translation type="unfinished">Vidiniai aplankai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search recursively through directories for matching files</source>
        <translation type="unfinished">Bylos(-ų) su tinkamu pavadinimu ieškoti pradžios aplanko vidiniuose aplankuose</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Include directory names</source>
        <translation type="unfinished">Įtraukti aplankų pavadinimus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Include matching directories in search results</source>
        <translation type="unfinished">Į paiešką įtraukti vidinių aplankų pavadinimus</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+18"/>
        <source>Ignore case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-17"/>
        <location line="+18"/>
        <source>Perform case insensitive match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-15"/>
        <source>Contains text:</source>
        <translation type="unfinished">Bylos turinye yra:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Include only files containing specified text in search results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Text to match</source>
        <translation type="unfinished">Paieškos tekstas</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Results: Double click opens the file or sets the directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search results</source>
        <translation type="unfinished">Paieškos suvestinė</translation>
    </message>
    <message>
        <location line="-59"/>
        <source>Idle.</source>
        <translation type="unfinished">Laukiama.</translation>
    </message>
    <message>
        <location line="-94"/>
        <source>Enter the filename search patterns.
Several different patterns can be
separated by &apos;;&apos;, e.g. &apos;*.cc ; *.h&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Find</source>
        <translation type="unfinished">Ieškoti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Start search for matching files</source>
        <translation type="unfinished">Pradėti paiešką</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Stop</source>
        <translation type="unfinished">Sustoti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Filename/Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>File contents</source>
        <translation type="unfinished">Bylos turinys</translation>
    </message>
    <message>
        <location line="+143"/>
        <source>Searching...</source>
        <translation type="unfinished">Vykdoma paieška...</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>%1 match (es)</source>
        <translation type="unfinished">Rasta: %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Set search directory</source>
        <translation type="unfinished">Parinkite paieškos aplanką</translation>
    </message>
</context>
<context>
    <name>octave::find_files_model</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-files-model.cc" line="+96"/>
        <source>Filename</source>
        <translation type="unfinished">Bylos pavadinimas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory</source>
        <translation type="unfinished">Aplankas</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Double click to open the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Double click to set the directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::find_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-widget.cc" line="+48"/>
        <source>Find:</source>
        <translation type="unfinished">Rasti:</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+1"/>
        <source>Search forward</source>
        <translation type="unfinished">Ieškoti &quot;einant&quot; pirmyn</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1"/>
        <source>Search backward</source>
        <translation type="unfinished">Ieškoti &quot;einant&quot; atgal</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close find dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::gui_settings</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-settings.cc" line="+824"/>
        <source>Error %1 creating the settings file
%2
Make sure you have read and write permissions to
%3

Octave GUI must be closed now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Octave Critical Error</source>
        <translation type="unfinished">Kritinė Octave klaida</translation>
    </message>
</context>
<context>
    <name>octave::history_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/history-dock-widget.cc" line="+54"/>
        <source>Browse and search the command history.</source>
        <translation type="unfinished">Įvykdytų komandų peržiūra ir paieška.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Copy</source>
        <translation type="unfinished">Kopijuoti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Evaluate</source>
        <translation type="unfinished">Įvykdyti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create script</source>
        <translation type="unfinished">Sukurti scenarijų (script&apos;ą)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Hide filter</source>
        <translation type="unfinished">Paslėpti atranką</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show filter</source>
        <translation type="unfinished">Rodyti atranką</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>Double-click a command to transfer it to the Command Window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter text to filter the command history</source>
        <translation type="unfinished">Įveskite paieškos tekstą, pagal kurį bus atrinkti įrašai įvykdytų komandų sąraše</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Filter</source>
        <translation type="unfinished">Atranka</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Command History</source>
        <translation type="unfinished">Įvykdytos komandos</translation>
    </message>
</context>
<context>
    <name>octave::initial_page</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="-190"/>
        <source>Welcome to Octave!</source>
        <translation type="unfinished">Malonu, kad naudojate Octave!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation type="unfinished">Atšaukti</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;You seem to be using the Octave graphical interface for the first time on this computer.
Click &apos;Next&apos; to create a configuration file and launch Octave.&lt;/p&gt;
&lt;p&gt;The configuration file is stored in&lt;br&gt;%1.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;body&gt;
&lt;p&gt;Atrodo, kad šiame kompiuteryje Jūs pirmą kartą naudojate &quot;Octave&quot; grafikos sąsają.
Nuostatų bylos sukurimui ir &quot;Octave&quot; paleidimui paspauskite &quot;Toliau&quot; mygtuką.&lt;/p&gt;
&lt;p&gt;Nuostatų bylos vieta: %1.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>octave::label_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/octave-dock-widget.cc" line="+82"/>
        <source>Undock Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Close Widget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::main_window</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/main-window.cc" line="+155"/>
        <source>Profiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+361"/>
        <source>Octave</source>
        <translation type="unfinished">Octave</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to exit Octave?</source>
        <translation type="unfinished">Ar tikrai norite uždaryti Octave?</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Save Workspace As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Load Workspace</source>
        <translation type="unfinished">Įkelti aplinkos kintamuosius</translation>
    </message>
    <message>
        <location line="+214"/>
        <location line="+1747"/>
        <source>About Octave</source>
        <translation type="unfinished">Apie Octave</translation>
    </message>
    <message>
        <location line="-1560"/>
        <location line="+1635"/>
        <source>Browse directories</source>
        <translation type="unfinished">Parinkti aplanką</translation>
    </message>
    <message>
        <location line="-1330"/>
        <source>Octave Files (*.m);;All Files (*)</source>
        <translation type="unfinished">Octave bylos (*.m);;Visos bylos (*)</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>New Function</source>
        <translation type="unfinished">Nauja funkcija</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New function name:
</source>
        <translation type="unfinished">Naujos funkcijos pavadinimas:
</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>%1 is a built-in, compiled, or inline
function and can not be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Can not find function %1</source>
        <translation type="unfinished">Nepavyko rasti &quot;%1&quot; funkcijos</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Octave Editor</source>
        <translation type="unfinished">Octave tvarkyklė</translation>
    </message>
    <message>
        <location line="+736"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Byla</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open...</source>
        <translation type="unfinished">Atidaryti...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open an existing file in editor</source>
        <translation type="unfinished">Atidaryti bylą tvarkyklėje</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Load Workspace...</source>
        <translation type="unfinished">Įkelti aplinkos kintamuosius...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save Workspace As...</source>
        <translation type="unfinished">Išsaugoti aplinkos kintamuosius kaip...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Exit</source>
        <translation type="unfinished">Uždaryti</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>New</source>
        <translation type="unfinished">Naujas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Script</source>
        <translation type="unfinished">Naujas scenarijus</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New Function...</source>
        <translation type="unfinished">Nauja funkcija...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New Figure</source>
        <translation type="unfinished">Naujas braižymo langas</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">&amp;Tvarkyti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Undo</source>
        <translation type="unfinished">Atšaukti</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Copy</source>
        <translation type="unfinished">Kopijuoti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <translation type="unfinished">Įklijuoti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Select All</source>
        <translation type="unfinished">Pažymėti viską</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear Clipboard</source>
        <translation type="unfinished">Išvalyti atminties mainų sritį (clipboard)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Find Files...</source>
        <translation type="unfinished">Rasti bylas...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear Command Window</source>
        <translation type="unfinished">Išvalyti komandų langą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Command History</source>
        <translation type="unfinished">Išvalyti įvykdytų komandų sąrašą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Workspace</source>
        <translation type="unfinished">Ištrinti aplinkos kintamuosius</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Set Path...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Preferences...</source>
        <translation type="unfinished">Nustatymai...</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>De&amp;bug</source>
        <translation type="unfinished">&amp;Derinimas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Step</source>
        <translation type="unfinished">Žengti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Step In</source>
        <translation type="unfinished">Žengti į vidų</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Step Out</source>
        <translation type="unfinished">Žengti į išorę</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue</source>
        <translation type="unfinished">Tęsti scenarijau vykdymą</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Quit Debug Mode</source>
        <translation type="unfinished">Išeiti iš derinimo būvio</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start &amp;Profiler Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Resume Profiler Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Stop Profiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Show Profiler Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>&amp;Window</source>
        <translation type="unfinished">&amp;Langas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Command Window</source>
        <translation type="unfinished">Rodyti komandų langą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Command History</source>
        <translation type="unfinished">Rodyti įvykdytų komandų langą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show File Browser</source>
        <translation type="unfinished">Rodyti bylų naršyklę</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Workspace</source>
        <translation type="unfinished">Rodyti aplinkos kintamųjų langą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Editor</source>
        <translation type="unfinished">Rodyti tvarkyklės langą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Documentation</source>
        <translation type="unfinished">Rodyti dokumentacijos langą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Variable Editor</source>
        <translation type="unfinished">Rodyti kintamųjų tvarkyklės langą</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Command Window</source>
        <translation type="unfinished">Komandų langas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command History</source>
        <translation type="unfinished">Įvykdytos komandos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>File Browser</source>
        <translation type="unfinished">Bylų naršyklė</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Workspace</source>
        <translation type="unfinished">Aplinkos kintamieji</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Editor</source>
        <translation type="unfinished">Tvarkyklė</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+46"/>
        <source>Documentation</source>
        <translation type="unfinished">Dokumentacija</translation>
    </message>
    <message>
        <location line="-43"/>
        <source>Variable Editor</source>
        <translation type="unfinished">Kintamųjų tvarkyklė</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Previous Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset Default Window Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Pagalba</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Report Bug</source>
        <translation type="unfinished">Pranešti apie klaidą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Octave Packages</source>
        <translation type="unfinished">Octave paketai</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Get Involved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Donate to Octave</source>
        <translation type="unfinished">Paremti Octave</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>On Disk</source>
        <translation type="unfinished">Diske</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Online</source>
        <translation type="unfinished">Octave tinklapyje</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;News</source>
        <translation type="unfinished">&amp;Naujienos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Release Notes</source>
        <translation type="unfinished">Octave laidos pastabos</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Community News</source>
        <translation type="unfinished">Bendruomenės naujienos</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Toolbar</source>
        <translation type="unfinished">Įrankių juosta</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Enter directory name</source>
        <translation type="unfinished">Įveskite aplanko pavadinimą</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Current Directory: </source>
        <translation type="unfinished">Einamasis aplankas: </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::news_reader</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/news-reader.cc" line="+106"/>
        <source>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Octave&apos;s community news source seems to be unavailable.
&lt;/p&gt;
&lt;p&gt;
For the latest news, please check
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
when you have a connection to the web (link opens in an external browser).
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; The Octave Developers, </source>
        <translation type="unfinished">&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Neprieinamas Octave bendruomenės naujienų šaltinis.
&lt;/p&gt;
&lt;p&gt;
Kai turėsite priėjimą prie interneto, paskutines naujienas galėsite paržiūrėti čia: 
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt; (nuoroda atsidaro išorinėje naršyklėje).
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; Octave kūrėjai, </translation>
    </message>
    <message>
        <location line="+18"/>
        <source>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Connecting to the web to display the latest Octave Community news has been disabled.
&lt;/p&gt;
&lt;p&gt;
For the latest news, please check
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
when you have a connection to the web (link opens in an external browser)
or enable web connections for news in Octave&apos;s network settings tab.
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; The Octave Developers, </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::octave_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/octave-dock-widget.cc" line="+144"/>
        <source>Hide Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+81"/>
        <source>Dock Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Undock Widget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::octave_qscintilla</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/octave-qscintilla.cc" line="+302"/>
        <source>Help on</source>
        <translation type="unfinished">Pagalba apie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Documentation on</source>
        <translation type="unfinished">Dokumantacija apie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit</source>
        <translation type="unfinished">Tvarkyti</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>dbstop if ...</source>
        <translation type="unfinished">dbstop() jei ...</translation>
    </message>
    <message>
        <location line="+544"/>
        <source>Octave Editor</source>
        <translation type="unfinished">Octave tvarkyklė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Creating temporary files failed.
Make sure you have write access to temp. directory
%1

&quot;Run Selection&quot; requires temporary files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+313"/>
        <source>Press &apos;%1&apos; to replace all occurrences of &apos;%2&apos; with &apos;%3&apos;.</source>
        <translation type="unfinished">Norėdami pakeisti visus &quot;%2&quot;, esančius &quot;%3&quot;, paspauskite &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>octave::octave_txt_lexer</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/octave-txt-lexer.cc" line="+41"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::qt_interpreter_events</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/qt-interpreter-events.cc" line="+357"/>
        <location line="+5"/>
        <location line="+2"/>
        <source>Create</source>
        <translation type="unfinished">Sukurti</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+31"/>
        <source>Cancel</source>
        <translation type="unfinished">Atšaukti</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>File
%1
does not exist. Do you want to create it?</source>
        <translation type="unfinished">Nėra &quot;%1&quot;
bylos. Ar norite tokią sukurti?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Octave Editor</source>
        <translation type="unfinished">Octave tvarkyklė</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>The file %1 does not exist in the load path.  To run or debug the function you are editing, you must either change to the directory %2 or add that directory to the load path.</source>
        <translation type="unfinished">Funkcijų aplankuose nėra &quot;%1&quot; bylos. Tvarkomos funkcijos įvykdymui ar derinimui reikia &quot;%2&quot; aplanaką padaryti darbiniu arba pridėti prie funkcijų aplankų sąrašo.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The file %1 is shadowed by a file with the same name in the load path. To run or debug the function you are editing, change to the directory %2.</source>
        <translation type="unfinished">Funkcijų aplankuose jau yra &quot;%1&quot; byla. Tvarkomos funkcijos įvykdymui ar derinimui reikia &quot;%2&quot; aplanaką padaryti darbiniu.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Change Directory or Add Directory to Load Path</source>
        <translation type="unfinished">Aplanaką padaryti darbiniu arba pridėti prie funkcijų aplankų sąrašo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Directory</source>
        <translation type="unfinished">&amp;Aplanką padaryti darbiniu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add Directory to Load Path</source>
        <translation type="unfinished">&amp;Pridėti prie funkcijų aplankų sąrašo</translation>
    </message>
</context>
<context>
    <name>octave::release_notes</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/release-notes.cc" line="+84"/>
        <source>The release notes file &apos;%1&apos; is empty.</source>
        <translation type="unfinished">Byla &quot;%1&quot;, kurioje saugomos Octave laidos pastabos, yra tuščia.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The release notes file &apos;%1&apos; cannot be read.</source>
        <translation type="unfinished">Byla &quot;%1&quot;, kurioje saugomos Octave laidos pastabos, negali būti nustaitytas.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Octave Release Notes</source>
        <translation type="unfinished">Octave laidos pastabos</translation>
    </message>
</context>
<context>
    <name>octave::set_path_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/set-path-dialog.cc" line="+64"/>
        <source>Set Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>All changes take effect immediately.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add Directory...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Single Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory With Subdirectories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move to Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save</source>
        <translation type="unfinished">Išsaugoti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Revert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Revert Last Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Revert All Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::settings_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ed.h" line="+161"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-cs.h" line="+61"/>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+56"/>
        <source>Foreground</source>
        <translation type="unfinished">Teksto spalva</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+1"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+1"/>
        <source>Selected Foreground</source>
        <translation type="unfinished">Pažymėto teksto spalva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selected Background</source>
        <translation type="unfinished">Pažymėto fono spalva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alternating Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ws.h" line="+70"/>
        <source>argument</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>global</source>
        <translation type="unfinished">visuotinas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>persistent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-global.h" line="+193"/>
        <source>Environment Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-sd.h" line="+41"/>
        <source>Second color mode (light/dark)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Switch to a second set of colors.
Useful for defining light/dark modes.
Discards non-applied current changes!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reload the default colors,
depends on currently selected mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Reload the default style values,
depends on currently selected mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>&amp;Reload default colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Reload default styles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.cc" line="-972"/>
        <source>Loading current preferences ... </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+6"/>
        <location line="+1088"/>
        <source>System setting</source>
        <translation type="unfinished">Numatytieji</translation>
    </message>
    <message>
        <location line="-834"/>
        <source>IBeam Cursor</source>
        <translation type="unfinished">stačias brūkšnys</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Block Cursor</source>
        <translation type="unfinished">stačiakampis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Underline Cursor</source>
        <translation type="unfinished">gulsčias brūkšnys</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Color of highlighted current line (magenta (255,0,255) for automatic color)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Set Octave Startup Directory</source>
        <translation type="unfinished">Nustatyti naujai paleistos Octave darbo aplanką</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set File Browser Startup Directory</source>
        <translation type="unfinished">Nustatyti pradinį bylų naršyklės aplanką</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Applying preferences ... </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+22"/>
        <location line="+1120"/>
        <source>Failed to open %1 as Octave shortcut file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-453"/>
        <source>Enable attribute colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide tool tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+384"/>
        <source>Import shortcuts from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+6"/>
        <source>Octave Shortcut Files (*.osc);;All Files (*)</source>
        <translation type="unfinished">Octave sparčiųjų klavišų rinkinių byla (*.osc);;Visos bylos (*)</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Export shortcuts to file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Overwriting Shortcuts</source>
        <translation type="unfinished">Perrašomi sparčiųjų klavišų rinkiniai</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are about to overwrite all shortcuts.
Would you like to save the current shortcut set or cancel the action?</source>
        <translation type="unfinished">Jūs ruošiatės perrašyti visus sparčiųjų klavišų rinkinius.
Norite išsaugoti esamus sparčiųjų klavišų rinkinius ar atšaukti veiksmą?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Don&apos;t save</source>
        <translation type="unfinished">Nesaugoti</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Octave GUI preferences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::setup_community_news</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="+45"/>
        <source>Community News</source>
        <translation type="unfinished">Bendruomenės naujienos</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Previous</source>
        <translation type="unfinished">Ankstesnis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation type="unfinished">Atšaukti</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;When Octave starts, it will optionally check the Octave web site for current news and information about the Octave community.
The check will happen at most once each day and news will only be displayed if there is something new since the last time you viewed the news.&lt;/p&gt;
&lt;p&gt;You may also view the news by selecting the &quot;Community News&quot; item in the &quot;Help&quot; menu, or by visiting
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;body&gt;
&lt;p&gt;Octave bendruomenės naujienos tikrinamos kaskart paleidus Octave.
Tikrinimas atliekamas ne daugiau kaip vieną kartą per dieną, o naujienos rodomos tik tada, jei nuo paskutinio naujienų peržiūrėjimo yra kažkas naujo.&lt;/p&gt;
&lt;p&gt;Naujienas taip pat galite matyti pasirinkę menių punktą &quot;Pagalba =&gt; Bendruomenės naujienos&quot; arba apsilankius
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt; tinklapyje.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;Allow Octave to connect to the Octave web site when it starts in order to display current news and information about the Octave community.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::shortcut_edit_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/shortcuts-tree-widget.cc" line="+172"/>
        <source>Enter New Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter custom shortcut
Action: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Enter shortcut by typing it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add Shift modifier
(allows one to enter number keys)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Actual Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Default Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Clear</source>
        <translation type="unfinished">Išvalyti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set to default</source>
        <translation type="unfinished">Padaryti numatytu</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>Double Shortcut</source>
        <translation type="unfinished">Pasikartojantis sparčiųjų klavišų rinkinys</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The chosen shortcut
  &quot;%1&quot;
is already used for the action
  &quot;%2&quot;.
Do you want to use the shortcut and remove it from the previous action?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::shortcuts_tree_widget</name>
    <message>
        <location line="+27"/>
        <source>Global</source>
        <translation type="unfinished">Visuotinas</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+49"/>
        <source>File Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+49"/>
        <source>Edit Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+52"/>
        <source>Debug Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Tools Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Window Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+49"/>
        <source>Help Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-46"/>
        <source>News Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Handling of Dock Widgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tab Handling in Dock Widgets</source>
        <translation type="unfinished">Koretelių tvarkymas &quot;prisegtuose&quot; languose</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find &amp; Replace in Dock Widgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zooming in Editor and Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Editor</source>
        <translation type="unfinished">Tvarkyklė</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>View Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Run Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Documentation Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+115"/>
        <source>item %1 not found in shortcut settings dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::terminal_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/terminal-dock-widget.cc" line="+106"/>
        <source>Command Window</source>
        <translation type="unfinished">Komandų langas</translation>
    </message>
</context>
<context>
    <name>octave::variable_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/variable-editor.cc" line="+177"/>
        <source>Dock Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable Editor: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Undock Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Restore geometry</source>
        <translation type="unfinished">Atkurti geometriją</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Redock</source>
        <translation type="unfinished">&quot;Persegti&quot; langą</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor</name>
    <message>
        <location line="+922"/>
        <source>Variable Editor</source>
        <translation type="unfinished">Kintamųjų tvarkyklė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit variables.</source>
        <translation type="unfinished">Tvarkyti kintamuosius.</translation>
    </message>
    <message>
        <location line="+555"/>
        <source>Variable Editor Toolbar</source>
        <translation type="unfinished">Kintamųjų tvarkyklės įrankių juosta</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save</source>
        <translation type="unfinished">Išsaugoti</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save variable to a file</source>
        <translation type="unfinished">Kintamąjį išsaugoti byloje</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+5"/>
        <location line="+6"/>
        <source>Save in format ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Save variable to a file in different format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cut</source>
        <translation type="unfinished">Iškirpti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cut data to clipboard</source>
        <translation type="unfinished">Iškirptus duomenis patalpinti į atminties mainų sritį (clipboard)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy</source>
        <translation type="unfinished">Kopijuoti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy data to clipboard</source>
        <translation type="unfinished">Duomenų kopiją patalpinti į atminties mainų sritį (clipboard)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste</source>
        <translation type="unfinished">Įklijuoti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste clipboard into variable data</source>
        <translation type="unfinished">Į kintamojo vetę įrašyti atminties mainų srities (clipboard) duomenis</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+6"/>
        <location line="+7"/>
        <source>Plot</source>
        <translation type="unfinished">Atvaizduoti grafiškai</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Plot Selected Data</source>
        <translation type="unfinished">Pažymėtus duomenis atvaizduoti garfiškai</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Plot selected data</source>
        <translation type="unfinished">Pažymėtus duomenis atvaizduoti garfiškai</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Up</source>
        <translation type="unfinished">Vienu lygiu aukštyn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go one level up in variable hierarchy</source>
        <translation type="unfinished">Kintamojo sandaroje pereiti vienu lygiu aukščiau</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor_stack</name>
    <message>
        <location line="-1265"/>
        <source>Save Variable %1 As</source>
        <translation type="unfinished">Kintamąjį &quot;%1&quot; išsaugoti kaip</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor_view</name>
    <message>
        <location line="+142"/>
        <source>Cut</source>
        <translation type="unfinished">Iškirpti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy</source>
        <translation type="unfinished">Kopijuoti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste</source>
        <translation type="unfinished">Įklijuoti</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Clear</source>
        <translation type="unfinished">Išvalyti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation type="unfinished">Pašalinti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Variable from Selection</source>
        <translation type="unfinished">Sukurti kintamąjį iš pažymėtų duomenų</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transpose</source>
        <translation type="unfinished">Transponuoti</translation>
    </message>
    <message>
        <location line="+56"/>
        <source> columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source> column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+52"/>
        <source> rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source> row</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>octave::welcome_wizard</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="-196"/>
        <source>Welcome to GNU Octave</source>
        <translation type="unfinished">Malonu, kad naudojate GNU Octave</translation>
    </message>
</context>
<context>
    <name>octave::workspace_model</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/workspace-model.cc" line="+49"/>
        <source>Name</source>
        <translation type="unfinished">Pavadinimas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Class</source>
        <translation type="unfinished">Klasė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dimension</source>
        <translation type="unfinished">Išmatavimai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation type="unfinished">Vertė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attribute</source>
        <translation type="unfinished">Savybė</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Right click to copy, rename, or display</source>
        <translation type="unfinished">Kopijavimui, pervardinimui ar išvedimui spauskite dešinį pelės myguką</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+2"/>
        <source>complex</source>
        <translation type="unfinished">kompleksinis</translation>
    </message>
</context>
<context>
    <name>octave::workspace_view</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ws.h" line="-21"/>
        <source>Class</source>
        <translation type="unfinished">Klasė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dimension</source>
        <translation type="unfinished">Išmatavimai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation type="unfinished">Vertė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attribute</source>
        <translation type="unfinished">Savybė</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/workspace-view.cc" line="+60"/>
        <source>Workspace</source>
        <translation type="unfinished">Aplinkos kintamieji</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View the variables in the active workspace.</source>
        <translation type="unfinished">Aplinkos kintamųjų sąrašas.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter text to filter the workspace</source>
        <translation type="unfinished">Įveskite tekstą pagal kurį bus atrinkti kintamieji</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Filter</source>
        <translation type="unfinished">Atranka</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>View the variables in the active workspace.&lt;br&gt;</source>
        <translation type="unfinished">Aplinkos kintamųjų sąrašas.&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Colors for variable attributes:</source>
        <translation type="unfinished">Kintamųjų savybių spalvos paaiškinimai:</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>Open in Variable Editor</source>
        <translation type="unfinished">Rodyti kintamųjų tvarkyklėje</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy name</source>
        <translation type="unfinished">Kopijuoti pavadinimą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy value</source>
        <translation type="unfinished">Kopijuoti vertę</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Rename</source>
        <translation type="unfinished">Pervadinti</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Only top-level symbols may be renamed</source>
        <translation type="unfinished">Gali būti pervadinti tik aukščiausio lygmens ženklai</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Hide filter</source>
        <translation type="unfinished">Paslėpti atranką</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show filter</source>
        <translation type="unfinished">Rodyti atranką</translation>
    </message>
</context>
<context>
    <name>self_listener</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/self-listener.cc" line="+82"/>
        <source>Can not open redirected stream with fd = %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Can not disable buffering of stream with fd = %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Can not dup redirected stream with fd = %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot create pipe for redirecting stream with fd = %1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot dup2 redirected stream with fd = %1
to pipe with fd = %2: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Error while listening to redirected streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Error reading from redirected strem fd = %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>
Output redirection in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source> won&apos;t work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>
Error: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Octave</source>
        <translation type="unfinished">Octave</translation>
    </message>
</context>
<context>
    <name>settings_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.ui" line="+45"/>
        <location line="+834"/>
        <location line="+1909"/>
        <source>General</source>
        <translation>Pagrindiniai</translation>
    </message>
    <message>
        <location line="-2390"/>
        <source>Octave logo only</source>
        <translation>tik Octave logotipas</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Letter icons</source>
        <translation>logotipas su raide</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>Dock widget title bar</source>
        <translation>&quot;Prisegto&quot; lango antraštė</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Small</source>
        <translation>mažas</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Large</source>
        <translation>didelis</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Custom style</source>
        <translation>Vartotojo stilius</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>3D</source>
        <translation>Erdvinis vaizdas</translation>
    </message>
    <message>
        <location line="+558"/>
        <source>Editor</source>
        <translation>Tvarkyklė</translation>
    </message>
    <message>
        <location line="+488"/>
        <location line="+140"/>
        <source>This works well for monospaced fonts. The line is drawn at a position based on the width of a space character in the default font. It may not work very well if styles use proportional fonts or if varied font sizes or bold, italic and normal texts are used.</source>
        <translation>Tinka tik &quot;monospaces&quot; šriftams. Linijos vieta priklauso nuo tarpo ženklo pločio.&lt;/br&gt;Linija gali nesutapti su tikrąja perkėlimo vieta, kai naudojami proporciniai šriftai arba jei naudojami įvairių dydžių ir stilių (pastorintas, pasviręs ir pan.) šriftai dydžiai.</translation>
    </message>
    <message>
        <location line="-535"/>
        <source>Enable Code Folding</source>
        <translation>Galimas kodo blokų sutraukimas</translation>
    </message>
    <message>
        <location line="+1059"/>
        <source>Windows (CRLF)</source>
        <translation>Windows (CRLF)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unix (LF)</source>
        <translation>Unix (LF)</translation>
    </message>
    <message>
        <location line="-1034"/>
        <source>Show horizontal scroll bar</source>
        <translation>Rodyti horizontalią slinkties juostą</translation>
    </message>
    <message>
        <location line="-509"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If set, the focus of the widgets that are docked to the main window follows the mouse cursor. This is intended for having the same behavior within the main window when &amp;quot;focus follows mouse&amp;quot; is used for the desktop environment.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Focus follows mouse for widgets docked to the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+487"/>
        <source>Show tool bar</source>
        <translation>Rodyti įrankių juostą</translation>
    </message>
    <message>
        <location line="+221"/>
        <source>Rotated tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Position</source>
        <translation type="unfinished">Padėtis</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Max. tab width in chars (0: no limit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Max. width of a tab in characters (average char. width). Especially useful for rotated tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+260"/>
        <source>Wrap long lines at current window border</source>
        <translation>Perkelti eilutės turinį į sekančią eilutę ties lango riba</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Indentation</source>
        <translation>Įtraukos</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Indent width</source>
        <translation>Įtraukų plotis</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tab indents line</source>
        <translation>Tab įtraukia liniją</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Auto indentation</source>
        <translation>Savaiminis įtaukimas</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tab width</source>
        <translation>Ženklo &quot;TAB&quot; plotis</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show indentation guides</source>
        <translation>Rodyti įtraukų linijas</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Backspace unindents line</source>
        <translation>Klavišo &quot;BackSpace&quot; paspaudimas mažina įtrauką</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Indentation uses tabs</source>
        <translation>Įraukoms naudoti &quot;TAB&quot; ženklus</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Auto completion</source>
        <translation>Savaiminis užbaigimas</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Match keywords</source>
        <translation>Raktiniai žodžiai</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Case sensitive</source>
        <translation>Skirti didžiasas ir mažasias raides</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Replace word by suggested one</source>
        <translation>Pakeisti žodį siūlomu</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Match words in document</source>
        <translation>Žodžiai panaudoti byloje</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>With Octave builtins</source>
        <translation>Į Octave &quot;įdėti&quot; žodžiai</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>With Octave functions</source>
        <translation>Octave funkcijos</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Show completion list automatically</source>
        <translation>Savaime rodyti galimų užbaigimų sąrašą</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Reload externally changed files without prompt</source>
        <translation>Be paklausimo iš naujo įkelti bylų turinį, kuris buvo pakeistas kitų programų</translation>
    </message>
    <message>
        <location line="-841"/>
        <source>Use custom file editor</source>
        <translation>Naudoti išorinę bylų tvarkyklę</translation>
    </message>
    <message>
        <location line="+934"/>
        <source>Text encoding used for loading and saving</source>
        <translation>Naudojama koduotė įkeliant ir saugant bylas</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Editor Styles</source>
        <translation>Vaizdavimas tvarkyklėje</translation>
    </message>
    <message>
        <location line="-1383"/>
        <source>(Changing buffer size clears history)</source>
        <translation>(pakeitus dydį išvalomas atliktų komandų sąrašas)</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>History buffer Size</source>
        <translation>Atliktų komandų sąrašo dydis</translation>
    </message>
    <message>
        <location line="-123"/>
        <location line="+1863"/>
        <source>Font</source>
        <translation>Šriftas</translation>
    </message>
    <message>
        <location line="-1573"/>
        <source>Show line numbers</source>
        <translation>Rodyti eilučių numerius</translation>
    </message>
    <message>
        <location line="-859"/>
        <source>Interface</source>
        <translation>Aplinka</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Confirm before exiting</source>
        <translation>Patvirtinti Octave uždarymą</translation>
    </message>
    <message>
        <location line="-72"/>
        <source>Graphic icons</source>
        <translation>išskirtinis</translation>
    </message>
    <message>
        <location line="+52"/>
        <location line="+439"/>
        <source>Show status bar</source>
        <translation>Rodyti būvio juostą</translation>
    </message>
    <message>
        <location line="-677"/>
        <source>Text inactive</source>
        <translation>Tekstas: nesužadinto</translation>
    </message>
    <message>
        <location line="-32"/>
        <location line="+45"/>
        <source>Active</source>
        <translation>einamojo</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Background inactive</source>
        <translation>Fonas: nesužadinto</translation>
    </message>
    <message>
        <location line="+218"/>
        <source>Octave Startup</source>
        <translation>Octave paleisties nustatymai</translation>
    </message>
    <message>
        <location line="+46"/>
        <location line="+1691"/>
        <source>Browse</source>
        <translation>Naršyti</translation>
    </message>
    <message>
        <location line="-1298"/>
        <source>Show whitespace</source>
        <translation>Rodyti tarpo ženklus</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Do not show whitespace used for indentation</source>
        <translation>Nerodyti tarpo ženklų, kurie naudojami įtraukose</translation>
    </message>
    <message>
        <location line="+1085"/>
        <source>Create nonexistent files without prompting</source>
        <translation>Be paklausimo sukurti trūkstamas bylas</translation>
    </message>
    <message>
        <location line="-857"/>
        <source>command line (%f=file, %l=line):</source>
        <translation>užklausos eilutė (%f=byla, %l=eilutė):</translation>
    </message>
    <message>
        <location line="-500"/>
        <source>Cursor type:</source>
        <translation>Žymeklio tipas:</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Use foreground color</source>
        <translation>Naudoti teksto spalvą</translation>
    </message>
    <message>
        <location line="+52"/>
        <location line="+1753"/>
        <source>Font size</source>
        <translation>Šrifto dydis</translation>
    </message>
    <message>
        <location line="-245"/>
        <source>File Browser</source>
        <translation>Bylų naršyklė</translation>
    </message>
    <message>
        <location line="-2017"/>
        <source>Normal</source>
        <translation>įprastas</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>These preferences are applied after any .octaverc startup files.</source>
        <translation>Šie nustatymai taikomi po bet kokių .octaverc nustatymo bylų paleisties.</translation>
    </message>
    <message>
        <location line="+540"/>
        <source>Show EOL characters</source>
        <translation>Rodyti eilutės pabaigos ženklą</translation>
    </message>
    <message>
        <location line="+945"/>
        <source>Default EOL mode</source>
        <translation>Numatytasis eilutės pabaigos ženklas</translation>
    </message>
    <message>
        <location line="-162"/>
        <source>Number of characters before list is shown: </source>
        <translation>Sąrašą rodyti, jei žodį sudaro sekantis ženklų kiekis: </translation>
    </message>
    <message>
        <location line="-1805"/>
        <source>Preferences</source>
        <translation type="unfinished">Nustatymai</translation>
    </message>
    <message>
        <location line="+351"/>
        <source>(requires restart)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Use native file dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-98"/>
        <source>Toolbar Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-224"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-41"/>
        <source>Dock widgets window icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Icon theme (requires restart)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+323"/>
        <source>Blinking cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Initial working directory of Octave interpreter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Restore last working directory of previous session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Command</source>
        <translation type="unfinished">Komanda</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Set focus to Command Window when running a command from within another widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Print debug location in Command Window in addition to the marker in the editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Command Window Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+190"/>
        <source>Number size as difference to editor font</source>
        <translation>Numerių ir tvarkyklės šriftų dydžių skirtumas</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Highlight current line (color adjustable below with editor styles)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Highlight all occurrences of a word selected by a double click</source>
        <translation>Paryškinti visus žodžio, kuris pažymėtas dvigubu pelės paspaudimu, pasikartojimus</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Rotate tabs: Vertical when at top or bottom and horizontal when left or right. The close button is not shown in rotated tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Comments (Octave)</source>
        <translation>Komentarai (tik Octave)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Strings considered for uncommenting text</source>
        <translation>Ženklai, kurie bus pašalinti įvykdžius atkomentavimo veiksmą</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>String used for commenting selected text</source>
        <translation>Ženklai naudojami pažymėtos srities užkomentavimui</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Long lines</source>
        <translation>Ilgos eilutės</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Break long lines at line length</source>
        <translation>Tęsti naujoje eilutėje, jei viršijamas eilutės ilgis</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Break lines only in comments</source>
        <translation>Tęsti naujoje eilutėje tik komentarus</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Line length</source>
        <translation>Eilutės ilgis</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Long line marker</source>
        <translation>Rodyti ilgos eilutės pabaigos žymę</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Line</source>
        <translation>kaip linija</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Background</source>
        <translation>ypatinga fono spalva</translation>
    </message>
    <message>
        <location line="+191"/>
        <source>Auto insert after &quot;if&quot; etc.</source>
        <translation>Po &quot;if&quot; ir pan., savaime</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Nothing</source>
        <translation>nieko neįterpti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&quot;endif&quot; etc.</source>
        <translation>įterpti &quot;endif&quot; ir pan.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&quot;end&quot;</source>
        <translation>įterpti &quot;end&quot;</translation>
    </message>
    <message>
        <location line="+238"/>
        <source>Debugging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Always show debug breakpoints and pointers (opens related file if closed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>File handling</source>
        <translation>Darbas su bylomis</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Restore editor tabs from previous session on startup or when editor is shown again after closing</source>
        <translation>Atkurti vėliausiai naudotas tvarkyklės korteles, kai Octave naujai paleidžiama arba kai tampa matomas tvarkyklės langas</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Legacy Mac (CR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Close all files when the editor widget is closed/hidden</source>
        <translation>Uždaryti visas bylas, kai uždaroma/išjungiama tvarkyklė</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Force newline at end when saving file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove trailing spaces when saving file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select font, font size (as a difference from the default size), font style (&lt;b&gt;b&lt;/b&gt;old, &lt;b&gt;i&lt;/b&gt;talic, &lt;b&gt;u&lt;/b&gt;nderline), text color, and background color (for the latter, the color magenta (255,0,255) is a placeholder for the default background color).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pasirinkite šriftą, šrifto dydį (kaip skirtumą nuo numatytojo dydžio), šrifto stilių (pa&lt;b&gt;r&lt;/b&gt;yškintas, pa&lt;b&gt;s&lt;/b&gt;viręs, pa&lt;b&gt;b&lt;/b&gt;rauktas), teksto ir fono spalvą (šviesi alyvinė spalva (255,0,255) nurodo, kad naudoti numatytąją spalvą).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Behavior</source>
        <translation>Elgsena</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Restore last directory of previous session</source>
        <translation>Atkurti vėliausiai naudotą aplanką</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Initial file browser directory (only if not synchronized with initial working directory of Octave)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Extensions of files to be opened in the default text editor (separated by &quot;;&quot;):</source>
        <translation>Bylų, kurios atveriamos numatytaja teksto tvarkykle, plėtiniai (atskiriami kabliataškiu, t.y. &quot;;&quot;):</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Workspace</source>
        <translation>Aplinkos kintamieji</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Colors for variable attributes</source>
        <translation>Kintamųjų savybių spalvos</translation>
    </message>
    <message>
        <location line="+287"/>
        <source>Import shortcut set</source>
        <translation>Įkelti sparčiųjų klavišų rinkinius</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Export current shortcut set</source>
        <translation>Išsaugoti esamus sparčiųjų klavišų rinkinius</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reset shortcuts to their defaults</source>
        <translation>Naudoti numatytuosius sparčiųjų klavišų rinkinius</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+80"/>
        <source>Default</source>
        <translation>Numatytasis</translation>
    </message>
    <message>
        <location line="+232"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ok&lt;/span&gt; - close dialog and apply settings&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Apply - &lt;/span&gt;apply settings but leave dialog open&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Cancel - &lt;/span&gt;close dialog and discard changes not yet applied&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Reset -&lt;/span&gt; reload settings discarding changes not yet applied&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-227"/>
        <source>Actual</source>
        <translation>Einamasis</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Network</source>
        <translation>Tinklas</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Allow Octave to connect to the Octave web site to display current news and information</source>
        <translation>Leisti prisijungti prie Octave svetainės, kad gauti naujausius duomenis</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Proxy Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select &lt;span style=&quot; font-style:italic;&quot;&gt;HttpProxy&lt;/span&gt;, &lt;span style=&quot; font-style:italic;&quot;&gt;Sock5Proxy&lt;/span&gt; or &lt;span style=&quot; font-style:italic;&quot;&gt;Environment Variables&lt;/span&gt;. With the last selection, the proxy is taken from the first non-empty environment variable ALL_PROXY, HTTP_PROXY or HTTPS_PROXY .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Use proxy server</source>
        <translation>Naudoti proxy serverį</translation>
    </message>
    <message>
        <location line="-523"/>
        <source>Variable Editor</source>
        <translation>Kintamųjų tvarkyklė</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Default row height</source>
        <translation>Numatytasis eilutės auktšis</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Default column width</source>
        <translation>Numatytasis stulpelio plotis</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Use Command Window font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Variable Editor Colors</source>
        <translation>Kintamųjų tvarkyklės spalvos</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Use alternating row colors</source>
        <translation>Kas antrą eilutę vaizduoti su skirtinga fono spalva</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Disable global shortcuts in order to prevent
interference with readline key strokes.
Exceptions: Ctrl-C for interrupting the interpreter
and the shortcuts for switching to other widgets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Disable global shortcuts when Command Window has focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-392"/>
        <source>Synchronize Octave working directory with file browser</source>
        <translation>Visada suvienodinti Octave darbo aplanką su einamuoju bylų naršyklės aplanku</translation>
    </message>
    <message>
        <location line="+348"/>
        <source>Shortcuts</source>
        <translation>Sparčiųjų klavišų rinkiniai</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Export</source>
        <translation>Išsaugoti</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Import</source>
        <translation>Įkelti</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Disable menu accelerators in order to prevent
interference with readline key strokes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Disable menu accelerators of main window menus when Command Window has focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Edit a shortcut by double-clicking in Actual column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Action</source>
        <translation>Veiksmas</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Hostname:</source>
        <translation>Kompiuterio vardas:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Proxy type:</source>
        <translation>Proxy tipas:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Port:</source>
        <translation>Portas:</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Username:</source>
        <translation>Vartotojo vardas:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Password:</source>
        <translation>Slaptažodis:</translation>
    </message>
</context>
<context>
    <name>shortcuts</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-sc.cc" line="+227"/>
        <source>Undock/Dock Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New File</source>
        <translation type="unfinished">Nauja byla</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Function</source>
        <translation type="unfinished">Nauja funkcija</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Figure</source>
        <translation type="unfinished">Naujas braižymo langas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open File</source>
        <translation type="unfinished">Atidaryti bylą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Load Workspace</source>
        <translation type="unfinished">Įkelti aplinkos kintamuosius</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save Workspace As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exit Octave</source>
        <translation type="unfinished">Uždaryti Octave</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy</source>
        <translation type="unfinished">Kopijuoti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste</source>
        <translation type="unfinished">Įklijuoti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Undo</source>
        <translation type="unfinished">Atšaukti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select All</source>
        <translation type="unfinished">Pažymėti viską</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Clipboard</source>
        <translation type="unfinished">Išvalyti atminties mainų sritį (clipboard)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find in Files</source>
        <translation type="unfinished">Bylose rasti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Command Window</source>
        <translation type="unfinished">Išvalyti komandų langą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Command History</source>
        <translation type="unfinished">Išvalyti įvykdytų komandų sąrašą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Workspace</source>
        <translation type="unfinished">Ištrinti aplinkos kintamuosius</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+119"/>
        <source>Preferences</source>
        <translation type="unfinished">Nustatymai</translation>
    </message>
    <message>
        <location line="-116"/>
        <source>Step</source>
        <translation type="unfinished">Žengti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Step In</source>
        <translation type="unfinished">Žengti į vidų</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Step Out</source>
        <translation type="unfinished">Žengti į išorę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Continue</source>
        <translation type="unfinished">Tęsti scenarijau vykdymą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit Debug Mode</source>
        <translation type="unfinished">Išeiti iš derinimo būvio</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start/Stop Profiler Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resume Profiler Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Profile Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show Command Window</source>
        <translation type="unfinished">Rodyti komandų langą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Command History</source>
        <translation type="unfinished">Rodyti įvykdytų komandų langą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show File Browser</source>
        <translation type="unfinished">Rodyti bylų naršyklę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Workspace</source>
        <translation type="unfinished">Rodyti aplinkos kintamųjų langą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Editor</source>
        <translation type="unfinished">Rodyti tvarkyklės langą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Documentation</source>
        <translation type="unfinished">Rodyti dokumentacijos langą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Variable Editor</source>
        <translation type="unfinished">Rodyti kintamųjų tvarkyklės langą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command Window</source>
        <translation type="unfinished">Komandų langas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command History</source>
        <translation type="unfinished">Įvykdytos komandos</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File Browser</source>
        <translation type="unfinished">Bylų naršyklė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Workspace</source>
        <translation type="unfinished">Aplinkos kintamieji</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Editor</source>
        <translation type="unfinished">Tvarkyklė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Documentation</source>
        <translation type="unfinished">Dokumentacija</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Variable Editor</source>
        <translation type="unfinished">Kintamųjų tvarkyklė</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset Default Window Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show On-disk Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Online Documentation</source>
        <translation type="unfinished">Rodyti internete esančią dokumentaciją</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Report Bug</source>
        <translation type="unfinished">Pranešti apie klaidą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Octave Packages</source>
        <translation type="unfinished">Octave paketai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contribute to Octave</source>
        <translation type="unfinished">Prisidėti prie Octave</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Octave Developer Resources</source>
        <translation type="unfinished">Octave kūrėjų ištekliai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About Octave</source>
        <translation type="unfinished">Apie Octave</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Release Notes</source>
        <translation type="unfinished">Octave laidos pastabos</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Community News</source>
        <translation type="unfinished">Bendruomenės naujienos</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Close Tab</source>
        <translation type="unfinished">Uždaryti kortelę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close All Tabs</source>
        <translation type="unfinished">Uždaryti visas korteles</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close Other Tabs</source>
        <translation type="unfinished">Uždaryti kitas korteles</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch to Left Tab</source>
        <translation type="unfinished">Pereiti į kairiąją kortelę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch to Right Tab</source>
        <translation type="unfinished">Pereiti į dešiniąją kortelę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Tab Left</source>
        <translation type="unfinished">Perstumti kortelę į kairę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Tab Right</source>
        <translation type="unfinished">Perstumti kortelę į dešinę</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom In</source>
        <translation type="unfinished">Padidinti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom Out</source>
        <translation type="unfinished">Sumažinti</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Zoom Normal</source>
        <translation type="unfinished">Įprastas mastelis</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Edit Function</source>
        <translation type="unfinished">Tvarkyti funkciją</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save File</source>
        <translation type="unfinished">Išsaugoti  bylą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save File As</source>
        <translation type="unfinished">Išsaugoti bylą kaip</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation type="unfinished">Spausdinti</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Redo</source>
        <translation type="unfinished">Naikinti atšaukimą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut</source>
        <translation type="unfinished">Iškirpti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find and Replace</source>
        <translation type="unfinished">Surasti ir pakeisti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Next</source>
        <translation type="unfinished">Surasti sekantį</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Previous</source>
        <translation type="unfinished">Surasti ankstesnį</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to Start of Word</source>
        <translation type="unfinished">Trinti iki žodžio pradžios</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to End of Word</source>
        <translation type="unfinished">Trinti iki žodžio pabaigos</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to Start of Line</source>
        <translation type="unfinished">Trinti iki eilutės pradžios</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to End of Line</source>
        <translation type="unfinished">Trinti iki eilutės pabaigos</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Line</source>
        <translation type="unfinished">Kopijuoti eilutę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut Line</source>
        <translation type="unfinished">Iškirpti eilutę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Duplicate Selection/Line</source>
        <translation type="unfinished">Dvigubinti pažymėtą sritį/eilutę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transpose Line</source>
        <translation type="unfinished">Sukeisti eilutes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Completion List</source>
        <translation type="unfinished">Rodyti automatinio užbaigimo sąrašą</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment Selection</source>
        <translation type="unfinished">Užkomentuoti pažymėtą sritį</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uncomment Selection</source>
        <translation type="unfinished">Atkomentuoti pažymėtą sritį</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment Selection (Choosing String)</source>
        <translation type="unfinished">Užkomentuoti pažymėtą sritį nurodant komentavimo ženklą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uppercase Selection</source>
        <translation type="unfinished">Pažymėtos srities raides pakeisti didžiosiomis raidėmis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Lowercase Selection</source>
        <translation type="unfinished">Pažymėtos srities raides pakeisti mažosiomis raidėmis</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Indent Selection Rigidly</source>
        <translation type="unfinished">Pažymėtą sritį padaryti su įtraukomis</translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+3"/>
        <source>Unindent Selection Rigidly</source>
        <translation type="unfinished">Šalinti pažymėtos srities įtraukas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Indent Code</source>
        <translation type="unfinished">Kodą padaryti su įtraukomis</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Convert Line Endings to Windows</source>
        <translation type="unfinished">Eilučių pabaigos ženklus perversti į Wondows formatą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Convert Line Endings to Unix</source>
        <translation type="unfinished">Eilučių pabaigos ženklus perversti į Unix formatą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Convert Line Endings to Mac</source>
        <translation type="unfinished">Eilučių pabaigos ženklus perversti į Mac formatą</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Goto Line</source>
        <translation type="unfinished">Pereiti į eilutę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to Matching Brace</source>
        <translation type="unfinished">Pereiti į priešingą skliaustelių poros pusę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select to Matching Brace</source>
        <translation type="unfinished">Pažymėti skliaustelių poros turinį</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle Bookmark</source>
        <translation type="unfinished">Pridėti/nuimti žymelę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Bookmark</source>
        <translation type="unfinished">Sekanti eilutė su žymele</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Bookmark</source>
        <translation type="unfinished">Ankstesnė eilutė su žymele</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove All Bookmark</source>
        <translation type="unfinished">Pašalinti visas žymeles</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Styles Preferences</source>
        <translation type="unfinished">Vaizdavimo nustatymai</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Line Numbers</source>
        <translation type="unfinished">Rodyti eilučių numerius</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Whitespace Characters</source>
        <translation type="unfinished">Rodyti tarpo ženklą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Line Endings</source>
        <translation type="unfinished">Rodyti eilutės pabaigos ženklus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Indentation Guides</source>
        <translation type="unfinished">Rodyti įtraukų linijas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Long Line Marker</source>
        <translation type="unfinished">Rodyti ilgos eilutės pabaigos žymę</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Toolbar</source>
        <translation type="unfinished">Rodyti įrankių juostą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Statusbar</source>
        <translation type="unfinished">Rodyti būvio juostą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Horizontal Scrollbar</source>
        <translation type="unfinished">Rodyti horizontalią slinkties juostą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort Tabs Alphabetically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Toggle Breakpoint</source>
        <translation type="unfinished">Pridėti/nuimti sustojimo tašką</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Breakpoint</source>
        <translation type="unfinished">Sekantis sustojimo taškas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Breakpoint</source>
        <translation type="unfinished">Ankstesnis sustojimo taškas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove All Breakpoints</source>
        <translation type="unfinished">Pašalinti visus sustojimo taškus</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run File</source>
        <translation type="unfinished">Vykdyti bylą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Selection</source>
        <translation type="unfinished">Vykdyti pažymėtą kodą</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Tests</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Demos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help on Keyword</source>
        <translation type="unfinished">Pagalba pagal raktinį žodį</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document on Keyword</source>
        <translation type="unfinished">Dokumentacija pagal raktinį žodį</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Go to Homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go Back one Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go Forward one Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bookmark this Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
