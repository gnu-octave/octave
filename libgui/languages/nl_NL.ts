<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/files-dock-widget.cc" line="+141"/>
        <source>Invalid filename</source>
        <translation>Ongeldige bestandsnaam</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.cc" line="+1045"/>
        <source>Difference to the default size</source>
        <translation>Verschil met standaardgrootte</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Background color, magenta (255, 0, 255) means default</source>
        <translation>Achtergrondkleur; roze (255,0,255) staat voor standaardkleur}</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>b</source>
        <comment>short form for bold</comment>
        <translation>d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>i</source>
        <comment>short form for italic</comment>
        <translation>s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>u</source>
        <comment>short form for underlined</comment>
        <translation>o</translation>
    </message>
</context>
<context>
    <name>QTerminal</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/QTerminal.cc" line="+119"/>
        <source>Edit &quot;%1&quot;</source>
        <translation>Bewerk &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help on &quot;%1&quot;</source>
        <translation>Hulp over &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Documentation on &quot;%1&quot;</source>
        <translation>Documentatie over &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+165"/>
        <source>Copy</source>
        <translation>Kopiëer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste</source>
        <translation>Plak</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>Selecteer Alles</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run Selection</source>
        <translation>Voer selectie uit</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit selection</source>
        <translation>Bewerk selectie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help on selection</source>
        <translation>Hulp over selectie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Documentation on selection</source>
        <translation>Documentatie over selectie</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Clear Window</source>
        <translation>Veeg venster schoon</translation>
    </message>
    <message>
        <location line="-233"/>
        <source>Edit %1 at line %2</source>
        <translation>Bewerk %1 op regel %2</translation>
    </message>
</context>
<context>
    <name>QWinTerminalImpl</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/win32/QWinTerminalImpl.cpp" line="+1859"/>
        <source>copied selection to clipboard</source>
        <translation>selectie is naar klembord gekopieerd</translation>
    </message>
</context>
<context>
    <name>QsciLexerBash</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerbash.cpp" line="+203"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Getal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Tekst tussen dubbele aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Tekst tussen aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Identifier</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scalar</source>
        <translation>Scalair getal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Parameter expansion</source>
        <translation>Geëxpandeerde parameter</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks</source>
        <translation>Backticks</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Here document delimiter</source>
        <translation>Here document scheidingsteken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted here document</source>
        <translation>Here document tussen aanhalingstekens</translation>
    </message>
</context>
<context>
    <name>QsciLexerBatch</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerbatch.cpp" line="+174"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide command character</source>
        <translation>Verberg opdrachtteken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>External command</source>
        <translation>Externe opdracht</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable</source>
        <translation>Variabele</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
</context>
<context>
    <name>QsciLexerCPP</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexercpp.cpp" line="+364"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive default</source>
        <translation>Inactieve standaardfont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C comment</source>
        <translation>C commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C comment</source>
        <translation>Inactief C commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C++ comment</source>
        <translation>C++ commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C++ comment</source>
        <translation>Inactief C++ commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style C comment</source>
        <translation>C commentaar in JavaDoc-stijl</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style C comment</source>
        <translation>Inactief C commentaar in JavaDoc-stijl</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Getal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive number</source>
        <translation>Inactief getal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive keyword</source>
        <translation>Inactief trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Tekst tussen dubbele aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive double-quoted string</source>
        <translation>Inactieve tekst tussen dubbele aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Tekst tussen aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive single-quoted string</source>
        <translation>Inactieve tekst tussen aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>IDL UUID</source>
        <translation>IDL UUID</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive IDL UUID</source>
        <translation>Inactieve IDL UUID</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pre-processor block</source>
        <translation>Preprocessor blok</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive pre-processor block</source>
        <translation>Inactief preprocessor blok</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive operator</source>
        <translation>Inactieve operator</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Identifier</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive identifier</source>
        <translation>Inactieve identifier</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unclosed string</source>
        <translation>Niet afgesloten tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive unclosed string</source>
        <translation>Inactieve niet afgesloten tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C# verbatim string</source>
        <translation>C# letterlijke tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C# verbatim string</source>
        <translation>Inactieve C# letterlijke tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaScript regular expression</source>
        <translation>Javascript reguliere expressie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaScript regular expression</source>
        <translation>Inactieve Javascript reguliere expressie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style C++ comment</source>
        <translation>C++ commentaar in JavaDoc-stijl</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style C++ comment</source>
        <translation>Inactief C++ commentaar in JavaDoc-stijl</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Secondary keywords and identifiers</source>
        <translation>Secundaire trefwoorden en identifiers</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive secondary keywords and identifiers</source>
        <translation>Inactieve secundaire trefwoorden en identifiers</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc keyword</source>
        <translation>JavaDoc sleutelwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc keyword</source>
        <translation>Inactief JavaDoc sleutelwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc keyword error</source>
        <translation>JavaDoc fout trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc keyword error</source>
        <translation>Inactief fout JavaDoc trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Global classes and typedefs</source>
        <translation>Globale klassen en typedefs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive global classes and typedefs</source>
        <translation>Inactieve globale klassen en typedefs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C++ raw string</source>
        <translation>C++ ruwe tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C++ raw string</source>
        <translation>Inactieve C++ ruwe tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vala triple-quoted verbatim string</source>
        <translation>Vala tekst tussen driedubbele aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive Vala triple-quoted verbatim string</source>
        <translation>Inactieve Vala tekst tussen driedubbele aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pike hash-quoted string</source>
        <translation>Pike tekst tussen hashtekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive Pike hash-quoted string</source>
        <translation>Inactieve Pike tekst tussen hashtekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pre-processor C comment</source>
        <translation>Pre-processor C-commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive pre-processor C comment</source>
        <translation>Inactief pre-processor C-commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style pre-processor comment</source>
        <translation>Javadoc-stijl preprocessor-commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style pre-processor comment</source>
        <translation>Inactief Javadoc-stijl preprocessor-commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>User-defined literal</source>
        <translation>Door gebruiker gedefinieerde tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive user-defined literal</source>
        <translation>Inactieve door gebruiker gedefinieerde tekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Task marker</source>
        <translation>Taakmarkering</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive task marker</source>
        <translation>Inactieve taakmarkering</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Escape sequence</source>
        <translation>Stuurcode</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive escape sequence</source>
        <translation>Inactieve stuurcode</translation>
    </message>
</context>
<context>
    <name>QsciLexerDiff</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerdiff.cpp" line="+106"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command</source>
        <translation>Opdracht</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Header</source>
        <translation>Koptekst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed line</source>
        <translation>Verwijderde regel</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added line</source>
        <translation>Toegevoegde regel</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Changed line</source>
        <translation>Gewijzigde regel</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added adding patch</source>
        <translation>Toegevoegde uitbreidingspatch</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed adding patch</source>
        <translation>Verwijderde uitbreidingspatch</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added removing patch</source>
        <translation>Toegevoegde verwijderpatch</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed removing patch</source>
        <translation>Verwijderde verwijderpatch</translation>
    </message>
</context>
<context>
    <name>QsciLexerMatlab</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexermatlab.cpp" line="+133"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command</source>
        <translation>Opdracht</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Getal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Tekst tussen aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Identifier</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Tekst tussen dubbele aanhalingstekens</translation>
    </message>
</context>
<context>
    <name>QsciLexerPerl</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerperl.cpp" line="+328"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>POD</source>
        <translation>POD</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Getal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Trefwoord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Tekst tussen dubbele aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Tekst tussen aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Identifier</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scalar</source>
        <translation>Scalair getal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Array</source>
        <translation>Array</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hash</source>
        <translation>Hash</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol table</source>
        <translation>Symbooltabel</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Regular expression</source>
        <translation>Reguliere expressie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Substitution</source>
        <translation>Vervanging</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks</source>
        <translation>Backticks</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data section</source>
        <translation>Data sectie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Here document delimiter</source>
        <translation>Here document scheidingsteken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted here document</source>
        <translation>Here document tussen aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted here document</source>
        <translation>Here document tussen dubbele aanhalingstekens</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backtick here document</source>
        <translation>Backtick here document</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (q)</source>
        <translation>Tekst tussen aanhalingstekens (q)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qq)</source>
        <translation>Tekst tussen aanhalingstekens (qq)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qx)</source>
        <translation>Tekst tussen aanhalingstekens (qx)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qr)</source>
        <translation>Tekst tussen aanhalingstekens (qr)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qw)</source>
        <translation>Tekst tussen aanhalingstekens (qw)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>POD verbatim</source>
        <translation>POD letterlijk</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Subroutine prototype</source>
        <translation>Subroutine prototype</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Format identifier</source>
        <translation>Sjabloon identifier</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Format body</source>
        <translation>Sjabloon inhoud</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string (interpolated variable)</source>
        <translation>Tekst tussen dubbele aanhalingstekens (geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Translation</source>
        <translation>Vertaling</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Regular expression (interpolated variable)</source>
        <translation>Reguliere expressie (geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Substitution (interpolated variable)</source>
        <translation>Vervanging (geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks (interpolated variable)</source>
        <translation>Backticks (geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted here document (interpolated variable)</source>
        <translation>Here document tussen dubbele aanhalingstekens (geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backtick here document (interpolated variable)</source>
        <translation>Backtick here document (geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qq, interpolated variable)</source>
        <translation>Tekst tussen aanhalingstekens (qq, geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qx, interpolated variable)</source>
        <translation>Tekst tussen aanhalingstekens (qx, geïnterpoleerde variabele)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qr, interpolated variable)</source>
        <translation>Tekst tussen aanhalingstekens (qr, geïnterpoleerde variabele)</translation>
    </message>
</context>
<context>
    <name>QsciScintilla</name>
    <message>
        <location filename="build_ts/octave-qsci/qsciscintilla.cpp" line="+4478"/>
        <source>&amp;Undo</source>
        <translation>&amp;Ongedaan maken</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>He&amp;rhalen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cu&amp;t</source>
        <translation>Kni&amp;ppen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiëren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>&amp;Plakken</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>Selecteer Alles</translation>
    </message>
</context>
<context>
    <name>UrlFilter</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/unix/Filter.cpp" line="+630"/>
        <source>Open Link</source>
        <translation>Open koppeling</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Link Address</source>
        <translation>Kopieer koppelingadres</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Send Email To...</source>
        <translation>Stuur email naar ...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Email Address</source>
        <translation>Kopieer emailadres</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Edit %1 at line %2</source>
        <translation>Bewerk %1 op regel %2</translation>
    </message>
</context>
<context>
    <name>annotation_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/graphics/annotation-dialog.ui" line="+17"/>
        <source>Annotation</source>
        <translation>Annotatie</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>String</source>
        <translation>String</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Font</source>
        <translation>Lettertype</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>bold</source>
        <translation>vetgedrukt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>b</source>
        <translation>v</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>italic</source>
        <translation>schuin</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>i</source>
        <translation>s</translation>
    </message>
    <message>
        <location line="+428"/>
        <source>color</source>
        <translation>kleur</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>left</source>
        <translation>links</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+29"/>
        <source>middle</source>
        <translation>midden</translation>
    </message>
    <message>
        <location line="-24"/>
        <source>right</source>
        <translation>rechts</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Horizontal alignment</source>
        <translation>horizontale uitlijning</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Vertical alignment</source>
        <translation>verticale uitlijning</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>top</source>
        <translation>boven</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>bottom</source>
        <translation>onder</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Width</source>
        <translation>Breedte</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Height</source>
        <translation>Hoogte</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>units</source>
        <translation>eenheden</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>normalized</source>
        <translation>genormeerd</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Fit to box</source>
        <translation>Pas in rechthoek</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Box</source>
        <translation>Rechthoek</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Background</source>
        <translation>Achtergrond</translation>
    </message>
    <message>
        <location line="+444"/>
        <source>Edge</source>
        <translation>Rand</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Line style</source>
        <translation>Lijnstijl</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>none</source>
        <translation>geen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Line width</source>
        <translation>Lijnbreedte</translation>
    </message>
</context>
<context>
    <name>octave::ListDialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/dialog.cc" line="+454"/>
        <source>Select All</source>
        <translation>Selecteer Alles</translation>
    </message>
</context>
<context>
    <name>octave::command_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/command-widget.cc" line="+76"/>
        <source>Pause</source>
        <translation>Pauzeer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Continue</source>
        <translation>Hervatten</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Command Widget</source>
        <translation>Opdracht Widget</translation>
    </message>
</context>
<context>
    <name>octave::community_news</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/community-news.cc" line="+70"/>
        <source>Octave Community News</source>
        <translation>Octave Community Nieuws</translation>
    </message>
</context>
<context>
    <name>octave::console_lexer</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/console-lexer.cc" line="+48"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prompt</source>
        <translation>Prompt</translation>
    </message>
</context>
<context>
    <name>octave::documentation</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation.cc" line="+135"/>
        <location line="+14"/>
        <location line="+715"/>
        <source>Octave Documentation</source>
        <translation>Octave Documentatie</translation>
    </message>
    <message>
        <location line="-728"/>
        <source>Could not copy help collection to temporary
file. Search capabilities may be affected.
%1</source>
        <translation>Kon de help collectie niet naar een tijdelijk
bestand kopiëren. De zoekfunctie kan beïnvloed zijn.
%1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Could not setup the data required for the
documentation viewer. Maybe the Qt SQlite
module is missing?
Only help text in the Command Window will
be available.</source>
        <translation>kon de data voor documentatie browser niet
installeren, misschien ontbreekt de Qt SQlite
module?
Alleen de hulptekst in het commando venster
is beschikbaar.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Contents</source>
        <translation>Inhoud</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter text to search function index</source>
        <translation>Geef tekst op om in functie-index te zoeken</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+51"/>
        <source>Search</source>
        <translation>Zoek</translation>
    </message>
    <message>
        <location line="-35"/>
        <source>Function Index</source>
        <translation>Functie-index</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Bookmarks</source>
        <translation>Bladwijzers</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Go home</source>
        <translation>Naar begin</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Go back</source>
        <translation>Ga terug</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Previous pages</source>
        <translation>Voorgaande pagina&apos;s</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Go forward</source>
        <translation>Ga vooruit</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Next pages</source>
        <translation>Volgende pagina&apos;s</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Find</source>
        <translation>Zoek</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Zoom In</source>
        <translation>Vergroten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom Out</source>
        <translation>Verkleinen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom Original</source>
        <translation>Naar begingrootte</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Bookmark current page</source>
        <translation>Maak bladwijzer naar huidige pagina</translation>
    </message>
    <message>
        <location line="+424"/>
        <source>Unable to register help file %1.</source>
        <translation>Kan helpbestand %1 niet registreren.</translation>
    </message>
</context>
<context>
    <name>octave::documentation_bookmarks</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation-bookmarks.cc" line="+102"/>
        <source>
No documentation bookmarks loaded!</source>
        <translation>
Geen documentatiebladwijzers geladen!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Octave: Loading Documentation Bookmarks</source>
        <translation>Octave: documentatie bladwijzers inlezen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter text to search the bookmarks</source>
        <translation>Geef zoektekst in bladwijzers op</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>New Folder</source>
        <translation>Nieuwe map</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>&amp;Open</source>
        <translation>&amp;Open</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Rename</source>
        <translation>He&amp;rnoem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remo&amp;ve</source>
        <translation>&amp;Verwijder</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Add Folder</source>
        <translation>M&amp;ap toevoegen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Hide &amp;Filter</source>
        <translation>Verberg &amp;filter</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show &amp;Filter</source>
        <translation>Toon &amp;filter</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>Octave: Saving Documentation Bookmarks</source>
        <translation>Octave: Documentatiebladwijzers bewaren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to write file %1:
%2.

Documentation bookmarks are not saved!
</source>
        <translation>Kan bestand %1 niet beschrijven:
%2.

Documentatie bladwijzers niet opgeslagen!
</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Unable to read file %1:
%2.</source>
        <translation>Kan bestand %1 niet lezen:
%2.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No start element found in %1.
Invalid bookmark file?</source>
        <translation>Geen beginelement gevonden in %1.
Ongeldig bladwijzerbestand?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The file
%1
is not a valid XBEL file version 1.0.</source>
        <translation>Het bestand 
%1
is geen geldig XBEL versie 1.0 bestand.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Unknown title</source>
        <translation>Onbekende titel</translation>
    </message>
</context>
<context>
    <name>octave::documentation_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation-dock-widget.cc" line="+40"/>
        <source>Documentation</source>
        <translation>Documentatie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>See the documentation for help.</source>
        <translation>Kijk in de documentatie voor hulp.</translation>
    </message>
</context>
<context>
    <name>octave::dw_main_window</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/dw-main-window.cc" line="+53"/>
        <source>&amp;Close</source>
        <translation>&amp;Sluit bestand</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close &amp;All</source>
        <translation>Sluit &amp;Alle</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close &amp;Other</source>
        <translation>Sluit &amp;Andere</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Switch to &amp;Left Widget</source>
        <translation>Schakel naar &amp;Linker widget</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Switch to &amp;Right Widget</source>
        <translation>Schakel naar &amp;Rechter widget</translation>
    </message>
</context>
<context>
    <name>octave::external_editor_interface</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/external-editor-interface.cc" line="+68"/>
        <location line="+50"/>
        <source>Octave Editor</source>
        <translation>Octave Editor</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Could not start custom file editor
%1</source>
        <translation>Kon externe editor niet starten
%1</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>There is no custom editor configured yet.
Do you want to open the preferences?</source>
        <translation>Er is nog geen externe editor ingesteld.
Wilt u de voorkeursinstellingen aanpassen?</translation>
    </message>
</context>
<context>
    <name>octave::file_editor</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/file-editor.cc" line="+194"/>
        <source>Continue</source>
        <translation>Doorgaan</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Save File and Run</source>
        <translation>Sla Bestand op en Voer uit</translation>
    </message>
    <message>
        <location line="+1002"/>
        <location line="+29"/>
        <location line="+565"/>
        <location line="+18"/>
        <location line="+25"/>
        <source>Octave Editor</source>
        <translation>Octave Editor</translation>
    </message>
    <message>
        <location line="-636"/>
        <source>File not saved! A file with the selected name
%1
is already open in the editor.</source>
        <translation>Bestand niet opgeslagen! Een bestand met de geselecteerde naam
%1
is al geopend in the editor.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The associated file editor tab has disappeared.</source>
        <translation>Het bijbehorende editortabblad is verdwenen.</translation>
    </message>
    <message>
        <location line="+565"/>
        <source>Could not open file
%1
for reading: %2.</source>
        <translation>kon bestand
%1
niet openen voor lezen: %2.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>File
%1
does not exist. Do you want to create it?</source>
        <translation>Bestand
%1
bestaat niet. Wilt u het aanmaken?</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Create</source>
        <translation>Maak nieuw</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not open file
%1
for writing: %2.</source>
        <translation>Kon bestand
%1
niet openen voor schrijven: %2.</translation>
    </message>
    <message>
        <location line="+271"/>
        <source>&amp;File</source>
        <translation>&amp;Bestand</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Recent Editor Files</source>
        <translation>&amp;Recent geopende bestanden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Edit Function</source>
        <translation>&amp;Pas Functie Aan</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Save File</source>
        <translation>Op&amp;slaan</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save File &amp;As...</source>
        <translation>Opslaan &amp;als...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Close</source>
        <translation>&amp;Sluit bestand</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close All</source>
        <translation>Alle bestanden sluiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close Other Files</source>
        <translation>Andere bestanden sluiten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Print...</source>
        <translation>Afdrukken...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Edit</source>
        <translation>B&amp;ewerken</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>He&amp;rhalen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>Kni&amp;ppen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Find and Replace...</source>
        <translation>&amp;Zoek en Vervang...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find &amp;Next</source>
        <translation>Zoek &amp;volgende</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Find &amp;Previous</source>
        <translation>zoek Vori&amp;ge</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Commands</source>
        <translation>&amp;Opdrachten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete Line</source>
        <translation>Verwijder regel</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy Line</source>
        <translation>Kopieer regel</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cut Line</source>
        <translation>Knip regel</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete to Start of Word</source>
        <translation>Wis tot begin van woord</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to End of Word</source>
        <translation>Wis tot einde van woord</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to Start of Line</source>
        <translation>Wis tot begin van regel</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to End of Line</source>
        <translation>Wis tot einde van regel</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Duplicate Selection/Line</source>
        <translation>Dubbele Selectie/Regel</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transpose Line</source>
        <translation>Transponeer regel</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Show Completion List</source>
        <translation>&amp;Toon aanvullijst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Format</source>
        <translation>&amp;Formaat</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Uppercase Selection</source>
        <translation>&amp;Hoofdletters</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Lowercase Selection</source>
        <translation>Kleine &amp;letters</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Comment</source>
        <translation>Zet om naar &amp;commentaar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Uncomment</source>
        <translation>Zet om &amp;naar code</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Comment (Choosing String)</source>
        <translation>Commentaar (kies string)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Indent Selection Rigidly</source>
        <translation>Laat selectie &amp;inspringen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Unindent Selection Rigidly</source>
        <translation>Laat selectie &amp;terug inspringen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Indent Code</source>
        <translation>Code laten inspringen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Convert Line Endings to &amp;Windows (CRLF)</source>
        <translation>Zet regeleindes om naar &amp;Windows (CRLF)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Convert Line Endings to &amp;Unix (LF)</source>
        <translation>Zet regeleindes om naar &amp;Unix (LF)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Convert Line Endings to Legacy &amp;Mac (CR)</source>
        <translation>Zet regeleindes om naar Legacy &amp;Mac (CR)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Navi&amp;gation</source>
        <translation>Navi&amp;geer</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Go &amp;to Line...</source>
        <translation>Ga naar &amp;regel...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Move to Matching Brace</source>
        <translation>Spring naar bijbehorende accolade</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select to Matching Brace</source>
        <translation>Selecteer tot bijbehorende accolade</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Next Bookmark</source>
        <translation>Volge&amp;nde bladwijzer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Pre&amp;vious Bookmark</source>
        <translation>&amp;Vorige bladwijzer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Toggle &amp;Bookmark</source>
        <translation>&amp;Bladwijzer invoegen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Remove All Bookmarks</source>
        <translation>Alle bladwijzers ve&amp;rwijderen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Preferences...</source>
        <translation>&amp;Voorkeuren...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Styles Preferences...</source>
        <translation>&amp;Stijlvoorkeuren...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;View</source>
        <translation>&amp;Weergave</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Editor</source>
        <translation>&amp;Editor</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show &amp;Line Numbers</source>
        <translation>Toon rege&amp;lnummers</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Whitespace Characters</source>
        <translation>Toon &amp;witruimte-tekens</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Line &amp;Endings</source>
        <translation>Toon &amp;regeleindes</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Indentation Guides</source>
        <translation>Toon &amp;inspringraster</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Long Line &amp;Marker</source>
        <translation>Toon lange-regel &amp;markering</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;Toolbar</source>
        <translation>Toon &amp;Taakbalk</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Statusbar</source>
        <translation>Toon &amp;Statusbalk</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Horizontal Scrollbar</source>
        <translation>Toon &amp;Horizontale Schuifbalk</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Zoom &amp;In</source>
        <translation>&amp;Vergroten</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Zoom &amp;Out</source>
        <translation>&amp;Verkleinen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Normal Size</source>
        <translation>&amp;Normale grootte</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Sort Tabs Alphabetically</source>
        <translation>Tabs alfabetisch &amp;sorteren</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Debug</source>
        <translation>&amp;Debuggen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Toggle &amp;Breakpoint</source>
        <translation>Toggle &amp;onderbrekingspunt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Next Breakpoint</source>
        <translation>Volge&amp;nd onderbrekingspunt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pre&amp;vious Breakpoint</source>
        <translation>&amp;Vorig onderbrekingspunt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Remove All Breakpoints</source>
        <translation>Verwijde&amp;r alle onderbrekingspunten</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Run</source>
        <translation>Uitvoe&amp;ren</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save File and Run/Continue</source>
        <translation>Sla bestand op en voer uit/ga door</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Run &amp;Selection</source>
        <translation>Geselecteerde tekst &amp;uitvoeren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save File and Run All &amp;Tests</source>
        <translation>Sla bestand op en voer alle &amp;tests uit</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save File and Run All &amp;Demos</source>
        <translation>Sla bestand op en voer alle &amp;Demos uit</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Help</source>
        <translation>&amp;Hulp</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Help on Keyword</source>
        <translation>&amp;Hulp bij sleutelwoord</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Documentation on Keyword</source>
        <translation>&amp;Documentatie over sleutelwoord</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Recent Files</source>
        <translation>Recente bestanden</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Copy Full File &amp;Path</source>
        <translation>Volledig &amp;pad kopiëren</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
</context>
<context>
    <name>octave::file_editor_tab</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/file-editor-tab.cc" line="+162"/>
        <source>line:</source>
        <translation>regel:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>col:</source>
        <translation>kolom:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>encoding:</source>
        <translation>codering:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>eol:</source>
        <translation>eol:</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Breakpoint condition</source>
        <translation>Onderbrekingspunt voorwaarde</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>ERROR: </source>
        <translation>FOUT: </translation>
    </message>
    <message>
        <location line="+77"/>
        <location line="+1260"/>
        <location line="+152"/>
        <location line="+20"/>
        <location line="+447"/>
        <location line="+111"/>
        <location line="+103"/>
        <location line="+38"/>
        <location line="+60"/>
        <location line="+57"/>
        <location line="+36"/>
        <source>Octave Editor</source>
        <translation>Octave Editor</translation>
    </message>
    <message>
        <location line="-2283"/>
        <source>Cannot add breakpoint to modified or unnamed file.
Save and add breakpoint, or cancel?</source>
        <translation>Kan geen onderbrekingspunt instellen in een veranderd of nog niet benoemd bestand.
Opslaan en onderbrekingspunt toevoegen, of Terug?</translation>
    </message>
    <message>
        <location line="+924"/>
        <source>Goto line</source>
        <translation>Ga naar regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Line number</source>
        <translation>Regelnummer</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Comment selected text</source>
        <translation>Geselecteerde tekst omzetten naar commentaar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment string to use:
</source>
        <translation>Te gebruiken commentaar string:
</translation>
    </message>
    <message>
        <location line="+140"/>
        <location line="+57"/>
        <source>&lt;unnamed&gt;</source>
        <translation>&lt;naamloos&gt;</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Do you want to cancel closing, save, or discard the changes?</source>
        <translation>Wilt u het afsluiten afbreken, opslaan, of de wijzigingen negeren?</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The file

  %1

is about to be closed but has been modified.  %2</source>
        <translation>Het bestand

  %1

dat zou worden gesloten is ondertussen gewijzigd.  %2</translation>
    </message>
    <message>
        <location line="+152"/>
        <source>Unable to read file &apos;%1&apos;
with selected encoding &apos;%2&apos;: %3</source>
        <translation>Kan bestand &apos;%1&apos;
niet lezen met de geselecteerd karakter codering &apos;%2&apos;: %3</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>There were problems reading the file
%1
with the selected encoding %2.

Modifying and saving the file might cause data loss!</source>
        <translation>Er was een probleem met het lezen van bestand

%1
met de gekozen codering %2

Wijzigen en opslaan van het bestand kan leiden tot gegevensverlies!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Edit anyway</source>
        <translation>&amp;Bewerk evengoed</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+45"/>
        <source>Chan&amp;ge encoding</source>
        <translation>&amp;Verander codering</translation>
    </message>
    <message>
        <location line="-43"/>
        <location line="+36"/>
        <source>&amp;Close</source>
        <translation>&amp;Sluit bestand</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Select new default encoding</source>
        <translation>Kies nieuwe standaard codering</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Please select a new encoding
for reloading the current file.

This does not change the default encoding.
</source>
        <translation>Kies een andere codering
om het bestand opnieuw mee te lezen.

Dit heeft geen invloed op de standaard codering.
</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Debug or Save</source>
        <translation>Debug of Opslaan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This file is currently being executed.
Quit debugging and save?</source>
        <translation>Dit bestand wordt momenteel uitgevoerd.
Debuggen afsluiten en opslaan?</translation>
    </message>
    <message>
        <location line="+214"/>
        <source>Could not open file %1 for writing:
%2.</source>
        <translation>kon bestand %1 niet openen voor schrijven:
%2.</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>The changes could not be saved to the file
%1</source>
        <translation>De wijzigingen konden niet worden opgeslagen in bestand
%1</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Octave Files (*.m)</source>
        <translation>Octave bestanden (*.m)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>All Files (*)</source>
        <translation>Alle bestanden (*)</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>&quot;%1&quot;
is not a valid identifier.

If you keep this filename, you will not be able to
call your script using its name as an Octave command.

Do you want to choose another name?</source>
        <translation>&quot;%1&quot;
is geen geldige identifier.

Als je deze bestandsnaam houdt, kan je dit script 
niet aanroepen met z&apos;n naam als Octave commando.

Wil je een andere naam kiezen?</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>The current editor contents can not be encoded
with the selected encoding %1.
Using it would result in data loss!

Please select another one!</source>
        <translation>De huidige inhoud van de editor kan niet
worden gecodeerd met de huidige codering %1.
Toch toepassen zou leiden tot gegevensverlies!

Kies s.v.p.een andere encoding!</translation>
    </message>
    <message>
        <location line="-98"/>
        <source>%1
 already exists
Do you want to overwrite it?</source>
        <translation>%1
 bestaat al.
Wil je het overschrijven?</translation>
    </message>
    <message>
        <location line="+155"/>
        <source>It seems that &apos;%1&apos; has been modified by another application. Do you want to reload it?</source>
        <translation>Het lijkt erop dat &apos;%1&apos; is gewijzigd door een ander programma. Wil je het opnieuw laden?</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>

Warning: The contents in the editor is modified!</source>
        <translation>

Opgelet: Het bestand in de editor is gewijzigd!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>It seems that the file
%1
has been deleted or renamed. Do you want to save it now?%2</source>
        <translation>Het lijkt er op dat bestand
%1
is gewist of hernoemd. Wil je het nu opslaan?%2</translation>
    </message>
</context>
<context>
    <name>octave::files_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/files-dock-widget.cc" line="-4"/>
        <source>Could not rename file &quot;%1&quot; to &quot;%2&quot;.</source>
        <translation>Kon bestand &quot;%1&quot; niet hernoemen naar &quot;%2&quot;.</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>File Browser</source>
        <translation>Bestandsbrowser</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse your files</source>
        <translation>Verken bestanden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>File size</source>
        <translation>Bestandsgrootte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File type</source>
        <translation>Bestandstype</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date modified</source>
        <translation>Laatst gewijzigd</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show hidden</source>
        <translation>Toon verborgen bestanden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alternating row colors</source>
        <translation>Alternerende regelkleuren</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Enter the path or filename</source>
        <translation>Voer pad- of bestandsnaam in</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>One directory up</source>
        <translation>Eén mapniveau omhoog</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Octave directory</source>
        <translation>Toon Octave werkmap</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go to current Octave directory</source>
        <translation>Ga naar huidige Octave map</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Octave directory</source>
        <translation>Stel Octave map in</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set Octave directory to current browser directory</source>
        <translation>Stel huidige browsermap in als Octave map</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Actions on current directory</source>
        <translation>Bewerkingen op huidige map</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Home Directory</source>
        <translation>Toon home directory</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Set Browser Directory...</source>
        <translation>Stel browser map in...</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+438"/>
        <source>Find Files...</source>
        <translation>Zoek bestanden...</translation>
    </message>
    <message>
        <location line="-434"/>
        <location line="+446"/>
        <source>New File...</source>
        <translation>Nieuw bestand...</translation>
    </message>
    <message>
        <location line="-443"/>
        <location line="+445"/>
        <source>New Directory...</source>
        <translation>Nieuwe map...</translation>
    </message>
    <message>
        <location line="-390"/>
        <source>Double-click to open file/folder, right click for alternatives</source>
        <translation>Dubbelklik om map te openen, rechts-klik voor andere opties</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Rename...</source>
        <translation>Hernoemen...</translation>
    </message>
    <message>
        <location line="+317"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in System File Explorer</source>
        <translation>Openen in Systeem Bestandsverkenner</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in Text Editor</source>
        <translation>Openen in Tekstverwerker</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy Selection to Clipboard</source>
        <translation>Kopieer selectie naar klembord</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Run</source>
        <translation>Voer uit</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Load Data</source>
        <translation>Lees data in</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set Current Directory</source>
        <translation>Stel huidige map in</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add to Path</source>
        <translation>Aan zoekpad toevoegen</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+7"/>
        <source>Selected Directories</source>
        <translation>Gekozen directories</translation>
    </message>
    <message>
        <location line="-5"/>
        <location line="+7"/>
        <source>Selected Directories and Subdirectories</source>
        <translation>Gekozen directories en subdirectories</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Remove from Path</source>
        <translation>Uit zoekpad verwijderen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Delete...</source>
        <translation>Weggooien...</translation>
    </message>
    <message>
        <location line="+140"/>
        <location line="+11"/>
        <location line="+17"/>
        <source>Delete file/directory</source>
        <translation>Wis bestand/map</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Are you sure you want to delete all %1 selected files?
</source>
        <translation>Weet je zeker dat je alle %1 bestanden wil wissen?
</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Are you sure you want to delete
</source>
        <translation>Weet u zeker dat u dit wilt verwijderen
</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Can not delete a directory that is not empty</source>
        <translation>Kan een niet-lege map niet wissen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Deletion error</source>
        <translation>Fout bij verwijderen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Could not delete file &quot;%1&quot;.</source>
        <translation>Kon bestand &quot;%1&quot; niet verwijderen.</translation>
    </message>
    <message>
        <location line="+216"/>
        <source>Set directory of file browser</source>
        <translation>Stel file browser map in</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Create File</source>
        <translation>Maak nieuw bestand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Create file in
</source>
        <comment>String ends with 
!</comment>
        <translation>Maak nieuw bestand in
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New File.txt</source>
        <translation>Nieiw bestand.txt</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Create Directory</source>
        <translation>Maak nieuwe map</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Create folder in
</source>
        <comment>String ends with 
!</comment>
        <translation>Maak nieuwe map in
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New Directory</source>
        <translation>Nieuwe map</translation>
    </message>
</context>
<context>
    <name>octave::final_page</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="+318"/>
        <source>Enjoy!</source>
        <translation>Veel plezier!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Previous</source>
        <translation>Vorige</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Finish</source>
        <translation>Voltooien</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;We hope you find Octave to be a useful tool.&lt;/p&gt;
&lt;p&gt;If you encounter problems, there are a number of ways to get help, including commercial support options, a discussion board, a wiki, and other community-based support channels.
You can find more information about each of these by visiting &lt;a href=&quot;https://octave.org/support.html&quot;&gt;https://octave.org/support.html&lt;/a&gt; (opens in external browser).&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;
&lt;p&gt;We hopen dat u Octave nuttig vindt.&lt;/p&gt;
&lt;p&gt;Als u problemen ondervindt zijn er meerdere manieren om hulp te krijgen, waaronder commerciele opties, een discussiesite, een wiki en andere door de gebruikers gemeenschap ondersteunde platforms.
U kunt meer informatie over elk van deze mogelijkheden vinden op &lt;a href=&quot;https://octave.org/support.html&quot;&gt;https://octave.org/support.html&lt;/a&gt; (opent in externe browser).&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;For more information about Octave:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Visit &lt;a href=&quot;https://octave.org&quot;&gt;https://octave.org&lt;/a&gt; (opens in external browser)&lt;/li&gt;
&lt;li&gt;Get the documentation online in &lt;a href=&quot;https://www.gnu.org/software/octave/doc/interpreter/index.html&quot;&gt;HTML&lt;/a&gt; or &lt;a href=&quot;https://www.gnu.org/software/octave/octave.pdf&quot;&gt;PDF&lt;/a&gt; format (links open in external browser)&lt;/li&gt;
&lt;li&gt;Open the documentation browser of the Octave GUI with the help menu&lt;/li&gt;
&lt;/ul&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;Voor meer informatie over Octave:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Ga naar &lt;a href=&quot;https://octave.org&quot;&gt;https://octave.org&lt;/a&gt; (opent in externe browser)&lt;/li&gt;
&lt;li&gt;Bekijk de documentatie online&lt;a href=&quot;https://www.gnu.org/software/octave/doc/interpreter/index.html&quot;&gt;HTML&lt;/a&gt; of &lt;a href=&quot;https://www.gnu.org/software/octave/octave.pdf&quot;&gt;PDF&lt;/a&gt; formaat (link opent in externe browser)&lt;/li&gt;
&lt;li&gt;Open de documentatiebrowser van het Octave GUI in het help menu&lt;/li&gt;
&lt;/ul&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>octave::find_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/find-dialog.cc" line="+93"/>
        <source>Editor: Find and Replace</source>
        <translation>Editor: Zoek en Vervang</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Find:</source>
        <translation>&amp;Zoek:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter search text</source>
        <translation>Geef zoektekst op</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Re&amp;place:</source>
        <translation>Ver&amp;vang:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter replacement text</source>
        <translation>voer vervangende tekst in</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Match &amp;case</source>
        <translation>&amp;Hoofdlettergevoelig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search from &amp;start</source>
        <translation>Zoek vanaf het &amp;begin</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Wrap while searching</source>
        <translation>Na bestandseinde &amp;doorgaan vanaf begin</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Find &amp;Next</source>
        <translation>Zoek &amp;Volgende</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Pre&amp;vious</source>
        <translation>Zoek Vo&amp;rige</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Replace</source>
        <translation>Ve&amp;rvang</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Replace &amp;All</source>
        <translation>Vervang &amp;Alles</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;More...</source>
        <translation>&amp;Meer...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Whole words</source>
        <translation>Hele &amp;woorden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Regular &amp;expressions</source>
        <translation>Reguliere &amp;expressies</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search &amp;backward</source>
        <translation>&amp;Terug zoeken</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search se&amp;lection</source>
        <translation>In se&amp;lectie zoeken</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>Search from end</source>
        <translation>Vanaf einde terugzoeken</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search from start</source>
        <translation>Zoek vanaf begin</translation>
    </message>
    <message>
        <location line="+290"/>
        <source>Replace Result</source>
        <translation>Resultaat vervangen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 items replaced</source>
        <translation>%1 vervangingen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Find Result</source>
        <translation>Zoek Resultaat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No more matches found</source>
        <translation>Geen overeenkomsten meer gevonden</translation>
    </message>
</context>
<context>
    <name>octave::find_files_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-files-dialog.cc" line="+61"/>
        <source>Find Files</source>
        <translation>Zoek bestanden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Named:</source>
        <translation>Genaamd:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start in:</source>
        <translation>Begin in:</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter the start directory</source>
        <translation>Geef startmap op</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Browse...</source>
        <translation>Bladeren...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse for start directory</source>
        <translation>Blader naar begin map</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Current Dir</source>
        <translation>Huidige map</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set start directory to current directory</source>
        <translation>Stel huidige map in als startmap</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Search subdirectories</source>
        <translation>Zoek in submappen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search recursively through directories for matching files</source>
        <translation>Zoek ook in onderliggende mappen naar overeenkomende bestanden</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Include directory names</source>
        <translation>Inclusief submappen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Include matching directories in search results</source>
        <translation>Neem overeenkomende mappen mee in zoekresultaten</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+18"/>
        <source>Ignore case</source>
        <translation>Negeer hfd/kl. letters</translation>
    </message>
    <message>
        <location line="-17"/>
        <location line="+18"/>
        <source>Perform case insensitive match</source>
        <translation>Doe een case gevoelige match</translation>
    </message>
    <message>
        <location line="-15"/>
        <source>Contains text:</source>
        <translation>Met tekst:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Include only files containing specified text in search results</source>
        <translation>Alleen bestanden waarin de gespecificeerde tekst zit in zoekresultaten meenemen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Text to match</source>
        <translation>Overeen te komen tekst</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Results: Double click opens the file or sets the directory</source>
        <translation>Resultaten: Dubbelklikken opent bestand of stelt de map in</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search results</source>
        <translation>Zoekresultaten</translation>
    </message>
    <message>
        <location line="-59"/>
        <source>Idle.</source>
        <translation>Niet bezig.</translation>
    </message>
    <message>
        <location line="-94"/>
        <source>Enter the filename search patterns.
Several different patterns can be
separated by &apos;;&apos;, e.g. &apos;*.cc ; *.h&apos;</source>
        <translation>Voer de zoekpatronen voor bestandsnamen in.
Meerdere zoekpatronen kunnen gescheiden worden door
&apos;;&apos;, b.v. &apos;*.cc ; *.h&apos;</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Find</source>
        <translation>Zoek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Start search for matching files</source>
        <translation>Begin met zoeken naar overeenkomende bestanden</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop search</source>
        <translation>Stop met zoeken</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Filename/Location</source>
        <translation>bestandnaam/locatie</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>File contents</source>
        <translation>Inhoud van bestand</translation>
    </message>
    <message>
        <location line="+143"/>
        <source>Searching...</source>
        <translation>Bezig met zoeken...</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>%1 match (es)</source>
        <translation>%1 overeenkomst(-en)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Set search directory</source>
        <translation>Stel zoekmap in</translation>
    </message>
</context>
<context>
    <name>octave::find_files_model</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-files-model.cc" line="+96"/>
        <source>Filename</source>
        <translation>Bestandsnaam</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory</source>
        <translation>Map</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Double click to open the file</source>
        <translation>Dubbel klik om bestand te openen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Double click to set the directory</source>
        <translation>Dubbel klik om map te kiezen</translation>
    </message>
</context>
<context>
    <name>octave::find_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-widget.cc" line="+48"/>
        <source>Find:</source>
        <translation>Zoek:</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+1"/>
        <source>Search forward</source>
        <translation>Zoek voorwaarts</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1"/>
        <source>Search backward</source>
        <translation>Zoek achterwaarts</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Close</source>
        <translation>Sluit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close find dialog</source>
        <translation>Sluit zoekdialoog</translation>
    </message>
</context>
<context>
    <name>octave::gui_settings</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-settings.cc" line="+824"/>
        <source>Error %1 creating the settings file
%2
Make sure you have read and write permissions to
%3

Octave GUI must be closed now.</source>
        <translation>Fout %1 bij aanmaken van settings file
%2
Controleer dat je lees en schrijfpermissie hebt in
%3

Octave GUI moet nu gesloten worden.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Octave Critical Error</source>
        <translation>Octave Kritieke Fout</translation>
    </message>
</context>
<context>
    <name>octave::history_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/history-dock-widget.cc" line="+54"/>
        <source>Browse and search the command history.</source>
        <translation>Bladeren en zoeken door opdrachtgeschiedenis.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Copy</source>
        <translation>Kopiëer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Evaluate</source>
        <translation>Werk uit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create script</source>
        <translation>Nieuw script</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Hide filter</source>
        <translation>Verberg filter</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show filter</source>
        <translation>Toon filter</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>Double-click a command to transfer it to the Command Window.</source>
        <translation>Dubbelklik op een opdracht om het naar Opdrachtvenster te kopiëren.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter text to filter the command history</source>
        <translation>Geef zoekpatroon voor filteren van opdrachtgeschiedenis</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Command History</source>
        <translation>Opdrachtgeschiedenis</translation>
    </message>
</context>
<context>
    <name>octave::initial_page</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="-190"/>
        <source>Welcome to Octave!</source>
        <translation>Welkom bij Octave!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;You seem to be using the Octave graphical interface for the first time on this computer.
Click &apos;Next&apos; to create a configuration file and launch Octave.&lt;/p&gt;
&lt;p&gt;The configuration file is stored in&lt;br&gt;%1.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;
&lt;p&gt;Het lijkt erop dat je de grafische interface van Octave voor het eerst gebruikt op deze computer.
Klik &apos;Volgende&apos; om een configuratiebestand aan te maken en Octave te starten.&lt;/p&gt;
&lt;p&gt;Het configuratiebestand is opgeslagen in&lt;br&gt;%1.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>octave::label_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/octave-dock-widget.cc" line="+82"/>
        <source>Undock Widget</source>
        <translation>Undock widget</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Close Widget</source>
        <translation>Sluit widget</translation>
    </message>
</context>
<context>
    <name>octave::main_window</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/main-window.cc" line="+155"/>
        <source>Profiler</source>
        <translation>Profiler</translation>
    </message>
    <message>
        <location line="+361"/>
        <source>Octave</source>
        <translation>Octave</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to exit Octave?</source>
        <translation>Zeker weten dat je Octave wil afsluiten?</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Save Workspace As</source>
        <translation>Sla Werkruimte Op Als</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Load Workspace</source>
        <translation>Werkruimte laden</translation>
    </message>
    <message>
        <location line="+214"/>
        <location line="+1747"/>
        <source>About Octave</source>
        <translation>Over Octave</translation>
    </message>
    <message>
        <location line="-1560"/>
        <location line="+1635"/>
        <source>Browse directories</source>
        <translation>Blader door mappen</translation>
    </message>
    <message>
        <location line="-1330"/>
        <source>Octave Files (*.m);;All Files (*)</source>
        <translation>Octave bestanden (*.m);;Alle bestanden (*)</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>New Function</source>
        <translation>Nieuwe Functie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New function name:
</source>
        <translation>Naam van nieuwe functie:
</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>%1 is a built-in, compiled, or inline
function and can not be edited.</source>
        <translation>%1 is een ingebouwde gecompileerde
of inline functie en kan niet worden bewerkt.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Can not find function %1</source>
        <translation>Kan functie %1 niet vinden</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Octave Editor</source>
        <translation>Octave Editor</translation>
    </message>
    <message>
        <location line="+736"/>
        <source>&amp;File</source>
        <translation>&amp;Bestand</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open...</source>
        <translation>Open...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open an existing file in editor</source>
        <translation>Open een bestaand bestand in de editor</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Load Workspace...</source>
        <translation>Werkruimte laden...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save Workspace As...</source>
        <translation>Sla Werkruimte Op Als...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Exit</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>New</source>
        <translation>Nieuw</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Script</source>
        <translation>Nieuw script</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New Function...</source>
        <translation>Nieuwe functie...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New Figure</source>
        <translation>Nieuwe figuur</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Edit</source>
        <translation>B&amp;ewerken</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Undo</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Copy</source>
        <translation>Kopiëren</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <translation>Plakken</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Select All</source>
        <translation>Alles selecteren</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear Clipboard</source>
        <translation>Wis klembord</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Find Files...</source>
        <translation>Zoek bestanden...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear Command Window</source>
        <translation>Veeg opdrachtvenster schoon</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Command History</source>
        <translation>Wis opdrachtgeschiedenis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Workspace</source>
        <translation>Wis werkruimte</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Set Path...</source>
        <translation>Pas zoekpad aan ...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Preferences...</source>
        <translation>Voorkeuren...</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>De&amp;bug</source>
        <translation>De&amp;buggen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Step</source>
        <translation>Volgende opdracht</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Step In</source>
        <translation>Stap naar binnen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Step Out</source>
        <translation>Stap naar buiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue</source>
        <translation>Doorgaan</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Quit Debug Mode</source>
        <translation>Verlaat debug modus</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Tools</source>
        <translation>&amp;Gereedschappen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start &amp;Profiler Session</source>
        <translation>Nieuwe &amp;Profilersessie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Resume Profiler Session</source>
        <translation>Profilersessie he&amp;rvatten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Stop Profiler</source>
        <translation>Profiler &amp;stoppen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Show Profiler Data</source>
        <translation>&amp;Toon profiler data</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>&amp;Window</source>
        <translation>&amp;Venster</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Command Window</source>
        <translation>Toon opdrachtvenster</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Command History</source>
        <translation>Toon opdrachtgeschiedenis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show File Browser</source>
        <translation>Toon bestandsbrowser</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Workspace</source>
        <translation>Toon werkruimte</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Editor</source>
        <translation>Toon editor</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Documentation</source>
        <translation>Toon documentatie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Variable Editor</source>
        <translation>Toon Variable Editor</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Command Window</source>
        <translation>Opdrachtvenster</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command History</source>
        <translation>Opdrachtgeschiedenis</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>File Browser</source>
        <translation>Bestandsbrowser</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Workspace</source>
        <translation>Werkruimte</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+46"/>
        <source>Documentation</source>
        <translation>Documentatie</translation>
    </message>
    <message>
        <location line="-43"/>
        <source>Variable Editor</source>
        <translation>Variabele-Editor</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Previous Widget</source>
        <translation>Voorgaande widget</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset Default Window Layout</source>
        <translation>Herstel standaard window layout</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Report Bug</source>
        <translation>Probleem rapporteren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Octave Packages</source>
        <translation>Octave packages</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Get Involved</source>
        <translation>Kom Erbij</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Donate to Octave</source>
        <translation>Doneer aan Octave</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>On Disk</source>
        <translation>Op schijf</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;News</source>
        <translation>&amp;Nieuws</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Release Notes</source>
        <translation>Release notes</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Community News</source>
        <translation>Community Nieuws</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Toolbar</source>
        <translation>Werkbalk</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Enter directory name</source>
        <translation>Geef mapnaam op</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Current Directory: </source>
        <translation>Huidige map: </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>Eén mapniveau omhoog</translation>
    </message>
</context>
<context>
    <name>octave::news_reader</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/news-reader.cc" line="+106"/>
        <source>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Octave&apos;s community news source seems to be unavailable.
&lt;/p&gt;
&lt;p&gt;
For the latest news, please check
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
when you have a connection to the web (link opens in an external browser).
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; The Octave Developers, </source>
        <translation>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Octave&apos;s community nieuws lijkt niet beschikbaar te zijn.
&lt;/p&gt;
&lt;p&gt;
Kijk voor het laatste nieuws op
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
indien je een internetverbinding hebt (link opent in een nieuw scherm).
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; De Octave Ontwikkelaars, </translation>
    </message>
    <message>
        <location line="+18"/>
        <source>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Connecting to the web to display the latest Octave Community news has been disabled.
&lt;/p&gt;
&lt;p&gt;
For the latest news, please check
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
when you have a connection to the web (link opens in an external browser)
or enable web connections for news in Octave&apos;s network settings tab.
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; The Octave Developers, </source>
        <translation>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
On-line verbinding maken voor het laatste Octave community nieuws is uitgevinkt.
&lt;/p&gt;
&lt;p&gt;
Kijk voor het laatste nieuws op
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
indien je een internetverbinding hebt (link opent in een nieuw scherm).
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; De Octave Ontwikkelaars, </translation>
    </message>
</context>
<context>
    <name>octave::octave_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/octave-dock-widget.cc" line="+144"/>
        <source>Hide Widget</source>
        <translation>Verberg Widget</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>Dock Widget</source>
        <translation>Dock Widget</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Undock Widget</source>
        <translation>Undock Widget</translation>
    </message>
</context>
<context>
    <name>octave::octave_qscintilla</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/octave-qscintilla.cc" line="+302"/>
        <source>Help on</source>
        <translation>Help over</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Documentation on</source>
        <translation>Documentatie over</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>dbstop if ...</source>
        <translation>dbstop als ...</translation>
    </message>
    <message>
        <location line="+544"/>
        <source>Octave Editor</source>
        <translation>Octave Editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Creating temporary files failed.
Make sure you have write access to temp. directory
%1

&quot;Run Selection&quot; requires temporary files.</source>
        <translation>Aanmaken van tijdelijke bestanden is mislukt.
Zorg dat je schrijfrechten hebt in tijdelijke (temp) map.
%1

%quot;Selectie uitvoeren&quot; gebruikt tijdelijke bestanden.</translation>
    </message>
    <message>
        <location line="+313"/>
        <source>Press &apos;%1&apos; to replace all occurrences of &apos;%2&apos; with &apos;%3&apos;.</source>
        <translation>Druk op &apos;%1&apos; om alle voorkomens van &apos;%2&apos; te vervangen door &apos;%3&apos;.</translation>
    </message>
</context>
<context>
    <name>octave::octave_txt_lexer</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/octave-txt-lexer.cc" line="+41"/>
        <source>Default</source>
        <translation>Standaardfont</translation>
    </message>
</context>
<context>
    <name>octave::qt_interpreter_events</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/qt-interpreter-events.cc" line="+357"/>
        <location line="+5"/>
        <location line="+2"/>
        <source>Create</source>
        <translation>Maak nieuw</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>File
%1
does not exist. Do you want to create it?</source>
        <translation>Bestand
%1
bestaat niet. Wilt u het aanmaken?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Octave Editor</source>
        <translation>Octave Editor</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>The file %1 does not exist in the load path.  To run or debug the function you are editing, you must either change to the directory %2 or add that directory to the load path.</source>
        <translation>Het bestand %1 bestaat niet in het zoekpad.  Om de functie uit te voeren of te debuggen moet je naar de map %2 of die map aan het zoekpad toevoegen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The file %1 is shadowed by a file with the same name in the load path. To run or debug the function you are editing, change to the directory %2.</source>
        <translation>Het bestand %1 is afgedekt door een ander bestand met dezelfde naam in het zoekpad. Om de functie die je nu bewerkt uit te voeren of te debuggen moet je naar de map %2.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Change Directory or Add Directory to Load Path</source>
        <translation>Ga naar andere map of Voeg map toe aan zoekpad</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Directory</source>
        <translation>&amp;Ga naar andere map</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add Directory to Load Path</source>
        <translation>&amp;Voeg map toe aan zoekpad</translation>
    </message>
</context>
<context>
    <name>octave::release_notes</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/release-notes.cc" line="+84"/>
        <source>The release notes file &apos;%1&apos; is empty.</source>
        <translation>Bestand met release notes &apos;%1&apos; is leeg.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The release notes file &apos;%1&apos; cannot be read.</source>
        <translation>Bestand met release notes &apos;%1&apos; kan niet worden gelezen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Octave Release Notes</source>
        <translation>Octave Release Notes</translation>
    </message>
</context>
<context>
    <name>octave::set_path_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/set-path-dialog.cc" line="+64"/>
        <source>Set Path</source>
        <translation>Zet Pad</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>All changes take effect immediately.</source>
        <translation>Alle wijzigingen gaan direct in.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add Directory...</source>
        <translation>Voeg Map toe ...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Single Directory</source>
        <translation>Enkele Map</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory With Subdirectories</source>
        <translation>Map met Submappen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move to Top</source>
        <translation>Verschuif naar bovenaan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to Bottom</source>
        <translation>Verschuif naar onderaan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Up</source>
        <translation>Verschuif omhoog</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Down</source>
        <translation>Verschuif omlaag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove</source>
        <translation>Verwijder</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload</source>
        <translation>Opnieuw laden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save</source>
        <translation>Bewaar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Revert</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Revert Last Change</source>
        <translation>Laatste bewerking ongedaan maken</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Revert All Changes</source>
        <translation>Alle bewerkingen ongedaan maken</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Open Directory</source>
        <translation>Map openen</translation>
    </message>
</context>
<context>
    <name>octave::settings_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ed.h" line="+161"/>
        <source>Top</source>
        <translation>Bovenaan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bottom</source>
        <translation>Onderaan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-cs.h" line="+61"/>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+56"/>
        <source>Foreground</source>
        <translation>Voorgrond</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+1"/>
        <source>Background</source>
        <translation>Achtergrond</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selection</source>
        <translation>Selectie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cursor</source>
        <translation>Cursor</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+1"/>
        <source>Selected Foreground</source>
        <translation>Gekozen, Voorgrond</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selected Background</source>
        <translation>Gekozen, Achtergrond</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alternating Background</source>
        <translation>Alternerende Achtergrond</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ws.h" line="+70"/>
        <source>argument</source>
        <translation>argument</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>global</source>
        <translation>globaal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>persistent</source>
        <translation>persistent</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-global.h" line="+193"/>
        <source>Environment Variables</source>
        <translation>Omgevingsvariabelen</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-sd.h" line="+41"/>
        <source>Second color mode (light/dark)</source>
        <translation>Tweede kleurenmodus (licht/donker)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Switch to a second set of colors.
Useful for defining light/dark modes.
Discards non-applied current changes!</source>
        <translation>Schakel naar een tweede kleuren palet.
Bruikbaar voor lichte/donkere modus.
Niet opgeslagen veranderingen gaan verloren!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reload the default colors,
depends on currently selected mode.</source>
        <translation>Herlaadt de standaard kleuren,
afhankelijk van de nu geselecteerde modus.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Reload the default style values,
depends on currently selected mode.</source>
        <translation>Herlaad de standaard stijl waarden,
afhankelijk van de nu geselecteerde modus.</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>&amp;Reload default colors</source>
        <translation>Standaard kleuren he&amp;rstellen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Reload default styles</source>
        <translation>Standaard stijlen he&amp;rstellen</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.cc" line="-972"/>
        <source>Loading current preferences ... </source>
        <translation>Huidige instelling aan het laden ... </translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+6"/>
        <location line="+1088"/>
        <source>System setting</source>
        <translation>Systeeminstelling</translation>
    </message>
    <message>
        <location line="-834"/>
        <source>IBeam Cursor</source>
        <translation>I-balkje</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Block Cursor</source>
        <translation>Blokje</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Underline Cursor</source>
        <translation>Liggend streepje</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Color of highlighted current line (magenta (255,0,255) for automatic color)</source>
        <translation>Kleur van uitgelichte huidige regel (magenta (255,0,255) voor automatische kleur)</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Set Octave Startup Directory</source>
        <translation>Octave Opstartmap Instellen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set File Browser Startup Directory</source>
        <translation>Stel bestandsbrowser opstartmap in</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Applying preferences ... </source>
        <translation>Voorkeuren aan het toepassen ... </translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+22"/>
        <location line="+1120"/>
        <source>Failed to open %1 as Octave shortcut file</source>
        <translation>Kon %1 niet openen als Octave snelkoppelingenbestand</translation>
    </message>
    <message>
        <location line="-453"/>
        <source>Enable attribute colors</source>
        <translation>Zet attribuutkleuren aan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide tool tips</source>
        <translation>Verberg tooltips</translation>
    </message>
    <message>
        <location line="+384"/>
        <source>Import shortcuts from file...</source>
        <translation>Importeer snelkoppelingen uit bestand...</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+6"/>
        <source>Octave Shortcut Files (*.osc);;All Files (*)</source>
        <translation>Octave Sneltoetsbestanden (*.osc);;Alle bestanden (*)</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Export shortcuts to file...</source>
        <translation>Exporteer snelkoppelingen naar bestand...</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Overwriting Shortcuts</source>
        <translation>Sneltoetsen overschrijven</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are about to overwrite all shortcuts.
Would you like to save the current shortcut set or cancel the action?</source>
        <translation>Je staat op het punt om alle sneltoetsen te overschrijven.
Wil je de huidige sneltoetsen opslaan of de actie annuleren?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Don&apos;t save</source>
        <translation>Niet opslaan</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Octave GUI preferences</source>
        <translation>Octave GUI voorkeuren</translation>
    </message>
</context>
<context>
    <name>octave::setup_community_news</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="+45"/>
        <source>Community News</source>
        <translation>Community Nieuws</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Previous</source>
        <translation>Vorige</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;When Octave starts, it will optionally check the Octave web site for current news and information about the Octave community.
The check will happen at most once each day and news will only be displayed if there is something new since the last time you viewed the news.&lt;/p&gt;
&lt;p&gt;You may also view the news by selecting the &quot;Community News&quot; item in the &quot;Help&quot; menu, or by visiting
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;
&lt;p&gt;Wanneer Octave start zal het optioneel op de Octave web site kijken naar nieuws en informatie over de Octave community.
Dit zal hooguit eenmaal daags geschieden en nieuws wordt alleen weergegeven als er iets nieuws is sinds u voor het laatst het nieuws hebt gezien.&lt;/p&gt;
&lt;p&gt;U kunt het nieuws ook bekijken door het &quot;Community News&quot; item in het &quot;Help&quot; Menu te selecteren, of door een bezoek te brengen aan
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;Allow Octave to connect to the Octave web site when it starts in order to display current news and information about the Octave community.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;Sta Octave toe bij het starten te verbinden met de Octave web site zodat actueel nieuws en informmatie over de Octave community getoond kan worden.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>octave::shortcut_edit_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/shortcuts-tree-widget.cc" line="+172"/>
        <source>Enter New Shortcut</source>
        <translation>Voer nieuwe sneltoets in</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter custom shortcut
Action: %1</source>
        <translation>Voer aangepaste sneltoets in
Actie: %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Enter shortcut by typing it</source>
        <translation>Voer sneltoets in door die te typen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add Shift modifier
(allows one to enter number keys)</source>
        <translation>Voeg Shift toe
(kan cijfertoets mee worden aangegeven)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Actual Shortcut</source>
        <translation>Huidige Sneltoets</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Default Shortcut</source>
        <translation>Standaard Sneltoets</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Clear</source>
        <translation>Wis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set to default</source>
        <translation>Maak standaard</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>Double Shortcut</source>
        <translation>Verdubbel Sneltoets</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The chosen shortcut
  &quot;%1&quot;
is already used for the action
  &quot;%2&quot;.
Do you want to use the shortcut and remove it from the previous action?</source>
        <translation>The gekozen sneltoets
  &quot;%1&quot;
is al gebruikt voor de actie
  &quot;%2&quot;.
Wil je de sneltoets toch gebruiken en loskoppelen van de vorige actie?</translation>
    </message>
</context>
<context>
    <name>octave::shortcuts_tree_widget</name>
    <message>
        <location line="+27"/>
        <source>Global</source>
        <translation>Globaal</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+49"/>
        <source>File Menu</source>
        <translation>Bestandsmenu</translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+49"/>
        <source>Edit Menu</source>
        <translation>Edit menu</translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+52"/>
        <source>Debug Menu</source>
        <translation>Debug menu</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Tools Menu</source>
        <translation>Gereedschappen Menu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Window Menu</source>
        <translation>Venster menu</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+49"/>
        <source>Help Menu</source>
        <translation>Hulp menu</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>News Menu</source>
        <translation>Nieuws menu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Handling of Dock Widgets</source>
        <translation>Gebruik van Dock widgets</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tab Handling in Dock Widgets</source>
        <translation>Tabbladgebruik in gedockte widgets</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find &amp; Replace in Dock Widgets</source>
        <translation>Zoek &amp; Vervang in dock widgets</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zooming in Editor and Documentation</source>
        <translation>Inzoomen in Editor en Documentatie</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>View Menu</source>
        <translation>Weergave menu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Run Menu</source>
        <translation>Uitvoeren menu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Documentation Viewer</source>
        <translation>Documentatie browser</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Browser</source>
        <translation>Browser</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>item %1 not found in shortcut settings dialog</source>
        <translation>item %1 niet gevonden in sneltoets instelling dialoog</translation>
    </message>
</context>
<context>
    <name>octave::terminal_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/terminal-dock-widget.cc" line="+106"/>
        <source>Command Window</source>
        <translation>Opdrachtvenster</translation>
    </message>
</context>
<context>
    <name>octave::variable_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/variable-editor.cc" line="+177"/>
        <source>Dock Widget</source>
        <translation>Dock Widget</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable Editor: </source>
        <translation>Variabele-Editor: </translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Undock Widget</source>
        <translation>Undock Widget</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Restore geometry</source>
        <translation>Herstel geometrie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Redock</source>
        <translation>Terug docken</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor</name>
    <message>
        <location line="+922"/>
        <source>Variable Editor</source>
        <translation>Variabele-Editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit variables.</source>
        <translation>Variabelen bewerken.</translation>
    </message>
    <message>
        <location line="+555"/>
        <source>Variable Editor Toolbar</source>
        <translation>Variabele-Editor Werkbalk</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save</source>
        <translation>Bewaar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save variable to a file</source>
        <translation>Sla variabele in een bestand op</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+5"/>
        <location line="+6"/>
        <source>Save in format ...</source>
        <translation>Bewaar in format ...</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Save variable to a file in different format</source>
        <translation>Bewaar variabele in bestand in andere indeling</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cut</source>
        <translation>Knippen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cut data to clipboard</source>
        <translation>Knip data naar klembord</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy</source>
        <translation>Kopiëren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy data to clipboard</source>
        <translation>Kopieer data naar klembord</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste</source>
        <translation>Plakken</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste clipboard into variable data</source>
        <translation>Plak klembord in variabele</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+6"/>
        <location line="+7"/>
        <source>Plot</source>
        <translation>Plot</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Plot Selected Data</source>
        <translation>Plot geselecteerde gegevens</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Plot selected data</source>
        <translation>Plot geselecteerde gegevens</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Up</source>
        <translation>Omhoog</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go one level up in variable hierarchy</source>
        <translation>Ga een niveau omhoog in variabele hiërarchie</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor_stack</name>
    <message>
        <location line="-1265"/>
        <source>Save Variable %1 As</source>
        <translation>Sla variabele %1 op als</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor_view</name>
    <message>
        <location line="+142"/>
        <source>Cut</source>
        <translation>Knippen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy</source>
        <translation>Kopiëren</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste</source>
        <translation>Plakken</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Clear</source>
        <translation>Wis</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Variable from Selection</source>
        <translation>Variabele uit selectie</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transpose</source>
        <translation>Transponeren</translation>
    </message>
    <message>
        <location line="+56"/>
        <source> columns</source>
        <translation> kolommen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source> column</source>
        <translation> kolom</translation>
    </message>
    <message>
        <location line="+52"/>
        <source> rows</source>
        <translation> rijen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source> row</source>
        <translation> rij</translation>
    </message>
</context>
<context>
    <name>octave::welcome_wizard</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="-196"/>
        <source>Welcome to GNU Octave</source>
        <translation>Welkom bij GNU Octave</translation>
    </message>
</context>
<context>
    <name>octave::workspace_model</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/workspace-model.cc" line="+49"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Class</source>
        <translation>Klasse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dimension</source>
        <translation>Dimensie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation>Waarde</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attribute</source>
        <translation>Eigenschap</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Right click to copy, rename, or display</source>
        <translation>Klik rechts om te kopiëren, hernoemen of weergeven</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+2"/>
        <source>complex</source>
        <translation>complex</translation>
    </message>
</context>
<context>
    <name>octave::workspace_view</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ws.h" line="-21"/>
        <source>Class</source>
        <translation>Klasse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dimension</source>
        <translation>Dimensie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation>Waarde</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attribute</source>
        <translation>Eigenschap</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/workspace-view.cc" line="+60"/>
        <source>Workspace</source>
        <translation>Werkruimte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View the variables in the active workspace.</source>
        <translation>Bekijk de variabelen in de huidige werkruimte.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter text to filter the workspace</source>
        <translation>Geef patroon voor filteren van werkruimte</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>View the variables in the active workspace.&lt;br&gt;</source>
        <translation>Bekijk variabelen in actieve werkruimte.&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Colors for variable attributes:</source>
        <translation>Kleuren voor eigenschappen van variabelen:</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>Open in Variable Editor</source>
        <translation>Toon in Variable Editor</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy name</source>
        <translation>Kopieer naam</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy value</source>
        <translation>Kopieer waarde</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Rename</source>
        <translation>Hernoemen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Only top-level symbols may be renamed</source>
        <translation>Alleen symbolen uit het bovenste niveau mogen hernoemd worden</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Hide filter</source>
        <translation>Verberg filter</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show filter</source>
        <translation>Toon filter</translation>
    </message>
</context>
<context>
    <name>self_listener</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/self-listener.cc" line="+82"/>
        <source>Can not open redirected stream with fd = %1.</source>
        <translation>Kan omgeleide stream met fd = %1 niet openen.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Can not disable buffering of stream with fd = %1.</source>
        <translation>Kan bufferen op stream met fd = %1 niet uitzetten.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Can not dup redirected stream with fd = %1.</source>
        <translation>Kan &apos;dup&apos; niet op omgeleide stream met fd = %1 toepassen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot create pipe for redirecting stream with fd = %1:</source>
        <translation>Kan geen pipe maken voor omleiding van stream met fd = %1:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot dup2 redirected stream with fd = %1
to pipe with fd = %2: %3</source>
        <translation>Kan geen &apos;dup2&apos; toepassen op omgeleide stream met fd = %1
naar pipe met fd = %2: %3</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Error while listening to redirected streams</source>
        <translation>Fout tijdens luisteren naar omgeleide streams</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Error reading from redirected stream fd = %1.</source>
        <oldsource>Error reading from redirected strem fd = %1.</oldsource>
        <translation>Fout bij lezen van omgeleide stream met fd = %1.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>
Output redirection in </source>
        <translation>
Uitvoer omleiding in </translation>
    </message>
    <message>
        <location line="+1"/>
        <source> won&apos;t work.</source>
        <translation> zal niet werken.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>
Error: </source>
        <translation>
Fout: </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Octave</source>
        <translation>Octave</translation>
    </message>
</context>
<context>
    <name>settings_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.ui" line="+45"/>
        <location line="+834"/>
        <location line="+1909"/>
        <source>General</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <location line="-2390"/>
        <source>Octave logo only</source>
        <translation>Alleen Octave logo</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Letter icons</source>
        <translation>Letter iconen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphic icons</source>
        <translation>Grafische iconen</translation>
    </message>
    <message>
        <location line="-341"/>
        <source>Dock widget title bar</source>
        <translation>Dock widget titelbalk</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Normal</source>
        <translation>Normaal</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Custom style</source>
        <translation>Aangepaste stijl</translation>
    </message>
    <message>
        <location line="+237"/>
        <source>These preferences are applied after any .octaverc startup files.</source>
        <translation>Deze voorkeuren worden toegepast na verwerking van .octaverc opstartbestanden.</translation>
    </message>
    <message>
        <location line="+353"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Show EOL characters</source>
        <translation>Toon EOL tekens</translation>
    </message>
    <message>
        <location line="+444"/>
        <source>Wrap long lines at current window border</source>
        <translation>Breek lange regels af op huidige vensterrand</translation>
    </message>
    <message>
        <location line="+501"/>
        <source>Default EOL mode</source>
        <translation>Standaard EOL instelling</translation>
    </message>
    <message>
        <location line="-446"/>
        <source>Indent width</source>
        <translation>Inspringafstand</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tab indents line</source>
        <translation>Inspringen met tabtoets</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Auto indentation</source>
        <translation>Automatisch inspringen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tab width</source>
        <translation>Tab breedte</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show indentation guides</source>
        <translation>Toon inspringraster</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Backspace unindents line</source>
        <translation>Backspace doet terug inspringen</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Match keywords</source>
        <translation>Zoek overeenkomende sleutelwoorden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Case sensitive</source>
        <translation>Hoofdlettergevoelig</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Replace word by suggested one</source>
        <translation>Vervang woord door suggestie</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Match words in document</source>
        <translation>Zoek overeenkomende woorden in document</translation>
    </message>
    <message>
        <location line="-643"/>
        <source>Use custom file editor</source>
        <translation>Gebruik een andere editor</translation>
    </message>
    <message>
        <location line="+1002"/>
        <source>Editor Styles</source>
        <translation>Editorstijlen</translation>
    </message>
    <message>
        <location line="-1478"/>
        <location line="+1863"/>
        <source>Font</source>
        <translation>Lettertype</translation>
    </message>
    <message>
        <location line="-1573"/>
        <source>Show line numbers</source>
        <translation>Toon regelnummers</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Show whitespace</source>
        <translation>Toon witruimte-tekens</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Do not show whitespace used for indentation</source>
        <translation>Verberg voor inspringen gebruikte witruimte</translation>
    </message>
    <message>
        <location line="+935"/>
        <source>Number of characters before list is shown: </source>
        <translation>Aantal tekens voordat lijst wordt getoond: </translation>
    </message>
    <message>
        <location line="+150"/>
        <source>Create nonexistent files without prompting</source>
        <translation>Maak nog niet bestaande bestanden aan zonder bevestiging</translation>
    </message>
    <message>
        <location line="-857"/>
        <source>command line (%f=file, %l=line):</source>
        <translation>command line (%f=bestand, %l=regel):</translation>
    </message>
    <message>
        <location line="-500"/>
        <source>Cursor type:</source>
        <translation>Type aanwijzer:</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Use foreground color</source>
        <translation>Gebruik voorgrondkleur</translation>
    </message>
    <message>
        <location line="+52"/>
        <location line="+1753"/>
        <source>Font size</source>
        <translation>Lettergrootte</translation>
    </message>
    <message>
        <location line="-245"/>
        <source>File Browser</source>
        <translation>Bestandsbrowser</translation>
    </message>
    <message>
        <location line="-2104"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Confirm before exiting</source>
        <translation>Bevestigen voor afsluiten</translation>
    </message>
    <message>
        <location line="-20"/>
        <location line="+439"/>
        <source>Show status bar</source>
        <translation>Toon statusbalk</translation>
    </message>
    <message>
        <location line="-677"/>
        <source>Text inactive</source>
        <translation>Inactieve tekst</translation>
    </message>
    <message>
        <location line="-32"/>
        <location line="+45"/>
        <source>Active</source>
        <translation>Actief</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Small</source>
        <translation>Klein</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Large</source>
        <translation>Groot</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Background inactive</source>
        <translation>Inactieve achtergrond</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>3D</source>
        <translation>3D</translation>
    </message>
    <message>
        <location line="+199"/>
        <source>Octave Startup</source>
        <translation>Octave opstarten</translation>
    </message>
    <message>
        <location line="+46"/>
        <location line="+1691"/>
        <source>Browse</source>
        <translation>Bladeren</translation>
    </message>
    <message>
        <location line="-890"/>
        <location line="+140"/>
        <source>This works well for monospaced fonts. The line is drawn at a position based on the width of a space character in the default font. It may not work very well if styles use proportional fonts or if varied font sizes or bold, italic and normal texts are used.</source>
        <translation>Dit werkt goed met monospaced fonts. De regel wordt weergegeven op een positie gebaseerd op de breedte van een spatie in de standaard-font. Het werkt mogelijk minder goed bij een stijl met proportionele fonts of als vette, schuine en normale tekst met verschillende groottes worden gebruikt.</translation>
    </message>
    <message>
        <location line="-535"/>
        <source>Enable Code Folding</source>
        <translation>Te lange regels afbreken</translation>
    </message>
    <message>
        <location line="+1059"/>
        <source>Windows (CRLF)</source>
        <translation>Windows (CRLF)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Legacy Mac (CR)</source>
        <translation>Legacy Mac (CR)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unix (LF)</source>
        <translation>Unix (LF)</translation>
    </message>
    <message>
        <location line="-1034"/>
        <source>Show horizontal scroll bar</source>
        <translation>Toon horizontale schuifbalk</translation>
    </message>
    <message>
        <location line="-963"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
    <message>
        <location line="+351"/>
        <source>(requires restart)</source>
        <translation>(herstart van Octave nodig)</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Use native file dialogs</source>
        <translation>Gebruik standaard bestandsdialogen</translation>
    </message>
    <message>
        <location line="-98"/>
        <source>Toolbar Icons</source>
        <translation>Werkbalk-iconen</translation>
    </message>
    <message>
        <location line="-224"/>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>Dock widgets window icons</source>
        <translation>Dock Widgets venster iconen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Style</source>
        <translation>Stijl</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Icon theme (requires restart)</source>
        <translation>Iconenthema (herstart noodzakelijk)</translation>
    </message>
    <message>
        <location line="+323"/>
        <source>Blinking cursor</source>
        <translation>Knipperende cursor</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Initial working directory of Octave interpreter</source>
        <translation>Begin-werkmap van Octave&apos;s interpretator</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Restore last working directory of previous session</source>
        <translation>Ga naar laatste werkmap van vorige sessie</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Command</source>
        <translation>Opdrachtvenster</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Set focus to Command Window when running a command from within another widget</source>
        <translation>Zet focus op Opdrachtvenster bij uitvoeren van een opdracht vanuit een ander widget</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Print debug location in Command Window in addition to the marker in the editor</source>
        <translation>Geef debuglocatie in Opdrachtvenster weer, evenals markering in editor</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Command Window Colors</source>
        <translation>Opdrachtvenster-kleuren</translation>
    </message>
    <message>
        <location line="+159"/>
        <source>Show tool bar</source>
        <translation>Toon werkbalk</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Number size as difference to editor font</source>
        <translation>Puntgrootte-verschil met editor font</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Highlight current line (color adjustable below with editor styles)</source>
        <translation>Licht huidige regel uit (kleur beneden bij Editorstijlen instelbaar)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Highlight all occurrences of a word selected by a double click</source>
        <translation>Markeer alle voorkomens van een woord dat is geselecteerd met een dubbelklik</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Tabs</source>
        <translation>Tabs</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Rotate tabs: Vertical when at top or bottom and horizontal when left or right. The close button is not shown in rotated tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Roteer tabs: Verticaal indien boven- of onderaan en Horizontaal indien links of rechts. De sluitknop wordt niet getoond in geroteerde tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Comments (Octave)</source>
        <translation>Commentaar (Octave)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Strings considered for uncommenting text</source>
        <translation>Beginkarakters voor &quot;uncommenting&quot; tekst</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>String used for commenting selected text</source>
        <translation>Beginkarakters voor &quot;commenting&quot; tekst</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Long lines</source>
        <translation>Lange regels</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Break long lines at line length</source>
        <translation>Breek lange regels af op regellengte</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Break lines only in comments</source>
        <translation>Breek alleen commentaarregels af</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Line length</source>
        <translation>Regellengte</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Long line marker</source>
        <translation>Lange-regelmarkering</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Line</source>
        <translation>Verticale lijn</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Background</source>
        <translation>Achtergrond</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Indentation</source>
        <translation>Inspringen</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Indentation uses tabs</source>
        <translation>Inspringen o.b.v. tabs</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Auto insert after &quot;if&quot; etc.</source>
        <translation>Automatisch invoegen na &quot;if&quot; etc.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Nothing</source>
        <translation>Niets</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&quot;endif&quot; etc.</source>
        <translation>&quot;endif&quot; etc.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&quot;end&quot;</source>
        <translation>&quot;end&quot;</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Auto completion</source>
        <translation>Automatisch aanvullen</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>With Octave builtins</source>
        <translation>Met Octave &quot;builtins&quot;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>With Octave functions</source>
        <translation>Met Octave functies</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Show completion list automatically</source>
        <translation>Toon aanvullijst automatisch</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Debugging</source>
        <translation>Debuggen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Always show debug breakpoints and pointers (opens related file if closed)</source>
        <translation>Altijd debug onderbreekpunten en aanwijzers tonen (opent zo nodig het betreffende bestand)</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>File handling</source>
        <translation>Bestandsbeheer</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Reload externally changed files without prompt</source>
        <translation>Herlaad bestanden die van buitenaf zijn veranderd zonder bevestiging</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Restore editor tabs from previous session on startup or when editor is shown again after closing</source>
        <translation>Herstel editortabbladen uit vorige sessie bij opstarten of als editor weer getoond wordt na te zijn afgesloten</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Text encoding used for loading and saving</source>
        <translation>Tekst codering gebruikt voor laden en opslaan</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Close all files when the editor widget is closed/hidden</source>
        <translation>Sluit alle bestanden af als het editorwidget wordt gesloten of verborgen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Force newline at end when saving file</source>
        <translation>Forceer regeleinde aan eind van bestand bij opslaan</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove trailing spaces when saving file</source>
        <translation>Verwijder spaties aan einde van regels bij opslaan bestand</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select font, font size (as a difference from the default size), font style (&lt;b&gt;b&lt;/b&gt;old, &lt;b&gt;i&lt;/b&gt;talic, &lt;b&gt;u&lt;/b&gt;nderline), text color, and background color (for the latter, the color magenta (255,0,255) is a placeholder for the default background color).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kies font, fontgrootte (als verschil met standaardgrootte), font stijl (&lt;b&gt;v&lt;/b&gt;et, &lt;b&gt;s&lt;/b&gt;chuin, &lt;b&gt;o&lt;/b&gt;nderstreept), tekstkleur en achtergrondkleur (magenta (255,0,255) is een opvuller voor de standaard achtergrondkleur).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+848"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ok&lt;/span&gt; - close dialog and apply settings&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Apply - &lt;/span&gt;apply settings but leave dialog open&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Cancel - &lt;/span&gt;close dialog and discard changes not yet applied&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Reset -&lt;/span&gt; reload settings discarding changes not yet applied&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ok&lt;/span&gt; - sluit dialoog and pas instellingen toe&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Apply - &lt;/span&gt;pas instellingen toe, maar laat dialoog open&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Cancel - &lt;/span&gt;sluit dialoog en vergeet niet opgeslagen wijzigingen&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Reset -&lt;/span&gt; herlaad de instellingen, niet opgeslagen wijzigingen gaan verloren&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="-2247"/>
        <source>(Changing buffer size clears history)</source>
        <translation>(Bij aanpassen van de buffergrootte wordt de geschiedenis gewist)</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If set, the focus of the widgets that are docked to the main window follows the mouse cursor. This is intended for having the same behavior within the main window when &amp;quot;focus follows mouse&amp;quot; is used for the desktop environment.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indien actief volgt focus van in het hoofdvenster gedockte widgets de muiscursor. Dit is bedoeld om binnen het hoofdvenster hetzelfde &amp;quot;focus follows mouse&amp;quot; gedrag te verkrijgen als in de desktopomgeving.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Focus follows mouse for widgets docked to the main window</source>
        <translation>Focus volgt muiscursor voor in hoofdvenster gedockte widgets</translation>
    </message>
    <message>
        <location line="+278"/>
        <source>History buffer Size</source>
        <translation>Grootte van geschiedenisbuffer</translation>
    </message>
    <message>
        <location line="+430"/>
        <source>Rotated tabs</source>
        <translation>Geroteerde tabs</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Max. tab width in chars (0: no limit)</source>
        <translation>Max. tabbreedte in karakters (0: ongelimiteerd)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Max. width of a tab in characters (average char. width). Especially useful for rotated tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Max. tabbreedte in karakters (gemiddelde kar. breedte). Vooral handig voor geroteerde tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+962"/>
        <source>Behavior</source>
        <translation>Gedrag</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Restore last directory of previous session</source>
        <translation>Herstel laatste map van vorige sessie</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Initial file browser directory (only if not synchronized with initial working directory of Octave)</source>
        <translation>Initiële bestandsbrowser-map
(alleen indien niet gesynchroniseerd met startmap van Octave)</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Extensions of files to be opened in the default text editor (separated by &quot;;&quot;):</source>
        <translation>Bestandsextensies om in de standaard tekstverwerker te kunnen openen (gescheiden door &quot;;&quot;):</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Workspace</source>
        <translation>Werkruimte</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Colors for variable attributes</source>
        <translation>Kleuren voor eigenschappen van variabelen</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Use Command Window font</source>
        <translation>Gebruik font van Opdrachtvenster</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>Import shortcut set</source>
        <translation>Importeer sneltoetsen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Export current shortcut set</source>
        <translation>Exporteer huidige sneltoetsen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reset shortcuts to their defaults</source>
        <translation>Herstel sneltoetsen naar standaardwaarden</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+80"/>
        <source>Default</source>
        <translation>Standaardinstellingen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Actual</source>
        <translation>Huidig</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Network</source>
        <translation>Netwerk</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Allow Octave to connect to the Octave web site to display current news and information</source>
        <translation>Sta Octave toe om naar de Octave website te gaan om recente informatie en nieuws weer te geven</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Proxy Server</source>
        <translation>Proxy Server</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select &lt;span style=&quot; font-style:italic;&quot;&gt;HttpProxy&lt;/span&gt;, &lt;span style=&quot; font-style:italic;&quot;&gt;Sock5Proxy&lt;/span&gt; or &lt;span style=&quot; font-style:italic;&quot;&gt;Environment Variables&lt;/span&gt;. With the last selection, the proxy is taken from the first non-empty environment variable ALL_PROXY, HTTP_PROXY or HTTPS_PROXY .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kies &lt;span style=&quot; font-style:italic;&quot;&gt;HttpProxy&lt;/span&gt;, &lt;span style=&quot; font-style:italic;&quot;&gt;Sock5Proxy&lt;/span&gt; of &lt;span style=&quot; font-style:italic;&quot;&gt;Omgevingsvariaben&lt;/span&gt;. Ingeval van de laatste optie wordt de proxy overgenomen van de eerste niet-lege omgevingsvariabele ALL_PROXY, HTTP_PROXY of HTTPS_PROXY .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Use proxy server</source>
        <translation>Gebruik proxyserver</translation>
    </message>
    <message>
        <location line="-523"/>
        <source>Variable Editor</source>
        <translation>Variabele-Editor</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Default row height</source>
        <translation>Standaard regelhoogte</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Default column width</source>
        <translation>Standaard kolombreedte</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Variable Editor Colors</source>
        <translation>Variabele-Editor Kleuren</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Use alternating row colors</source>
        <translation>Gebruik alternerende rijkleuren</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Disable global shortcuts in order to prevent
interference with readline key strokes.
Exceptions: Ctrl-C for interrupting the interpreter
and the shortcuts for switching to other widgets.</source>
        <translation>Schakel globale sneltoetsen uit om
interferentie met readline toetsen te voorkomen.
Uitzonderingen: Ctrl-C om de interpreter te onderbreken
en de sneltoetsen om naar andere widgets te gaan.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Disable global shortcuts when Command Window has focus</source>
        <translation>Zet globale snelkoppelingen uit indien Opdrachtvenster focus heeft</translation>
    </message>
    <message>
        <location line="-392"/>
        <source>Synchronize Octave working directory with file browser</source>
        <translation>Synchroniseer Octave werkmap met bestandsverkenner</translation>
    </message>
    <message>
        <location line="+348"/>
        <source>Shortcuts</source>
        <translation>Sneltoetsen</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Export</source>
        <translation>Exporteer</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Import</source>
        <translation>Importeer</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Disable menu accelerators in order to prevent
interference with readline key strokes.</source>
        <translation>Schakel globale sneltoetsen uit om
interferentie met readline toetsaanslagen te voorkomen.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Disable menu accelerators of main window menus when Command Window has focus</source>
        <translation>Schakel menutoetsen van hoofdvenster uit indien Opdrachtvenster focus heeft</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Edit a shortcut by double-clicking in Actual column</source>
        <translation>Bewerk een sneltoets door te dubbelklikken in de kolom &apos;Huidig&apos;</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Action</source>
        <translation>Actie</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Hostname:</source>
        <translation>Hostnaam:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Proxy type:</source>
        <translation>Proxy type:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Port:</source>
        <translation>Poort:</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Username:</source>
        <translation>Gebruikersnaam:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Password:</source>
        <translation>Wachtwoord:</translation>
    </message>
</context>
<context>
    <name>shortcuts</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-sc.cc" line="+227"/>
        <source>Undock/Dock Widget</source>
        <translation>Undock/Dock widget</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close Widget</source>
        <translation>Sluit widget</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New File</source>
        <translation>Nieuw bestand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Function</source>
        <translation>Nieuwe Functie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Figure</source>
        <translation>Nieuwe figuur</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open File</source>
        <translation>Open bestand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Load Workspace</source>
        <translation>Werkruimte laden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save Workspace As</source>
        <translation>Sla Werkruimte Op Als</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exit Octave</source>
        <translation>Octave afsluiten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy</source>
        <translation>Kopiëren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste</source>
        <translation>Plakken</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Undo</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select All</source>
        <translation>Alles selecteren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Clipboard</source>
        <translation>Wis klembord</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find in Files</source>
        <translation>Zoek in bestanden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Command Window</source>
        <translation>Veeg opdrachtvenster schoon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Command History</source>
        <translation>Wis opdrachtgeschiedenis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Workspace</source>
        <translation>Wis werkruimte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set Path</source>
        <translation>Pas Zoekpad aan</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+119"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
    <message>
        <location line="-116"/>
        <source>Step</source>
        <translation>Volgende opdracht</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Step In</source>
        <translation>Stap functie binnen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Step Out</source>
        <translation>Stap functie uit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Continue</source>
        <translation>Doorgaan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit Debug Mode</source>
        <translation>Verlaat Debug modus</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start/Stop Profiler Session</source>
        <translation>Profiler sessie starten / stoppen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resume Profiler Session</source>
        <translation>Profiler sessie hervatten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Profile Data</source>
        <translation>Profiler resultaten tonen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show Command Window</source>
        <translation>Toon opdrachtvenster</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Command History</source>
        <translation>Toon opdrachtgeschiedenis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show File Browser</source>
        <translation>Toon bestandsbrowser</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Workspace</source>
        <translation>Toon werkruimte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Editor</source>
        <translation>Toon editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Documentation</source>
        <translation>Toon documentatie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Variable Editor</source>
        <translation>Toon Variabele-Editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command Window</source>
        <translation>Opdrachtvenster</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command History</source>
        <translation>Opdrachtgeschiedenis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File Browser</source>
        <translation>Bestandsbrowser</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Workspace</source>
        <translation>Werkruimte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Documentation</source>
        <translation>Documentatie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Variable Editor</source>
        <translation>Variabele-Editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Widget</source>
        <translation>Vorige widget</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset Default Window Layout</source>
        <translation>Herstel de standaard Window Layout</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show On-disk Documentation</source>
        <translation>Toon Documentatie op schijf</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Online Documentation</source>
        <translation>Toon on-line documentatie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Report Bug</source>
        <translation>Probleem rapporteren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Octave Packages</source>
        <translation>Octave Packages</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contribute to Octave</source>
        <translation>Draag bij aan Octave project</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Octave Developer Resources</source>
        <translation>Octave Ontwikkelaars Resources</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About Octave</source>
        <translation>Over Octave</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Release Notes</source>
        <translation>Release notes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Community News</source>
        <translation>Community Nieuws</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Close Tab</source>
        <translation>Tabblad sluiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close All Tabs</source>
        <translation>Alle tabbladen sluiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close Other Tabs</source>
        <translation>Andere tabbladen sluiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch to Left Tab</source>
        <translation>Wissel naar Linker Tabblad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch to Right Tab</source>
        <translation>Wissel naar Rechter Tabblad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Tab Left</source>
        <translation>Verplaats Tabblad naar Links</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Tab Right</source>
        <translation>Verplaats Tabblad naar Rechts</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom In</source>
        <translation>Vergroten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom Out</source>
        <translation>Verkleinen</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Zoom Normal</source>
        <translation>Zoomen naar standaard</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Edit Function</source>
        <translation>Bewerk functie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save File</source>
        <translation>Bewaar bestand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save File As</source>
        <translation>Bewaar bestand als</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>Afdrukken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Redo</source>
        <translation>Opnieuw</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut</source>
        <translation>Knippen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find and Replace</source>
        <translation>Zoek en vervang</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Next</source>
        <translation>Volgende zoeken</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Previous</source>
        <translation>Vorige zoeken</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to Start of Word</source>
        <translation>Wis tot begin van woord</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to End of Word</source>
        <translation>Wis tot einde van woord</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to Start of Line</source>
        <translation>Wis tot begin van regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to End of Line</source>
        <translation>Wis tot einde van regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete Line</source>
        <translation>Verwijder regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Line</source>
        <translation>Kopieer regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut Line</source>
        <translation>Knip regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Duplicate Selection/Line</source>
        <translation>Dubbele Selectie/Regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transpose Line</source>
        <translation>Transponeer regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Completion List</source>
        <translation>Toon aanvullijst</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment Selection</source>
        <translation>Maak commentaar van selectie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uncomment Selection</source>
        <translation>Maak code van selectie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment Selection (Choosing String)</source>
        <translation>Commentaar Selectie (String kiezen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uppercase Selection</source>
        <translation>Zet selectie om naar hoofdletters</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Lowercase Selection</source>
        <translation>Zet selectie om naar kleine letters</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Indent Selection Rigidly</source>
        <translation>Selectie strak laten inspringen</translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+3"/>
        <source>Unindent Selection Rigidly</source>
        <translation>Selectie strak terug laten inspringen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Indent Code</source>
        <translation>Code laten inspringen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Convert Line Endings to Windows</source>
        <translation>Zet regeleindes om naar Windows</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Convert Line Endings to Unix</source>
        <translation>Zet regeleindes om naar Unix</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Convert Line Endings to Mac</source>
        <translation>Zet regeleindes om naar Mac</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Goto Line</source>
        <translation>Ga naar regel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to Matching Brace</source>
        <translation>Ga naar bijbehorende accolade</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select to Matching Brace</source>
        <translation>Selecteer tot bijbehorende accolade</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle Bookmark</source>
        <translation>Schakel bladwijzer aan of uit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Bookmark</source>
        <translation>Volgende bladwijzer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Bookmark</source>
        <translation>Vorige bladwijzer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove All Bookmark</source>
        <translation>Alle bladwijzers verwijderen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Styles Preferences</source>
        <translation>Stijlvoorkeuren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Line Numbers</source>
        <translation>Toon regelnummers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Whitespace Characters</source>
        <translation>Toon witruimte-tekens</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Line Endings</source>
        <translation>Toon regeleindes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Indentation Guides</source>
        <translation>Toon inspringlijnen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Long Line Marker</source>
        <translation>Toon lange-regelmarkering</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Toolbar</source>
        <translation>Toon Taakbalk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Statusbar</source>
        <translation>Toon Statusbalk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Horizontal Scrollbar</source>
        <translation>Toon Horizontale Schuifbalk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort Tabs Alphabetically</source>
        <translation>Tabs alfabetisch sorteren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Toggle Breakpoint</source>
        <translation>Onderbrekingspunt aan/uitzetten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Breakpoint</source>
        <translation>Volgend onderbrekingspunt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Breakpoint</source>
        <translation>Vorig onderbrekingspunt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove All Breakpoints</source>
        <translation>Wis alle onderbrekingspunten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run File</source>
        <translation>Voer bestand uit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Selection</source>
        <translation>Voer selectie uit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Tests</source>
        <translation>Voer Tests uit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Demos</source>
        <translation>Voer Demos uit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help on Keyword</source>
        <translation>Hulp voor sleutelwoorden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document on Keyword</source>
        <translation>Documentatie over Sleutelwoord</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Go to Homepage</source>
        <translation>Ga naar Homepage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go Back one Page</source>
        <translation>Ga een pagina terug</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go Forward one Page</source>
        <translation>Ga een pagina vooruit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bookmark this Page</source>
        <translation>Bladwijzer naar deze pagina</translation>
    </message>
</context>
</TS>
