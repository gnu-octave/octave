<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/files-dock-widget.cc" line="+141"/>
        <source>Invalid filename</source>
        <translation>Érvénytelen fáljnév</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.cc" line="+1045"/>
        <source>Difference to the default size</source>
        <translation>Különbség az alapértelmezett mérethez képest</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Background color, magenta (255, 0, 255) means default</source>
        <translation>A háttérszín, rózsaszín (255, 0, 255) az alapértelmezett</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>b</source>
        <comment>short form for bold</comment>
        <translation>F</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>i</source>
        <comment>short form for italic</comment>
        <translation>D</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>u</source>
        <comment>short form for underlined</comment>
        <translation>A</translation>
    </message>
</context>
<context>
    <name>QTerminal</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/QTerminal.cc" line="+119"/>
        <source>Edit &quot;%1&quot;</source>
        <translation>„%1” szerkesztése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help on &quot;%1&quot;</source>
        <translation>Segítség keresése a következőhöz: „%1”</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Documentation on &quot;%1&quot;</source>
        <translation>Dokumentáció keresése a következőhöz: „%1”</translation>
    </message>
    <message>
        <location line="+165"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run Selection</source>
        <translation>Kijelölés futtatása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit selection</source>
        <translation>Kijelölés szerkesztése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help on selection</source>
        <translation>Kijelölés a súgóban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Documentation on selection</source>
        <translation>Kijelölés a dokumentációban</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Clear Window</source>
        <translation>Ablak tartalom kiürítése</translation>
    </message>
    <message>
        <location line="-233"/>
        <source>Edit %1 at line %2</source>
        <translation>%1 szerkesztése a(z) %2 sorban</translation>
    </message>
</context>
<context>
    <name>QWinTerminalImpl</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/win32/QWinTerminalImpl.cpp" line="+1859"/>
        <source>copied selection to clipboard</source>
        <translation>Kijelölés vágólapra másolva</translation>
    </message>
</context>
<context>
    <name>QsciLexerBash</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerbash.cpp" line="+203"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Szám</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Kétszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Egyszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Művelet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Azonosító</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scalar</source>
        <translation>Skalár</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Parameter expansion</source>
        <translation>Paraméterbővítés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks</source>
        <translation>Fordított ékezetek</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Here document delimiter</source>
        <translation>Itteni dokumentum határoló</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted here document</source>
        <translation>Egyszeres-idézett itteni dokumentum</translation>
    </message>
</context>
<context>
    <name>QsciLexerBatch</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerbatch.cpp" line="+174"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Címke</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide command character</source>
        <translation>Parancskarakter elrejtése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>External command</source>
        <translation>Külső parancs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable</source>
        <translation>Változó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Művelet</translation>
    </message>
</context>
<context>
    <name>QsciLexerCPP</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexercpp.cpp" line="+364"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive default</source>
        <translation>Inaktív alapértelmezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C comment</source>
        <translation>C megjegyzése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C comment</source>
        <translation>Inaktív C megjegyzése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C++ comment</source>
        <translation>C++ megjegyzése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C++ comment</source>
        <translation>Inaktív C++ megjegyzése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style C comment</source>
        <translation>JavaDoc stílusú C megjegyzése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style C comment</source>
        <translation>Inaktív JavaDoc stílusú C megjegyzése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Szám</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive number</source>
        <translation>Inaktív szám</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive keyword</source>
        <translation>Inaktív kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Kétszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive double-quoted string</source>
        <translation>Inaktív kétszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Egyszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive single-quoted string</source>
        <translation>Inaktív egyszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>IDL UUID</source>
        <translation>IDL UUID azonosító</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive IDL UUID</source>
        <translation>Inaktív IDL UUID azonosító</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pre-processor block</source>
        <translation>Előfeldolgozó blokk</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive pre-processor block</source>
        <translation>Inaktív előfeldolgozó blokk</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Művelet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive operator</source>
        <translation>Inaktív művelet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Azonosító</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive identifier</source>
        <translation>Inaktív azonosító</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unclosed string</source>
        <translation>Lezáratlan karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive unclosed string</source>
        <translation>Inaktív lezáratlan karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C# verbatim string</source>
        <translation>C# szó szerinti karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C# verbatim string</source>
        <translation>Inaktív C# szó szerinti karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaScript regular expression</source>
        <translation>JavaScript szabványos kifejezése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaScript regular expression</source>
        <translation>Inaktív JavaScript szabványos kifejezése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style C++ comment</source>
        <translation>JavaDoc stílusú C++ megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style C++ comment</source>
        <translation>Inaktív JavaDoc stílusú C++ megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Secondary keywords and identifiers</source>
        <translation>Másodlagos kulcsszavak és azonosítók</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive secondary keywords and identifiers</source>
        <translation>Inaktív másodlagos kulcsszavak és azonosítók</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc keyword</source>
        <translation>JavaDoc kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc keyword</source>
        <translation>Inaktív JavaDoc kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc keyword error</source>
        <translation>JavaDoc kulcsszóhiba</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc keyword error</source>
        <translation>Inaktív JavaDoc kulcsszóhiba</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Global classes and typedefs</source>
        <translation>Globális osztályok és típusdefiníciók</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive global classes and typedefs</source>
        <translation>Inaktív globális osztályok és típusdefiníciók</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>C++ raw string</source>
        <translation>C++ nyers karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive C++ raw string</source>
        <translation>Inaktív C++ nyers karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vala triple-quoted verbatim string</source>
        <translation>Hármas-idézett szó szerinti Vala-karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive Vala triple-quoted verbatim string</source>
        <translation>Inaktív hármas-idézett szó szerinti Vala-karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pike hash-quoted string</source>
        <translation>Kettőskereszt-idézett Pike-karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive Pike hash-quoted string</source>
        <translation>Inaktív kettőskereszt-idézett Pike-karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pre-processor C comment</source>
        <translation>Előfeldolgozó C megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive pre-processor C comment</source>
        <translation>Inaktív előfeldolgozó C megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>JavaDoc style pre-processor comment</source>
        <translation>Előfeldolgozó JavaDoc stílusú megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive JavaDoc style pre-processor comment</source>
        <translation>Inaktív előfeldolgozó JavaDoc stílusú megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>User-defined literal</source>
        <translation>Felhasználó által meghatározott szövegállandó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive user-defined literal</source>
        <translation>Inaktív felhasználó által meghatározott szövegállandó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Task marker</source>
        <translation>Feladatjelölő</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive task marker</source>
        <translation>Inaktív feladatjelölő</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Escape sequence</source>
        <translation>Vezérlősorozat</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inactive escape sequence</source>
        <translation>Inaktív vezérlősorozat</translation>
    </message>
</context>
<context>
    <name>QsciLexerDiff</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerdiff.cpp" line="+106"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command</source>
        <translation>Parancs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Header</source>
        <translation>Fejléc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Position</source>
        <translation>Szövegbeosztás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed line</source>
        <translation>Sor eltávolítva</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added line</source>
        <translation>Sor hozzáadva</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Changed line</source>
        <translation>Sor megváltozva</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added adding patch</source>
        <translation>Javítókészlet hozzáadása hozzáadva</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed adding patch</source>
        <translation>Javítókészlet hozzáadása eltávolítva</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Added removing patch</source>
        <translation>Javítókészlet eltávolítása hozzáadva</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Removed removing patch</source>
        <translation>Javítókészlet eltávolítása eltávolítva</translation>
    </message>
</context>
<context>
    <name>QsciLexerMatlab</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexermatlab.cpp" line="+133"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command</source>
        <translation>Parancs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Szám</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Egyszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Művelet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Azonosító</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Kétszeres-idézett karakterlánc</translation>
    </message>
</context>
<context>
    <name>QsciLexerPerl</name>
    <message>
        <location filename="build_ts/octave-qsci/qscilexerperl.cpp" line="+328"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>POD</source>
        <translation>POD</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number</source>
        <translation>Szám</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation>Kulcsszó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string</source>
        <translation>Kétszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted string</source>
        <translation>Egyszeres-idézett karakterlánc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation>Művelet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Identifier</source>
        <translation>Azonosító</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scalar</source>
        <translation>Skalár</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Array</source>
        <translation>Tömb</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hash</source>
        <translation>Kettőskereszt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol table</source>
        <translation>Szimbólumtábla</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Regular expression</source>
        <translation>Szabványos kifejezés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Substitution</source>
        <translation>Helyettesítés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks</source>
        <translation>Fordított ékezetek</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data section</source>
        <translation>Adatok szakasz</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Here document delimiter</source>
        <translation>Itteni dokumentum határoló</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single-quoted here document</source>
        <translation>Egyszeres-idézett itteni dokumentum</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted here document</source>
        <translation>Kétszeres-idézett itteni dokumentum</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backtick here document</source>
        <translation>Fordított ékezet itteni dokumentum</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (q)</source>
        <translation>Idézett karakterlánc (q)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qq)</source>
        <translation>Idézett karakterlánc (qq)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qx)</source>
        <translation>Idézett karakterlánc (qx)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qr)</source>
        <translation>Idézett karakterlánc (qr)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qw)</source>
        <translation>Idézett karakterlánc (qw)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>POD verbatim</source>
        <translation>POD szó szerint</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Subroutine prototype</source>
        <translation>Alprogram prototípusa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Format identifier</source>
        <translation>Azonosító formázása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Format body</source>
        <translation>Törzs formázása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted string (interpolated variable)</source>
        <translation>Kétszeres-idézett karakterlánc (interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Translation</source>
        <translation>Fordítás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Regular expression (interpolated variable)</source>
        <translation>Szabványos kifejezés (interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Substitution (interpolated variable)</source>
        <translation>Helyettesítés (interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backticks (interpolated variable)</source>
        <translation>Fordított ékezetek (interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double-quoted here document (interpolated variable)</source>
        <translation>Kétszeres-idézett itteni dokumentum (interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Backtick here document (interpolated variable)</source>
        <translation>Fordított ékezet itteni dokumentum (interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qq, interpolated variable)</source>
        <translation>Idézett karakterlánc (qq, interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qx, interpolated variable)</source>
        <translation>Idézett karakterlánc (qx, interpolált változó)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quoted string (qr, interpolated variable)</source>
        <translation>Idézett karakterlánc (qr, interpolált változó)</translation>
    </message>
</context>
<context>
    <name>QsciScintilla</name>
    <message>
        <location filename="build_ts/octave-qsci/qsciscintilla.cpp" line="+4478"/>
        <source>&amp;Undo</source>
        <translation>&amp;Visszavonás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>&amp;Ismét</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Kivágás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>&amp;Másolás</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>Beilles&amp;ztés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
</context>
<context>
    <name>UrlFilter</name>
    <message>
        <location filename="__octave_temp_gui_sources__/qterminal/libqterminal/unix/Filter.cpp" line="+630"/>
        <source>Open Link</source>
        <translation>Hivatkozás megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Link Address</source>
        <translation>Hivatkozás címének másolása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Send Email To...</source>
        <translation>E-mail küldése…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Email Address</source>
        <translation>E-mail-cím másolása</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Edit %1 at line %2</source>
        <translation>%1 szerkesztése a(z) %2 sorban</translation>
    </message>
</context>
<context>
    <name>annotation_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/graphics/annotation-dialog.ui" line="+17"/>
        <source>Annotation</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Text</source>
        <translation>Szöveg</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>String</source>
        <translation>Karakterlánc</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Font</source>
        <translation>Betűkészlet</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>bold</source>
        <translation>Félkövér</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>b</source>
        <translation>F</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>italic</source>
        <translation>Dőlt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>i</source>
        <translation>D</translation>
    </message>
    <message>
        <location line="+428"/>
        <source>color</source>
        <translation>Szín</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>left</source>
        <translation>Balra zárt</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+29"/>
        <source>middle</source>
        <translation>Középre igazítás</translation>
    </message>
    <message>
        <location line="-24"/>
        <source>right</source>
        <translation>Jobbra zárt</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Horizontal alignment</source>
        <translation>Vízszintes igazítás</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Vertical alignment</source>
        <translation>Függőleges igazítás</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>top</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>bottom</source>
        <translation>Le</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Position</source>
        <translation>Szövegbeosztás</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Width</source>
        <translation>Szélesség</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Height</source>
        <translation>Magasság</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>units</source>
        <translation>Egység</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>normalized</source>
        <translation>Normalizált</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Fit to box</source>
        <translation>Mezőhöz igazítás</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Box</source>
        <translation>Mező</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Background</source>
        <translation>Háttér</translation>
    </message>
    <message>
        <location line="+444"/>
        <source>Edge</source>
        <translation>Él</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Line style</source>
        <translation>Vonalstílus</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>none</source>
        <translation>Nincs</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Line width</source>
        <translation>Vonalvastagság</translation>
    </message>
</context>
<context>
    <name>octave::ListDialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/dialog.cc" line="+454"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
</context>
<context>
    <name>octave::command_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/command-widget.cc" line="+76"/>
        <source>Pause</source>
        <translation>Szüneteltetés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>Leállítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Continue</source>
        <translation>Folytatás</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Command Widget</source>
        <translation>Parancsvezérlő</translation>
    </message>
</context>
<context>
    <name>octave::community_news</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/community-news.cc" line="+70"/>
        <source>Octave Community News</source>
        <translation>Oktáv közösségi hírek</translation>
    </message>
</context>
<context>
    <name>octave::console_lexer</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/console-lexer.cc" line="+48"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prompt</source>
        <translation>Kérdezés</translation>
    </message>
</context>
<context>
    <name>octave::documentation</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation.cc" line="+135"/>
        <location line="+14"/>
        <location line="+715"/>
        <source>Octave Documentation</source>
        <translation>Oktáv-dokumentáció</translation>
    </message>
    <message>
        <location line="-728"/>
        <source>Could not copy help collection to temporary
file. Search capabilities may be affected.
%1</source>
        <translation>Nem sikerült átmásolni a súgógyűjteményt az ideiglenes
fájlba. Ez hatással lehet a keresési képességekre.
%1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Could not setup the data required for the
documentation viewer. Maybe the Qt SQlite
module is missing?
Only help text in the Command Window will
be available.</source>
        <translation>Nem sikerült beállítani a dokumentációnézőhöz
szükséges adatokat. Lehet, hogy a Qt SQLite
modul hiányzik?
Csak a parancsablak súgószövege lesz elérhető.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Contents</source>
        <translation>Tartalom</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter text to search function index</source>
        <translation>Szöveg megadása a függvénytárgymutatóban való kereséshez</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+51"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location line="-35"/>
        <source>Function Index</source>
        <translation>Függvénytárgymutató</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Bookmarks</source>
        <translation>Könyvjelzők</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Go home</source>
        <translation>Kezdőoldal</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Go back</source>
        <translation>Vissza</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Previous pages</source>
        <translation>Előző oldalak</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Go forward</source>
        <translation>Előre</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Next pages</source>
        <translation>Következő oldalak</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Zoom In</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom Out</source>
        <translation>Kicsinyítés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom Original</source>
        <translation>Valóságos méret</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Bookmark current page</source>
        <translation>A jelenlegi oldal megjelölése könyvjelzővel</translation>
    </message>
    <message>
        <location line="+424"/>
        <source>Unable to register help file %1.</source>
        <translation>Nem sikerült regisztrálni a(z) %1 súgófájlt.</translation>
    </message>
</context>
<context>
    <name>octave::documentation_bookmarks</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation-bookmarks.cc" line="+102"/>
        <source>
No documentation bookmarks loaded!</source>
        <translation>
Nincsenek betöltve dokumentációs könyvjelzők!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Octave: Loading Documentation Bookmarks</source>
        <translation>Oktáv: Dokumentációs könyvjelzők betöltése</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter text to search the bookmarks</source>
        <translation>Szöveg megadása a könyvjelzőkben való kereséshez</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>New Folder</source>
        <translation>Új mappa</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>&amp;Open</source>
        <translation>&amp;Megnyitás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Rename</source>
        <translation>Átne&amp;vezés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remo&amp;ve</source>
        <translation>&amp;Eltávolítás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Add Folder</source>
        <translation>Mappa &amp;hozzáadása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Hide &amp;Filter</source>
        <translation>Szűrő &amp;elrejtése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show &amp;Filter</source>
        <translation>Szűrő &amp;megjelenítése</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>Octave: Saving Documentation Bookmarks</source>
        <translation>Oktáv: Dokumentációs könyvjelzők mentése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to write file %1:
%2.

Documentation bookmarks are not saved!
</source>
        <translation>A(z) %1 fájl nem írható:
%2.

A dokumentációs könyvjelzők nem kerülnek mentésre!
</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Unable to read file %1:
%2.</source>
        <translation>A(z) %1 fájl nem olvasható:
%2.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No start element found in %1.
Invalid bookmark file?</source>
        <translation>Nem található kezdőelem a következőben: %1.
Érvénytelen könyvjelzőfájl?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The file
%1
is not a valid XBEL file version 1.0.</source>
        <translation>A fájl
%1
nem érvényes XBEL-fájl 1.0-s verziója.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Unknown title</source>
        <translation>Ismeretlen cím</translation>
    </message>
</context>
<context>
    <name>octave::documentation_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/documentation-dock-widget.cc" line="+40"/>
        <source>Documentation</source>
        <translation>Dokumentáció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>See the documentation for help.</source>
        <translation>Segítségért tekintse meg a dokumentációt.</translation>
    </message>
</context>
<context>
    <name>octave::dw_main_window</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/dw-main-window.cc" line="+53"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close &amp;All</source>
        <translation>Az összes be&amp;zárása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close &amp;Other</source>
        <translation>&amp;Másik bezárása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Switch to &amp;Left Widget</source>
        <translation>Váltás a &amp;bal oldali vezérlőre</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Switch to &amp;Right Widget</source>
        <translation>Váltás a &amp;jobb oldali vezérlőre</translation>
    </message>
</context>
<context>
    <name>octave::external_editor_interface</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/external-editor-interface.cc" line="+68"/>
        <location line="+50"/>
        <source>Octave Editor</source>
        <translation>Oktáv-szerkesztő</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Could not start custom file editor
%1</source>
        <translation>Nem sikerült elindítani az egyéni fájlszerkesztőt
%1</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>There is no custom editor configured yet.
Do you want to open the preferences?</source>
        <translation>Egyéni szerkesztő még nincs beállítva.
Szeretné megnyitni a beállításokat?</translation>
    </message>
</context>
<context>
    <name>octave::file_editor</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/file-editor.cc" line="+194"/>
        <source>Continue</source>
        <translation>Folytatás</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Save File and Run</source>
        <translation>Fájl mentése és futtatása</translation>
    </message>
    <message>
        <location line="+1002"/>
        <location line="+29"/>
        <location line="+565"/>
        <location line="+18"/>
        <location line="+25"/>
        <source>Octave Editor</source>
        <translation>Oktáv-szerkesztő</translation>
    </message>
    <message>
        <location line="-636"/>
        <source>File not saved! A file with the selected name
%1
is already open in the editor.</source>
        <translation>A fájl nincs mentve! Egy fájl a kijelölt névvel
%1
már meg van nyitva a szerkesztőben.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The associated file editor tab has disappeared.</source>
        <translation>A hozzátartozó fájlszerkesztő lap eltűnt.</translation>
    </message>
    <message>
        <location line="+565"/>
        <source>Could not open file
%1
for reading: %2.</source>
        <translation>Nem sikerült megnyitni a fájlt
%1
olvasáshoz: %2.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>File
%1
does not exist. Do you want to create it?</source>
        <translation>Fájl
%1
nem létezik. Szeretné létrehozni?</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Create</source>
        <translation>Létrehozás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not open file
%1
for writing: %2.</source>
        <translation>Nem sikerült megnyitni a fájlt
%1
íráshoz: %2.</translation>
    </message>
    <message>
        <location line="+271"/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Recent Editor Files</source>
        <translation>Legutóbbi szerkesztő&amp;fájlok</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Edit Function</source>
        <translation>Függvény s&amp;zerkesztése</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Save File</source>
        <translation>Fájl m&amp;entése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save File &amp;As...</source>
        <translation>Fájl me&amp;ntése másként…</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close All</source>
        <translation>Az összes bezárása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close Other Files</source>
        <translation>Más fájlok bezárása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Print...</source>
        <translation>Nyomtatás…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Edit</source>
        <translation>S&amp;zerkesztés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>&amp;Ismét</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Kivágás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Find and Replace...</source>
        <translation>Keresés és &amp;csere…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find &amp;Next</source>
        <translation>Kö&amp;vetkező találat</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Find &amp;Previous</source>
        <translation>Elő&amp;ző találat</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Commands</source>
        <translation>&amp;Parancsok</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete Line</source>
        <translation>Sor törlése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy Line</source>
        <translation>Sor másolása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cut Line</source>
        <translation>Sor kivágása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete to Start of Word</source>
        <translation>Törlés a szó elejére</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to End of Word</source>
        <translation>Törlés a szó végére</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to Start of Line</source>
        <translation>Törlés a sor elejére</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete to End of Line</source>
        <translation>Törlés a sor végére</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Duplicate Selection/Line</source>
        <translation>Kijelölés/sor megkettőzése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transpose Line</source>
        <translation>Sor transzponálása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Show Completion List</source>
        <translation>&amp;Befejezési lista megjelenítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Format</source>
        <translation>&amp;Formátum</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Uppercase Selection</source>
        <translation>&amp;Nagybetűs kijelölése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Lowercase Selection</source>
        <translation>&amp;Kisbetűs kijelölése</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Comment</source>
        <translation>&amp;Megjegyzés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Uncomment</source>
        <translation>Megjegyzés megs&amp;züntetése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Comment (Choosing String)</source>
        <translation>Megjegyzés (karakterlánc kijelölése)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Indent Selection Rigidly</source>
        <translation>Kijelölés behúzásának &amp;növekedése rögzítettként</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Unindent Selection Rigidly</source>
        <translation>Kijelölés behúzásának &amp;csökkentése rögzítettként</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Indent Code</source>
        <translation>Kód behúzása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Convert Line Endings to &amp;Windows (CRLF)</source>
        <translation>Átalakítás a &amp;Windows (CR LF karakterpár) sorvégződésekre</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Convert Line Endings to &amp;Unix (LF)</source>
        <translation>Átalakítás az &amp;Unix (LF karakter) sorvégződésekre</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Convert Line Endings to Legacy &amp;Mac (CR)</source>
        <translation>Átalakítás az örökölt &amp;Mac (CR karakter) sorvégződésekre</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Navi&amp;gation</source>
        <translation>&amp;Navigáció</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Go &amp;to Line...</source>
        <translation>Ugrás &amp;sorra…</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Move to Matching Brace</source>
        <translation>Ugrás az egyező zárójelre</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select to Matching Brace</source>
        <translation>Egyező zárójel kijelölése</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Next Bookmark</source>
        <translation>Kö&amp;vetkező könyvjelző</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Pre&amp;vious Bookmark</source>
        <translation>Elő&amp;ző könyvjelző</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Toggle &amp;Bookmark</source>
        <translation>&amp;Könyvjelző kapcsolása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Remove All Bookmarks</source>
        <translation>Összes könyvjelző &amp;eltávolítása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Preferences...</source>
        <translation>&amp;Beállítások…</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Styles Preferences...</source>
        <translation>&amp;Stílusok beállításai…</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;View</source>
        <translation>&amp;Nézet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Editor</source>
        <translation>S&amp;zerkesztő</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show &amp;Line Numbers</source>
        <translation>&amp;Sorszámok megjelenítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Whitespace Characters</source>
        <translation>El&amp;választó karakterek (szóköz, sortörés vagy tabulátor) megjelenítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Line &amp;Endings</source>
        <translation>Sor&amp;végek megjelenítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Indentation Guides</source>
        <translation>&amp;Behúzási segédvonalainak megjelenítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Long Line &amp;Marker</source>
        <translation>Hosszú vonal &amp;jelölő megjelenítése</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;Toolbar</source>
        <translation>&amp;Eszköztár megjelenítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Statusbar</source>
        <translation>Állapotsor me&amp;gjelenítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show &amp;Horizontal Scrollbar</source>
        <translation>&amp;Vízszintes görgetősáv megjelenítése</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Zoom &amp;In</source>
        <translation>&amp;Nagyítás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Zoom &amp;Out</source>
        <translation>&amp;Kicsinyítés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Normal Size</source>
        <translation>&amp;Tényleges méret</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Sort Tabs Alphabetically</source>
        <translation>Lapok &amp;rendezése betűrendben</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Debug</source>
        <translation>&amp;Hibakeresés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Toggle &amp;Breakpoint</source>
        <translation>&amp;Töréspont kapcsolása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Next Breakpoint</source>
        <translation>Kö&amp;vetkező töréspont</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pre&amp;vious Breakpoint</source>
        <translation>Elő&amp;ző töréspont</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Remove All Breakpoints</source>
        <translation>Összes töréspont &amp;eltávolítása</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Run</source>
        <translation>&amp;Futtatás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save File and Run/Continue</source>
        <translation>Fájl mentése és futtatása/folytatása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Run &amp;Selection</source>
        <translation>&amp;Kijelölés futtatása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save File and Run All &amp;Tests</source>
        <translation>Fájl mentése és az összes &amp;teszt futtatása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save File and Run All &amp;Demos</source>
        <translation>Fájl mentése és az összes &amp;bemutató futtatása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Help</source>
        <translation>&amp;Súgó</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Help on Keyword</source>
        <translation>Kulcsszó &amp;súgóban</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Documentation on Keyword</source>
        <translation>Kulcsszó a &amp;dokumentációban</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Recent Files</source>
        <translation>Legutóbbi fájlok</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Copy Full File &amp;Path</source>
        <translation>Teljes fájl &amp;elérési út másolása</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Editor</source>
        <translation>Szerkesztő</translation>
    </message>
</context>
<context>
    <name>octave::file_editor_tab</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/file-editor-tab.cc" line="+161"/>
        <source>line:</source>
        <translation>sor:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>col:</source>
        <translation>oszlop:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>encoding:</source>
        <translation>kódolás:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>eol:</source>
        <translation>sorvég:</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Breakpoint condition</source>
        <translation>Töréspontfeltétel</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>ERROR: </source>
        <translation>HIBA: </translation>
    </message>
    <message>
        <location line="+77"/>
        <location line="+1260"/>
        <location line="+152"/>
        <location line="+20"/>
        <location line="+447"/>
        <location line="+111"/>
        <location line="+103"/>
        <location line="+38"/>
        <location line="+60"/>
        <location line="+57"/>
        <location line="+36"/>
        <source>Octave Editor</source>
        <translation>Oktáv-szerkesztő</translation>
    </message>
    <message>
        <location line="-2283"/>
        <source>Cannot add breakpoint to modified or unnamed file.
Save and add breakpoint, or cancel?</source>
        <translation>Nem lehet töréspontot hozzáadni a módosított vagy névtelen fájlhoz.
Menteni és hozzáadni a töréspontot, vagy megszakítani?</translation>
    </message>
    <message>
        <location line="+924"/>
        <source>Goto line</source>
        <translation>Ugrás sorra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Line number</source>
        <translation>Sorszám</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Comment selected text</source>
        <translation>Megjegyzés hozzáfűzése a kijelölt szöveghez</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment string to use:
</source>
        <translation>Megjegyzés hozzáfűzése a használt karakterlánchoz:
</translation>
    </message>
    <message>
        <location line="+140"/>
        <location line="+57"/>
        <source>&lt;unnamed&gt;</source>
        <translation>&lt;névtelen&gt;</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Do you want to cancel closing, save, or discard the changes?</source>
        <translation>Szeretné visszavonni a bezárást, menteni vagy elvetni a módosításokat?</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The file

  %1

is about to be closed but has been modified.  %2</source>
        <translation>A fájl

  %1

bezárás előtt áll, de módosították.  %2</translation>
    </message>
    <message>
        <location line="+152"/>
        <source>Unable to read file &apos;%1&apos;
with selected encoding &apos;%2&apos;: %3</source>
        <translation>Nem sikerült beolvasni a(z) „%1” fájlt
„%2” kijelölt kódolással: %3</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>There were problems reading the file
%1
with the selected encoding %2.

Modifying and saving the file might cause data loss!</source>
        <translation>Problémák adódtak a fájl olvasásával
%1
a kijelölt %2 kódolással.

A fájl módosítása és mentése adatvesztést okozhat!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Edit anyway</source>
        <translation>S&amp;zerkesztés mégis</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+45"/>
        <source>Chan&amp;ge encoding</source>
        <translation>Kódolás &amp;módosítása</translation>
    </message>
    <message>
        <location line="-43"/>
        <location line="+36"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Select new default encoding</source>
        <translation>Új alapértelmezett kódolás kijelölése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Please select a new encoding
for reloading the current file.

This does not change the default encoding.
</source>
        <translation>Kérjük, adjon meg új kódolást
a jelenlegi fájl újratöltéséhez.

Ez nem változtatja meg az alapértelmezett kódolást.
</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Debug or Save</source>
        <translation>Hibakeresés vagy mentés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This file is currently being executed.
Quit debugging and save?</source>
        <translation>Ez a fájl jelenleg végrehajtás alatt áll.
Kilép a hibakeresésből és menti?</translation>
    </message>
    <message>
        <location line="+214"/>
        <source>Could not open file %1 for writing:
%2.</source>
        <translation>Nem sikerült megnyitni a(z) %1 fájlt írásra:
%2.</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>The changes could not be saved to the file
%1</source>
        <translation>A módosításokat nem sikerült elmenteni a fájlba
%1</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Octave Files (*.m)</source>
        <translation>Oktáv-fájlok (*.m)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>All Files (*)</source>
        <translation>Minden fájl (*)</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>%1
 already exists
Do you want to overwrite it?</source>
        <translation>A(z) %1 fájl már létezik.
Szeretné felülírni?</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>&quot;%1&quot;
is not a valid identifier.

If you keep this filename, you will not be able to
call your script using its name as an Octave command.

Do you want to choose another name?</source>
        <translation>„%1”
nem érvényes azonosító.

Ha ezt a fájlnevet megtartja, a parancsfájl nem lesz
hívható a nevét oktáv-parancsként használva.

Más nevet szeretne választani?</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>The current editor contents can not be encoded
with the selected encoding %1.
Using it would result in data loss!

Please select another one!</source>
        <translation>A jelenlegi szerkesztő tartalma nem kódolható a
%1 kijelölt kódolással.
Használata adatvesztéshez vezet!

Kérjük, válasszon másikat!</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>It seems that &apos;%1&apos; has been modified by another application. Do you want to reload it?</source>
        <translation>Úgy tűnik, hogy a(z) „%1” fájlt egy másik alkalmazás módosította. Szeretné újratölteni?</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>

Warning: The contents in the editor is modified!</source>
        <translation>

Figyelmeztetés: A szerkesztő tartalma módosult!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>It seems that the file
%1
has been deleted or renamed. Do you want to save it now?%2</source>
        <translation>Úgy tűnik, hogy a fájl
%1
törölve vagy átnevezve. Szeretné most menteni? %2</translation>
    </message>
</context>
<context>
    <name>octave::files_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/files-dock-widget.cc" line="-4"/>
        <source>Could not rename file &quot;%1&quot; to &quot;%2&quot;.</source>
        <translation>Nem sikerült átnevezni a(z) „%1” fájlt a következőre: „%2”.</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>File Browser</source>
        <translation>Fájltallózó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse your files</source>
        <translation>Fájlok tallózása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>File size</source>
        <translation>Fájlméret</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File type</source>
        <translation>Fájltípus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date modified</source>
        <translation>Módosítás dátuma</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show hidden</source>
        <translation>Rejtettek megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alternating row colors</source>
        <translation>Változó sorszínek</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Enter the path or filename</source>
        <translation>Elérési ut vagy fájlneve megadása</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>One directory up</source>
        <translation>Egy könyvtárral feljebb</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Octave directory</source>
        <translation>Oktáv-könyvtár megjelenítése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go to current Octave directory</source>
        <translation>Ugrás a jelenlegi Oktáv-könyvtárba</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Octave directory</source>
        <translation>Oktáv-könyvtár beállítása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set Octave directory to current browser directory</source>
        <translation>Oktáv-könyvtár beállítása a jelenlegi könyvtártallózóra</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Actions on current directory</source>
        <translation>Műveletek a jelenlegi könyvtárban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Home Directory</source>
        <translation>Kezdőkönyvtár megjelenítése</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Set Browser Directory...</source>
        <translation>Könyvtártallózó beállítása…</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+438"/>
        <source>Find Files...</source>
        <translation>Fájlok keresése…</translation>
    </message>
    <message>
        <location line="-434"/>
        <location line="+446"/>
        <source>New File...</source>
        <translation>Új fájl…</translation>
    </message>
    <message>
        <location line="-443"/>
        <location line="+445"/>
        <source>New Directory...</source>
        <translation>Új könyvtár…</translation>
    </message>
    <message>
        <location line="-390"/>
        <source>Double-click to open file/folder, right click for alternatives</source>
        <translation>Kattintson duplán a fájl/mappa megnyitásához, jobb gombbal kattintson az egyéb lehetőségekért</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Rename...</source>
        <translation>Átnevezés…</translation>
    </message>
    <message>
        <location line="+317"/>
        <source>Open</source>
        <translation>Megnyitás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in System File Explorer</source>
        <translation>Megnyitás a rendszerfájlkezelőben</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in Text Editor</source>
        <translation>Megnyitás a szövegszerkesztőben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy Selection to Clipboard</source>
        <translation>Kijelölés másolása a vágólapra</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Run</source>
        <translation>Futtatás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Load Data</source>
        <translation>Adatok betöltése</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set Current Directory</source>
        <translation>Jelenlegi könyvtár beállítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add to Path</source>
        <translation>Elérési úthoz hozzáadása</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+7"/>
        <source>Selected Directories</source>
        <translation>Kijelölt könyvtárak</translation>
    </message>
    <message>
        <location line="-5"/>
        <location line="+7"/>
        <source>Selected Directories and Subdirectories</source>
        <translation>Kijelölt könyvtárak és alkönyvtárak</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Remove from Path</source>
        <translation>Eltávolítás az elérési útról</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Delete...</source>
        <translation>Törlés…</translation>
    </message>
    <message>
        <location line="+140"/>
        <location line="+11"/>
        <location line="+17"/>
        <source>Delete file/directory</source>
        <translation>Fájl/könyvtár törlése</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Are you sure you want to delete all %1 selected files?
</source>
        <translation>Biztosan törli az összes %1 kijelölt fájlt?
</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Are you sure you want to delete
</source>
        <translation>Biztosan törölni szeretné
</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Can not delete a directory that is not empty</source>
        <translation>Nem törölhető a nem üres könyvtár</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Deletion error</source>
        <translation>Törlési hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Could not delete file &quot;%1&quot;.</source>
        <translation>Nem sikerült törölni a(z) „%1” fájlt.</translation>
    </message>
    <message>
        <location line="+216"/>
        <source>Set directory of file browser</source>
        <translation>Fájlböngésző könyvtárbeállítása</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Create File</source>
        <translation>Fájl létrehozása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Create file in
</source>
        <comment>String ends with 
!</comment>
        <translation>Fájl létrehozása a következő helyen:
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New File.txt</source>
        <translation>Új fájl.txt</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Create Directory</source>
        <translation>Könyvtár létrehozása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Create folder in
</source>
        <comment>String ends with 
!</comment>
        <translation>Mappa létrehozása a következő helyen:
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New Directory</source>
        <translation>Új könyvtár</translation>
    </message>
</context>
<context>
    <name>octave::final_page</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="+318"/>
        <source>Enjoy!</source>
        <translation>Készen áll a használatra!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Previous</source>
        <translation>Előző</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Finish</source>
        <translation>Befejezés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;We hope you find Octave to be a useful tool.&lt;/p&gt;
&lt;p&gt;If you encounter problems, there are a number of ways to get help, including commercial support options, a discussion board, a wiki, and other community-based support channels.
You can find more information about each of these by visiting &lt;a href=&quot;https://octave.org/support.html&quot;&gt;https://octave.org/support.html&lt;/a&gt; (opens in external browser).&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;
&lt;p&gt;Reméljük, hogy hasznos eszköznek találja az Oktávot.&lt;/p&gt;
&lt;p&gt;Ha hibákat tapasztal, számos módon kaphat segítséget, beleértve a kereskedelmi támogatási lehetőségeket, egy vitafórumot, egy wikit és más közösségi alapú támogatási csatornákat.
Ezekről további információkat találhat a &lt;a href=&quot;https://octave.org/support.html&quot;&gt;https://octave.org/support.html&lt;/a&gt; oldalon (külső böngészőben nyílik meg).&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;For more information about Octave:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Visit &lt;a href=&quot;https://octave.org&quot;&gt;https://octave.org&lt;/a&gt; (opens in external browser)&lt;/li&gt;
&lt;li&gt;Get the documentation online in &lt;a href=&quot;https://www.gnu.org/software/octave/doc/interpreter/index.html&quot;&gt;HTML&lt;/a&gt; or &lt;a href=&quot;https://www.gnu.org/software/octave/octave.pdf&quot;&gt;PDF&lt;/a&gt; format (links open in external browser)&lt;/li&gt;
&lt;li&gt;Open the documentation browser of the Octave GUI with the help menu&lt;/li&gt;
&lt;/ul&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;További információ az Oktávról:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Látogassa meg a &lt;a href=&quot;https://octave.org&quot;&gt;https://octave.org&lt;/a&gt; webhelyet (külső böngészőben nyílik meg)&lt;/li&gt;
&lt;li&gt;Szerezze be online a dokumentációt &lt;a href=&quot;https://www.gnu.org/software/octave/doc/interpreter/index.html&quot;&gt;HTML&lt;/a&gt; vagy &lt;a href=&quot;https://www. gnu.org/software/octave/octave.pdf&quot;&gt;PDF&lt;/a&gt; formátum (a hivatkozások külső böngészőben nyílnak meg)&lt;/li&gt;
&lt;li&gt;Nyissa meg az Oktáv grafikus felhasználói felület dokumentációs böngészőjét a súgó menüvel&lt;/li&gt;
&lt;/ul&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>octave::find_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/find-dialog.cc" line="+93"/>
        <source>Editor: Find and Replace</source>
        <translation>Szerkesztő: Keresés és csere</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Find:</source>
        <translation>&amp;Keresés:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter search text</source>
        <translation>Keresési szöveg megadása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Re&amp;place:</source>
        <translation>&amp;Csere:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter replacement text</source>
        <translation>Csereszöveg megadása</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Match &amp;case</source>
        <translation>Kis&amp;betűk és nagybetűk megkülönböztetése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search from &amp;start</source>
        <translation>Keresés az &amp;elejétől</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Wrap while searching</source>
        <translation>Keresés &amp;tördelése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Find &amp;Next</source>
        <translation>Kö&amp;vetkező találat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Pre&amp;vious</source>
        <translation>Elő&amp;ző találat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Replace</source>
        <translation>&amp;Csere</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Replace &amp;All</source>
        <translation>&amp;Az összes cseréje</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;More...</source>
        <translation>&amp;További lehetőségek…</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Whole words</source>
        <translation>Csak &amp;teljes szavak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Regular &amp;expressions</source>
        <translation>Szabványos ki&amp;fejezések</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search &amp;backward</source>
        <translation>Keresés &amp;visszafelé</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search se&amp;lection</source>
        <translation>&amp;Kijelölés keresése</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>Search from end</source>
        <translation>Keresés a végétől</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search from start</source>
        <translation>Keresés az elejétől</translation>
    </message>
    <message>
        <location line="+290"/>
        <source>Replace Result</source>
        <translation>Eredmény csere</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 items replaced</source>
        <translation>%1 elem cserélve</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Find Result</source>
        <translation>Keresés eredménye</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No more matches found</source>
        <translation>Nincs több találat</translation>
    </message>
</context>
<context>
    <name>octave::find_files_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-files-dialog.cc" line="+61"/>
        <source>Find Files</source>
        <translation>Fájlok keresése</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Named:</source>
        <translation>Elnevezett:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter the filename search patterns.
Several different patterns can be
separated by &apos;;&apos;, e.g. &apos;*.cc ; *.h&apos;</source>
        <translation>Adja meg a fájlnév-keresési mintákat.
Többféle minta lehet
„;”-vel elválasztva, pl. „*.cc ; *.h”</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start in:</source>
        <translation>Indítás helye:</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter the start directory</source>
        <translation>Kezdőkönyvtár megadása</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Browse...</source>
        <translation>Tallózás…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse for start directory</source>
        <translation>Tallózás a kezdőkönyvtárért</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Current Dir</source>
        <translation>Jelenlegi könyvtár</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set start directory to current directory</source>
        <translation>A kezdőkönyvtár beállítása a jelenlegi könyvtárra</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Search subdirectories</source>
        <translation>Alkönyvtárak keresése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search recursively through directories for matching files</source>
        <translation>Rekurzív keresése a fájlok egyeztetéséhez a könyvtárakban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Include directory names</source>
        <translation>Könyvtárnevek tartalmazása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Include matching directories in search results</source>
        <translation>Tartalmazza könyvtárak egyeztetése a keresési eredményekbe</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+18"/>
        <source>Ignore case</source>
        <translation>Kis- és nagybetűk figyelmen kívül hagyása</translation>
    </message>
    <message>
        <location line="-17"/>
        <location line="+18"/>
        <source>Perform case insensitive match</source>
        <translation>Kis- és nagybetűk közötti egyezés végrehajtása</translation>
    </message>
    <message>
        <location line="-15"/>
        <source>Contains text:</source>
        <translation>Szövegrészlet:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Include only files containing specified text in search results</source>
        <translation>Csak a megadott szöveget tartalmazó fájlokat jelenítse meg a keresési eredmények között</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Text to match</source>
        <translation>Egyező szöveg</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Idle.</source>
        <translation>Tétlen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Start search for matching files</source>
        <translation>Keresés indítása a fájlok egyeztetéséhez</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Stop</source>
        <translation>Leállítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop search</source>
        <translation>Keresés leállítása</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Filename/Location</source>
        <translation>Fájlnév/hely</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>File contents</source>
        <translation>Fájl tartalma</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Results: Double click opens the file or sets the directory</source>
        <translation>Eredmények: Dupla kattintással megnyitja a fájlt vagy beállítja a könyvtárat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search results</source>
        <translation>Keresés eredménye</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>Searching...</source>
        <translation>Keresés folyamatban van…</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>%1 match (es)</source>
        <translation>%1 találat</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Set search directory</source>
        <translation>Keresési könyvtár beállítása</translation>
    </message>
</context>
<context>
    <name>octave::find_files_model</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-files-model.cc" line="+96"/>
        <source>Filename</source>
        <translation>Fájlnév</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory</source>
        <translation>Könyvtár</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Double click to open the file</source>
        <translation>Kattintson duplán a fájl megnyitásához</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Double click to set the directory</source>
        <translation>Kattintson duplán a könyvtár beállításához</translation>
    </message>
</context>
<context>
    <name>octave::find_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/find-widget.cc" line="+48"/>
        <source>Find:</source>
        <translation>Keresés:</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+1"/>
        <source>Search forward</source>
        <translation>Keresés előre</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1"/>
        <source>Search backward</source>
        <translation>Keresés visszafelé</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close find dialog</source>
        <translation>Keresés párbeszédpanel bezárása</translation>
    </message>
</context>
<context>
    <name>octave::gui_settings</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-settings.cc" line="+824"/>
        <source>Error %1 creating the settings file
%2
Make sure you have read and write permissions to
%3

Octave GUI must be closed now.</source>
        <translation>Hiba %1 a beállítási fájl létrehozásakor
%2
Győződjön meg arról, hogy rendelkezik olvasási és írási jogosultsággal
%3

Az Oktáv grafikus felhasználói felületét most be kell zárni.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Octave Critical Error</source>
        <translation>Kritikus Oktáv-hiba</translation>
    </message>
</context>
<context>
    <name>octave::history_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/history-dock-widget.cc" line="+54"/>
        <source>Browse and search the command history.</source>
        <translation>Parancselőzmények tallózása és keresése.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Evaluate</source>
        <translation>Kiértékelés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create script</source>
        <translation>Parancsfájl létrehozása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Hide filter</source>
        <translation>Szűrő elrejtése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show filter</source>
        <translation>Szűrő megjelenítése</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>Double-click a command to transfer it to the Command Window.</source>
        <translation>Kattintson duplán egy parancsra a parancsablakba való átvitelhez.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter text to filter the command history</source>
        <translation>Szöveg megadása a parancselőzmények szűréséhez</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Command History</source>
        <translation>Parancselőzmények</translation>
    </message>
</context>
<context>
    <name>octave::initial_page</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="-190"/>
        <source>Welcome to Octave!</source>
        <translation>Isten hozott az Oktávnál!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Next</source>
        <translation>Tovább</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;You seem to be using the Octave graphical interface for the first time on this computer.
Click &apos;Next&apos; to create a configuration file and launch Octave.&lt;/p&gt;
&lt;p&gt;The configuration file is stored in&lt;br&gt;%1.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;
&lt;p&gt;Úgy tűnik, először használja az Oktáv grafikus felhasználói felületét ezen az eszközén.
Kattintson a „Tovább” gombra a beállítási fájl létrehozásához és az Oktáv elindításához.&lt;/p&gt;
&lt;p&gt;A beállítási fájl a található a következő címen:&lt;br&gt;%1&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>octave::label_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/octave-dock-widget.cc" line="+82"/>
        <source>Undock Widget</source>
        <translation>Vezérlő rögzítésének feloldása</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Close Widget</source>
        <translation>Vezérlő bezárása</translation>
    </message>
</context>
<context>
    <name>octave::main_window</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/main-window.cc" line="+155"/>
        <source>Profiler</source>
        <translation>Profilkészítő</translation>
    </message>
    <message>
        <location line="+361"/>
        <source>Octave</source>
        <translation>Oktáv</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to exit Octave?</source>
        <translation>Biztosan kilép az Oktávból?</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Save Workspace As</source>
        <translation>Munkaterület mentése másként</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Load Workspace</source>
        <translation>Munkaterület megnyitása</translation>
    </message>
    <message>
        <location line="+214"/>
        <location line="+1747"/>
        <source>About Octave</source>
        <translation>Az Oktáv névjegye</translation>
    </message>
    <message>
        <location line="-1560"/>
        <location line="+1635"/>
        <source>Browse directories</source>
        <translation>Könyvtárak tallózása</translation>
    </message>
    <message>
        <location line="-1330"/>
        <source>Octave Files (*.m);;All Files (*)</source>
        <translation>Oktáv-fájlok (*.m);;Minden fájl (*)</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>New Function</source>
        <translation>Új függvény</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New function name:
</source>
        <translation>Új függvénynév:
</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>%1 is a built-in, compiled, or inline
function and can not be edited.</source>
        <translation>A(z) %1 egy beépített, lefordított vagy soron belüli
függvény, és nem szerkeszthető.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Can not find function %1</source>
        <translation>Nem található a(z) %1 függvény</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Octave Editor</source>
        <translation>Oktáv-szerkesztő</translation>
    </message>
    <message>
        <location line="+736"/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open...</source>
        <translation>Megnyitás…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open an existing file in editor</source>
        <translation>Meglévő fájl megnyitása a szerkesztőben</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Load Workspace...</source>
        <translation>Munkaterület megnyitása…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save Workspace As...</source>
        <translation>Munkaterület mentése másként…</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Exit</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>New</source>
        <translation>Új</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Script</source>
        <translation>Új parancsfájl</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New Function...</source>
        <translation>Új függvény…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New Figure</source>
        <translation>Új ábra</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Edit</source>
        <translation>S&amp;zerkesztés</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Undo</source>
        <translation>Visszavonás</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear Clipboard</source>
        <translation>Vágólap kiürítése</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Find Files...</source>
        <translation>Fájlok keresése…</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear Command Window</source>
        <translation>Parancsablak kiürítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Command History</source>
        <translation>Parancselőzmények kiürítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Workspace</source>
        <translation>Munkaterület kiürítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Set Path...</source>
        <translation>Elérési útvonal beállítása…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Preferences...</source>
        <translation>Beállítások…</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>De&amp;bug</source>
        <translation>&amp;Hibakeresés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Step</source>
        <translation>Lépés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Step In</source>
        <translation>Előrelépés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Step Out</source>
        <translation>Visszalépés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue</source>
        <translation>Folytatás</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Quit Debug Mode</source>
        <translation>Hibakeresési mód kilépése</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Tools</source>
        <translation>&amp;Eszközök</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start &amp;Profiler Session</source>
        <translation>&amp;Profilkészítő munkamenet indítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Resume Profiler Session</source>
        <translation>Profilkészítő munkamenet &amp;folytatása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Stop Profiler</source>
        <translation>Profilkészítő munkamenet &amp;leállítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Show Profiler Data</source>
        <translation>Profiladatok &amp;megjelenítése</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>&amp;Window</source>
        <translation>&amp;Ablak</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Command Window</source>
        <translation>Parancsablak megjelenítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Command History</source>
        <translation>Parancselőzmények megjelenítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show File Browser</source>
        <translation>Fájltallózó megjelenítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Workspace</source>
        <translation>Munkaterület megjelenítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Editor</source>
        <translation>Szerkesztő megjelenítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Documentation</source>
        <translation>Dokumentáció megjelenítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Variable Editor</source>
        <translation>Változószerkesztő megjelenítése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Command Window</source>
        <translation>Parancsablak</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Command History</source>
        <translation>Parancselőzmények</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>File Browser</source>
        <translation>Fájltallózó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Workspace</source>
        <translation>Munkaterület</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Editor</source>
        <translation>Szerkesztő</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+46"/>
        <source>Documentation</source>
        <translation>Dokumentáció</translation>
    </message>
    <message>
        <location line="-43"/>
        <source>Variable Editor</source>
        <translation>Változószerkesztő</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Previous Widget</source>
        <translation>Előző vezérlő</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset Default Window Layout</source>
        <translation>Alapértelmezett ablakelrendezés visszaállítása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Help</source>
        <translation>&amp;Súgó</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Report Bug</source>
        <translation>Hibabejelentés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Octave Packages</source>
        <translation>Oktáv-csomagok</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Get Involved</source>
        <translation>Vegyen részt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Donate to Octave</source>
        <translation>Oktáv adományozása</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>On Disk</source>
        <translation>Kapcsolat nélküli</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Online</source>
        <translation>Kapcsolatban van</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;News</source>
        <translation>&amp;Hírek</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Release Notes</source>
        <translation>Kiadási megjegyzések</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Community News</source>
        <translation>Közösségi hírek</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Toolbar</source>
        <translation>Eszköztár</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Enter directory name</source>
        <translation>Könyvtárnév megadása</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Current Directory: </source>
        <translation>Jelenlegi könyvtár: </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>Egy könyvtárral feljebb</translation>
    </message>
</context>
<context>
    <name>octave::news_reader</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/news-reader.cc" line="+106"/>
        <source>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Octave&apos;s community news source seems to be unavailable.
&lt;/p&gt;
&lt;p&gt;
For the latest news, please check
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
when you have a connection to the web (link opens in an external browser).
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; The Octave Developers, </source>
        <translation>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Úgy tűnik, hogy az Oktáv közösségi hírforrása nem érhető el.
&lt;/p&gt;
&lt;p&gt;
A legfrissebb hírekért ellenőrizze
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
amikor csatlakozik az internethez (a hivatkozás külső böngészőben nyílik meg).
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; Az Oktáv fejlesztők, </translation>
    </message>
    <message>
        <location line="+18"/>
        <source>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
Connecting to the web to display the latest Octave Community news has been disabled.
&lt;/p&gt;
&lt;p&gt;
For the latest news, please check
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
when you have a connection to the web (link opens in an external browser)
or enable web connections for news in Octave&apos;s network settings tab.
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; The Octave Developers, </source>
        <translation>&lt;html&gt;
&lt;body&gt;
&lt;p&gt;
A legfrissebb Oktáv-közösségi hírek megjelenítéséhez való internetkapcsolat letiltásra került.
&lt;/p&gt;
&lt;p&gt;
A legfrissebb hírekért kérjük, látogasson el a
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;
oldalra, ha van kapcsolata a weben (a hivatkozás külső böngészőben nyílik meg),
vagy engedélyezze a webkapcsolatokat a hírekhez az Oktáv hálózati beállítások lapján.
&lt;/p&gt;
&lt;p&gt;
&lt;small&gt;&lt;em&gt;&amp;mdash; Az Oktáv fejlesztői, </translation>
    </message>
</context>
<context>
    <name>octave::octave_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/octave-dock-widget.cc" line="+144"/>
        <source>Hide Widget</source>
        <translation>Vezérlő elrejtése</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>Dock Widget</source>
        <translation>Vezérlő rögzítése</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Undock Widget</source>
        <translation>Vezérlő rögzítésének feloldása</translation>
    </message>
</context>
<context>
    <name>octave::octave_qscintilla</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/octave-qscintilla.cc" line="+302"/>
        <source>Help on</source>
        <translation>Segítség keresése a következőhöz:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Documentation on</source>
        <translation>Dokumentáció keresése a következőhöz:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit</source>
        <translation>Szerkesztés</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>dbstop if ...</source>
        <translation>dbstop ha…</translation>
    </message>
    <message>
        <location line="+544"/>
        <source>Octave Editor</source>
        <translation>Oktáv-szerkesztő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Creating temporary files failed.
Make sure you have write access to temp. directory
%1

&quot;Run Selection&quot; requires temporary files.</source>
        <translation>Nem sikerült az ideiglenes fájlok létrehozása.
Győződjön meg arról, hogy rendelkezik írási jogosultsággal a(z) %1 ideiglenes könyvtárhoz.

A „Kijelölés futtatása” ideiglenes fájlokat igényel.</translation>
    </message>
    <message>
        <location line="+313"/>
        <source>Press &apos;%1&apos; to replace all occurrences of &apos;%2&apos; with &apos;%3&apos;.</source>
        <translation>Cserélése a(z) „%2” elem minden olyan előfordulását a(z) „%3” elemmel, a(z) „%1” gomb megnyomással.</translation>
    </message>
</context>
<context>
    <name>octave::octave_txt_lexer</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/m-editor/octave-txt-lexer.cc" line="+41"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
</context>
<context>
    <name>octave::qt_interpreter_events</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/qt-interpreter-events.cc" line="+357"/>
        <location line="+5"/>
        <location line="+2"/>
        <source>Create</source>
        <translation>Létrehozás</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>File
%1
does not exist. Do you want to create it?</source>
        <translation>Fájl
%1
nem létezik. Szeretné létrehozni?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Octave Editor</source>
        <translation>Oktáv-szerkesztő</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>The file %1 does not exist in the load path.  To run or debug the function you are editing, you must either change to the directory %2 or add that directory to the load path.</source>
        <translation>A(z) %1 fájl nem létezik a betöltési elérési úton. A szerkesztett függvény futtatásához vagy hibakereséséhez vagy át kell váltania a(z) %2 könyvtárba, vagy hozzá kell adnia a könyvtárat a betöltési útvonalhoz.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The file %1 is shadowed by a file with the same name in the load path. To run or debug the function you are editing, change to the directory %2.</source>
        <translation>A(z) %1 fájlt a betöltési útvonal azonos nevű fájlja árnyékolja. A szerkesztett függvény futtatásához vagy hibakereséséhez váltson a(z) %2 könyvtárba.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Change Directory or Add Directory to Load Path</source>
        <translation>Könyvtár módosítása vagy könyvtár hozzáadása a betöltési útvonalhoz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Directory</source>
        <translation>Könyvtár &amp;módosítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add Directory to Load Path</source>
        <translation>Könyvtár &amp;hozzáadása a betöltési útvonalhoz</translation>
    </message>
</context>
<context>
    <name>octave::release_notes</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/release-notes.cc" line="+84"/>
        <source>The release notes file &apos;%1&apos; is empty.</source>
        <translation>A kiadási megjegyzések fájlja „%1” üres.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The release notes file &apos;%1&apos; cannot be read.</source>
        <translation>A kiadási megjegyzések fájlja „%1” nem olvasható.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Octave Release Notes</source>
        <translation>Oktáv kiadási megjegyzések</translation>
    </message>
</context>
<context>
    <name>octave::set_path_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/set-path-dialog.cc" line="+64"/>
        <source>Set Path</source>
        <translation>Elérési útvonal beállítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>All changes take effect immediately.</source>
        <translation>Összes módosítás azonnal érvénybe lép.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add Directory...</source>
        <translation>Könyvtár hozzáadása…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Single Directory</source>
        <translation>Egyetlen könyvtár</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory With Subdirectories</source>
        <translation>Könyvtár alkönyvtárakkal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move to Top</source>
        <translation>Áthelyezés felülre</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to Bottom</source>
        <translation>Áthelyezés alulra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Up</source>
        <translation>Áthelyezés felfelé</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Down</source>
        <translation>Áthelyezés lefelé</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload</source>
        <translation>Újratöltés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Revert</source>
        <translation>Visszaállítás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Revert Last Change</source>
        <translation>Utolsó módosítás visszaállítása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Revert All Changes</source>
        <translation>Minden módosítás visszaállítása</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Open Directory</source>
        <translation>Könyvtár megnyitása</translation>
    </message>
</context>
<context>
    <name>octave::settings_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ed.h" line="+161"/>
        <source>Top</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bottom</source>
        <translation>Le</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>Balra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>Jobbra</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-cs.h" line="+61"/>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+56"/>
        <source>Foreground</source>
        <translation>Előtér</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+1"/>
        <source>Background</source>
        <translation>Háttér</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selection</source>
        <translation>Kijelölés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cursor</source>
        <translation>Jelölő</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ve.h" line="+1"/>
        <source>Selected Foreground</source>
        <translation>Kijelölt előtér</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selected Background</source>
        <translation>Kijelölt háttér</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alternating Background</source>
        <translation>Változó háttér</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ws.h" line="+70"/>
        <source>argument</source>
        <translation>Argumentum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>global</source>
        <translation>Globális</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>persistent</source>
        <translation>tartós</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-global.h" line="+193"/>
        <source>Environment Variables</source>
        <translation>Környezeti változók</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-sd.h" line="+41"/>
        <source>Second color mode (light/dark)</source>
        <translation>Második színmód (világos/sötét)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Switch to a second set of colors.
Useful for defining light/dark modes.
Discards non-applied current changes!</source>
        <translation>Váltson egy második színkészletre.
Hasznos a világos/sötét módok meghatározásához.
Elveti a nem alkalmazott jelenlegi változtatásokat!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reload the default colors,
depends on currently selected mode.</source>
        <translation>Alapértelmezett színek újratöltése,
a jelenleg kijelölt módtól függ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Reload the default style values,
depends on currently selected mode.</source>
        <translation>Alapértelmezett stílusértékek újratöltése,
a jelenleg kijelölt módtól függ.</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>&amp;Reload default colors</source>
        <translation>Alapértelmezett színek új&amp;ratöltése</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Reload default styles</source>
        <translation>Alapértelmezett stílusok új&amp;ratöltése</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.cc" line="-972"/>
        <source>Loading current preferences ... </source>
        <translation>Jelenlegi beállítások betöltése … </translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+6"/>
        <location line="+1088"/>
        <source>System setting</source>
        <translation>Rendszerbeállítás</translation>
    </message>
    <message>
        <location line="-834"/>
        <source>IBeam Cursor</source>
        <translation>IBeam-jelölő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Block Cursor</source>
        <translation>Blokkjelölő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Underline Cursor</source>
        <translation>Aláhúzott jelölő</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Color of highlighted current line (magenta (255,0,255) for automatic color)</source>
        <translation>A kiemelt jelenlegi vonal színe (bíbor (255,0,255) az automatikus színhez)</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Set Octave Startup Directory</source>
        <translation>Oktáv indítási könyvtár beállítása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set File Browser Startup Directory</source>
        <translation>Fájltallózó indítási könyvtár beállítása</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Applying preferences ... </source>
        <translation>Beállítások alkalmazása folyamatban van…</translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+22"/>
        <location line="+1120"/>
        <source>Failed to open %1 as Octave shortcut file</source>
        <translation>Nem sikerült megnyitni a(z) „%1” fájlt Oktáv gyorsítóparancsfájlként</translation>
    </message>
    <message>
        <location line="-453"/>
        <source>Enable attribute colors</source>
        <translation>Attribútum színeinek engedélyezése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide tool tips</source>
        <translation>Eszköztippek elrejtése</translation>
    </message>
    <message>
        <location line="+384"/>
        <source>Import shortcuts from file...</source>
        <translation>Gyorsítóparancsok importálása fájlból…</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+6"/>
        <source>Octave Shortcut Files (*.osc);;All Files (*)</source>
        <translation>Oktáv-gyorsítóparancsfájlok (*.osc);;Minden fájl (*)</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Export shortcuts to file...</source>
        <translation>Gyorsítóparancsok exportálása fájlba…</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Overwriting Shortcuts</source>
        <translation>Gyorsítóparancsok felülírása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are about to overwrite all shortcuts.
Would you like to save the current shortcut set or cancel the action?</source>
        <translation>Az összes gyorsítóparancsok felülírása kerül.
Menteni szeretné a jelenlegi gyorsítóparancs-készletet, vagy törölni szeretné a műveletet?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Don&apos;t save</source>
        <translation>Ne mentse</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Octave GUI preferences</source>
        <translation>Az Oktáv grafikus felhasználói felület beállításai</translation>
    </message>
</context>
<context>
    <name>octave::setup_community_news</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="+45"/>
        <source>Community News</source>
        <translation>Közösségi hírek</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Previous</source>
        <translation>Előző</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next</source>
        <translation>Tovább</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;body&gt;
&lt;p&gt;When Octave starts, it will optionally check the Octave web site for current news and information about the Octave community.
The check will happen at most once each day and news will only be displayed if there is something new since the last time you viewed the news.&lt;/p&gt;
&lt;p&gt;You may also view the news by selecting the &quot;Community News&quot; item in the &quot;Help&quot; menu, or by visiting
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt;.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;
&lt;p&gt;Amikor az Oktáv elkezdődik, opcionálisan ellenőrzi az oktáv honlapot, hogy megtalálja-e az Oktáv közösséggel kapcsolatos jelenlegi híreket és információkat.
Naponta legfeljebb egyszer jelentkezik be, és a hírek csak akkor jelennek meg, ha valami új történt a legutóbbi hírnézés óta.&lt;/p&gt;
&lt;p&gt; A híreket megtekintheti a „Közösségi hírek” kiválasztásával a „Súgó” menüben vagy a(z)
&lt;a href=&quot;https://octave.org/community-news.html&quot;&gt;https://octave.org/community-news.html&lt;/a&gt; címen.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;Allow Octave to connect to the Octave web site when it starts in order to display current news and information about the Octave community.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;
&lt;/head&gt;&lt;body&gt;
&lt;p&gt;Lehetővé teszi, hogy az Oktív az induláskor csatlakozzon az Oktív-weboldalhoz, hogy megjelenjen az Oktív-közösséggel kapcsolatos jelenlegi hírek és információk.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>octave::shortcut_edit_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/shortcuts-tree-widget.cc" line="+172"/>
        <source>Enter New Shortcut</source>
        <translation>Új gyorsítóparancs megadása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter custom shortcut
Action: %1</source>
        <translation>Egyéni gyorsítóparancs megadása
Művelet: %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Enter shortcut by typing it</source>
        <translation>Billentyűparancs megadása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add Shift modifier
(allows one to enter number keys)</source>
        <translation>Shift módosító hozzáadása
(lehetővé teszi a számbillentyűk beírását)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Actual Shortcut</source>
        <translation>Jelenlegi gyorsítóparancs</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Default Shortcut</source>
        <translation>Alapértelmezett gyorsítóparancs</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Clear</source>
        <translation>Kiürítés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set to default</source>
        <translation>Alapértelmezettre beállítása</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>Double Shortcut</source>
        <translation>Kettős gyorsítóparancs</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The chosen shortcut
  &quot;%1&quot;
is already used for the action
  &quot;%2&quot;.
Do you want to use the shortcut and remove it from the previous action?</source>
        <translation>A kijelölt gyorsítóparancs
   „%1”
már használatban van a művelethez
   „%2”.
Szeretné használni a gyorsítóparancsot, és eltávolítani az előző műveletből?</translation>
    </message>
</context>
<context>
    <name>octave::shortcuts_tree_widget</name>
    <message>
        <location line="+27"/>
        <source>Global</source>
        <translation>Globális</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+49"/>
        <source>File Menu</source>
        <translation>Fájl menü</translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+49"/>
        <source>Edit Menu</source>
        <translation>Szerkesztés menü</translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+52"/>
        <source>Debug Menu</source>
        <translation>Hibakeresés menü</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Tools Menu</source>
        <translation>Eszközök menü</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Window Menu</source>
        <translation>Ablak menü</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+49"/>
        <source>Help Menu</source>
        <translation>Súgó menü</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>News Menu</source>
        <translation>Hírek menü</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Handling of Dock Widgets</source>
        <translation>Vezérlők rögzítésének kezelése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tab Handling in Dock Widgets</source>
        <translation>Vezérlők rögzítésének lapkezelése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find &amp; Replace in Dock Widgets</source>
        <translation>Keresés és csere a vezérlők rögzítésieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zooming in Editor and Documentation</source>
        <translation>Szerkesztő és dokumentáció nagyítása</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Editor</source>
        <translation>Szerkesztő</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>View Menu</source>
        <translation>Nézet menü</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Run Menu</source>
        <translation>Futtatás menü</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Documentation Viewer</source>
        <translation>Dokumentációmegjelenítő</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Browser</source>
        <translation>Tallózó</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>item %1 not found in shortcut settings dialog</source>
        <translation>%1 elem nem található a parancsbeállítások párbeszédpanelen</translation>
    </message>
</context>
<context>
    <name>octave::terminal_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/terminal-dock-widget.cc" line="+106"/>
        <source>Command Window</source>
        <translation>Parancsablak</translation>
    </message>
</context>
<context>
    <name>octave::variable_dock_widget</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/variable-editor.cc" line="+177"/>
        <source>Dock Widget</source>
        <translation>Vezérlő rögzítése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable Editor: </source>
        <translation>Változószerkesztő: </translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Undock Widget</source>
        <translation>Vezérlő rögzítésének feloldása</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Restore geometry</source>
        <translation>Geometria visszaállítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Redock</source>
        <translation>Ismételt rögzítés</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor</name>
    <message>
        <location line="+922"/>
        <source>Variable Editor</source>
        <translation>Változószerkesztő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit variables.</source>
        <translation>Változók szerkesztése.</translation>
    </message>
    <message>
        <location line="+555"/>
        <source>Variable Editor Toolbar</source>
        <translation>Változószerkesztő eszköztár</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save variable to a file</source>
        <translation>Változó mentése fájlba</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+5"/>
        <location line="+6"/>
        <source>Save in format ...</source>
        <translation>Exportálás másként…</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Save variable to a file in different format</source>
        <translation>Változó exportálása különböző formátumú fájlba</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cut data to clipboard</source>
        <translation>Adatok kivágása a vágólapra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy data to clipboard</source>
        <translation>Adatok másolása a vágólapra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste clipboard into variable data</source>
        <translation>Beillesztés a vágólapot a változó adatokba</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+6"/>
        <location line="+7"/>
        <source>Plot</source>
        <translation>Ábrázolás</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Plot Selected Data</source>
        <translation>Kijelölt adatok ábrázolása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Plot selected data</source>
        <translation>Kijelölt adatok ábrázolása</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Up</source>
        <translation>Felfelé</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go one level up in variable hierarchy</source>
        <translation>Egy szinttel feljebb lépése a változó hierarchiában</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor_stack</name>
    <message>
        <location line="-1265"/>
        <source>Save Variable %1 As</source>
        <translation>„%1” változó mentése másként</translation>
    </message>
</context>
<context>
    <name>octave::variable_editor_view</name>
    <message>
        <location line="+142"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Clear</source>
        <translation>Kiürítés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Variable from Selection</source>
        <translation>Változó a kijelölésből</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transpose</source>
        <translation>Transzponálás</translation>
    </message>
    <message>
        <location line="+56"/>
        <source> columns</source>
        <translation> oszlop</translation>
    </message>
    <message>
        <location line="+0"/>
        <source> column</source>
        <translation> oszlop</translation>
    </message>
    <message>
        <location line="+52"/>
        <source> rows</source>
        <translation> sor</translation>
    </message>
    <message>
        <location line="+0"/>
        <source> row</source>
        <translation> sor</translation>
    </message>
</context>
<context>
    <name>octave::welcome_wizard</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/welcome-wizard.cc" line="-196"/>
        <source>Welcome to GNU Octave</source>
        <translation>Isten hozott a GNU Oktávnál!</translation>
    </message>
</context>
<context>
    <name>octave::workspace_model</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/workspace-model.cc" line="+49"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Class</source>
        <translation>Osztály</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dimension</source>
        <translation>Méret</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation>Érték</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attribute</source>
        <translation>Tulajdonság</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Right click to copy, rename, or display</source>
        <translation>Másolás, átnevezés vagy megjelenítés érdekében kattintson jobb gombbal</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+2"/>
        <source>complex</source>
        <translation>összetett</translation>
    </message>
</context>
<context>
    <name>octave::workspace_view</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-ws.h" line="-21"/>
        <source>Class</source>
        <translation>Osztály</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dimension</source>
        <translation>Méret</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation>Érték</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attribute</source>
        <translation>Tulajdonság</translation>
    </message>
    <message>
        <location filename="__octave_temp_gui_sources__/src/workspace-view.cc" line="+60"/>
        <source>Workspace</source>
        <translation>Munkaterület</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View the variables in the active workspace.</source>
        <translation>Változók megtekintése a jelenlegi munkaterületen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter text to filter the workspace</source>
        <translation>Szöveg megadása a munkaterület szűréséhez</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>View the variables in the active workspace.&lt;br&gt;</source>
        <translation>Változók megtekintése a jelenlegi munkaterületen.&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Colors for variable attributes:</source>
        <translation>Változóattribútumok színei:</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>Open in Variable Editor</source>
        <translation>Változószerkesztő megnyitása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy name</source>
        <translation>Név másolása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy value</source>
        <translation>Érték másolása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Rename</source>
        <translation>Átnevezés</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Only top-level symbols may be renamed</source>
        <translation>Csak a legfelső szintű szimbólumok átnevezhető</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Hide filter</source>
        <translation>Szűrő elrejtése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show filter</source>
        <translation>Szűrő megjelenítése</translation>
    </message>
</context>
<context>
    <name>self_listener</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/self-listener.cc" line="+82"/>
        <source>Can not open redirected stream with fd = %1.</source>
        <translation>Nem sikerült megnyitni az fd = %1 adatfolyam átirányítását.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Can not disable buffering of stream with fd = %1.</source>
        <translation>Nem sikerült letiltani az fd = %1 adatfolyam pufferolását.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Can not dup redirected stream with fd = %1.</source>
        <translation>Nem sikerült másolni az fd = %1 adatfolyam átirányítását.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot create pipe for redirecting stream with fd = %1:</source>
        <translation>Nem sikerült létrehozni az átirányítási csövet az fd = %1 adatfolyamhoz:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot dup2 redirected stream with fd = %1
to pipe with fd = %2: %3</source>
        <translation>Nem sikerült másolni az átirányított fd = %1 adatfolyamot az fd = %2 csőhöz: %3</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Error while listening to redirected streams</source>
        <translation>Hiba történt az átirányított adatfolyamok hallgatása közben</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Error reading from redirected strem fd = %1.</source>
        <translation>Hiba történt az átirányított fd = %1 adatfolyam olvasása közben.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>
Output redirection in </source>
        <translation>
Kimenet átirányítás: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source> won&apos;t work.</source>
        <translation> nem fog működni.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>
Error: </source>
        <translation>
Hiba: </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Octave</source>
        <translation>Oktáv</translation>
    </message>
</context>
<context>
    <name>settings_dialog</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/settings-dialog.ui" line="+45"/>
        <location line="+834"/>
        <location line="+1909"/>
        <source>General</source>
        <translation>Általános</translation>
    </message>
    <message>
        <location line="-2390"/>
        <source>Octave logo only</source>
        <translation>Kizárólag oktáv embléma</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Letter icons</source>
        <translation>Betű ikonok</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>Dock widget title bar</source>
        <translation>Vezérlő rögzítőcímsora</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Small</source>
        <translation>Kicsi</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Large</source>
        <translation>Nagy</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Custom style</source>
        <translation>Egyéni stílus</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>3D</source>
        <translation>3D</translation>
    </message>
    <message>
        <location line="+558"/>
        <source>Editor</source>
        <translation>Szerkesztő</translation>
    </message>
    <message>
        <location line="+488"/>
        <location line="+140"/>
        <source>This works well for monospaced fonts. The line is drawn at a position based on the width of a space character in the default font. It may not work very well if styles use proportional fonts or if varied font sizes or bold, italic and normal texts are used.</source>
        <translation>Ez jól működik rögzített szélességű betűkészleteknél. A vonalat az alapértelmezett betűkészlet szóköz karaktereinek szélessége alapján kell megrajzolni. Előfordulhat, hogy nem működik jól, ha a stílusok arányos betűkészleteket használnak, vagy ha különböző betűméreteket vagy vastag, dőlt és normál szövegeket használnak.</translation>
    </message>
    <message>
        <location line="-535"/>
        <source>Enable Code Folding</source>
        <translation>Kódrészletek bezárásai engedélyezése</translation>
    </message>
    <message>
        <location line="+1059"/>
        <source>Windows (CRLF)</source>
        <translation>Windows (CR LF karakterpár)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Legacy Mac (CR)</source>
        <translation>Örökölt Mac (CR karakter)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unix (LF)</source>
        <translation>Unix (LF karakter)</translation>
    </message>
    <message>
        <location line="-1034"/>
        <source>Show horizontal scroll bar</source>
        <translation>Vízszintes görgetősáv megjelenítése</translation>
    </message>
    <message>
        <location line="-509"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If set, the focus of the widgets that are docked to the main window follows the mouse cursor. This is intended for having the same behavior within the main window when &amp;quot;focus follows mouse&amp;quot; is used for the desktop environment.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ha be van állítva, a főablakhoz rögzített vezérlők fókusza az egérkurzort követi. Ennek célja ugyanaz a viselkedés a főablakon belül, amikor az asztali környezetben a „Fókusz követi az egeret” funkciót használjuk.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Focus follows mouse for widgets docked to the main window</source>
        <translation>Fókusz követi az egeret a vezérlők rögzítve a főablakba</translation>
    </message>
    <message>
        <location line="+487"/>
        <source>Show tool bar</source>
        <translation>Eszköztár megjelenítése</translation>
    </message>
    <message>
        <location line="+221"/>
        <source>Rotated tabs</source>
        <translation>Elforgatott lapok</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Position</source>
        <translation>Szövegbeosztás</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Max. tab width in chars (0: no limit)</source>
        <translation>A tabulátor legfeljebb szélessége karakterekben (0: korlátlan)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Max. width of a tab in characters (average char. width). Especially useful for rotated tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;A tabulátor legfeljebb szélessége karakterekben (átlagos karakterszélesség). Különösen hasznos elforgatott lapok esetén.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+260"/>
        <source>Wrap long lines at current window border</source>
        <translation>Tekerjen hosszú sorokat a jelenlegi ablak határán</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Indentation</source>
        <translation>Behúzás</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Indent width</source>
        <translation>Behúzás szélessége</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tab indents line</source>
        <translation>Tabulátor behúzási segédvonala</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Auto indentation</source>
        <translation>Automatikus behúzás</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tab width</source>
        <translation>Tabulátor szélessége</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show indentation guides</source>
        <translation>Behúzási segédvonalainak megjelenítése</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Backspace unindents line</source>
        <translation>Visszatörlés (backspace) eltávolítja a sor behúzását</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Indentation uses tabs</source>
        <translation>Behúzás tabulátorokat használ</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Auto completion</source>
        <translation>Automatikus kiegészítés</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Match keywords</source>
        <translation>Kulcsszavak egyeztetése</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Case sensitive</source>
        <translation>Kis- és nagybetűk megkülönböztetése</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Replace word by suggested one</source>
        <translation>Helyettesítse a szót a javasolt szöveggel</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Match words in document</source>
        <translation>Párosítsa a szavakat a dokumentumban</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>With Octave builtins</source>
        <translation>Oktáv beépített elemekkel</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>With Octave functions</source>
        <translation>Oktáv függvényekkel</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Show completion list automatically</source>
        <translation>Befejezési lista automatikus megjelenítése</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Reload externally changed files without prompt</source>
        <translation>A külsőleg módosított fájlok újratöltése felszólítás nélkül</translation>
    </message>
    <message>
        <location line="-841"/>
        <source>Use custom file editor</source>
        <translation>Egyéni fájlszerkesztő használata</translation>
    </message>
    <message>
        <location line="+934"/>
        <source>Text encoding used for loading and saving</source>
        <translation>Betöltéshez és mentéshez használt szövegkódolás</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Editor Styles</source>
        <translation>Szerkesztő stílusai</translation>
    </message>
    <message>
        <location line="-1383"/>
        <source>(Changing buffer size clears history)</source>
        <translation>(A puffer méretének megváltoztatása törli az előzményeket)</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>History buffer Size</source>
        <translation>Előzménypuffer mérete</translation>
    </message>
    <message>
        <location line="-123"/>
        <location line="+1863"/>
        <source>Font</source>
        <translation>Betűkészlet</translation>
    </message>
    <message>
        <location line="-1573"/>
        <source>Show line numbers</source>
        <translation>Sorok számának megjelenítése</translation>
    </message>
    <message>
        <location line="-859"/>
        <source>Interface</source>
        <translation>Felület</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Confirm before exiting</source>
        <translation>Megerősít kilépés előtt</translation>
    </message>
    <message>
        <location line="-72"/>
        <source>Graphic icons</source>
        <translation>Képikonok</translation>
    </message>
    <message>
        <location line="+52"/>
        <location line="+439"/>
        <source>Show status bar</source>
        <translation>Állapotsor megjelenítése</translation>
    </message>
    <message>
        <location line="-677"/>
        <source>Text inactive</source>
        <translation>Tétlen szöveg</translation>
    </message>
    <message>
        <location line="-32"/>
        <location line="+45"/>
        <source>Active</source>
        <translation>Működő</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Background inactive</source>
        <translation>Tétlen háttér</translation>
    </message>
    <message>
        <location line="+218"/>
        <source>Octave Startup</source>
        <translation>Oktáv indítás</translation>
    </message>
    <message>
        <location line="+46"/>
        <location line="+1691"/>
        <source>Browse</source>
        <translation>Tallózás</translation>
    </message>
    <message>
        <location line="-1298"/>
        <source>Show whitespace</source>
        <translation>Elválasztó karakterek megjelenítése</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Do not show whitespace used for indentation</source>
        <translation>Ne jelenjen meg elválasztó karakterek használt behúzáshoz</translation>
    </message>
    <message>
        <location line="+1085"/>
        <source>Create nonexistent files without prompting</source>
        <translation>Hozzon létre nem létező fájlokat kérés nélkül</translation>
    </message>
    <message>
        <location line="-857"/>
        <source>command line (%f=file, %l=line):</source>
        <translation>Parancssor (%f=fájl, %l=sor):</translation>
    </message>
    <message>
        <location line="-500"/>
        <source>Cursor type:</source>
        <translation>Jelölőtípus:</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Use foreground color</source>
        <translation>Előtér színe használata</translation>
    </message>
    <message>
        <location line="+52"/>
        <location line="+1753"/>
        <source>Font size</source>
        <translation>Betűméret</translation>
    </message>
    <message>
        <location line="-245"/>
        <source>File Browser</source>
        <translation>Fájltallózó</translation>
    </message>
    <message>
        <location line="-2017"/>
        <source>Normal</source>
        <translation>Normál</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>These preferences are applied after any .octaverc startup files.</source>
        <translation>Ezeket a beállításokat minden .octaverc indítási fájl után alkalmazza.</translation>
    </message>
    <message>
        <location line="+540"/>
        <source>Show EOL characters</source>
        <translation>Sorvég (EOL) karakterek megjelenítése</translation>
    </message>
    <message>
        <location line="+945"/>
        <source>Default EOL mode</source>
        <translation>Alapértelmezett sorvég (EOL) üzemmód</translation>
    </message>
    <message>
        <location line="-162"/>
        <source>Number of characters before list is shown: </source>
        <translation>Karakterek száma a lista megjelenítése előtt: </translation>
    </message>
    <message>
        <location line="-1805"/>
        <source>Preferences</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location line="+351"/>
        <source>(requires restart)</source>
        <translation>(újraindítás szükséges)</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Use native file dialogs</source>
        <translation>Rendszer fájl párbeszédpanelek használata</translation>
    </message>
    <message>
        <location line="-98"/>
        <source>Toolbar Icons</source>
        <translation>Eszköztár ikonok</translation>
    </message>
    <message>
        <location line="-224"/>
        <source>Language</source>
        <translation>Nyelv</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>Dock widgets window icons</source>
        <translation>Vezérlő rögzítésének ablakikonok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Style</source>
        <translation>Stílus</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Icon theme (requires restart)</source>
        <translation>Ikon téma (újraindítás szükséges)</translation>
    </message>
    <message>
        <location line="+323"/>
        <source>Blinking cursor</source>
        <translation>Villogó jelölő</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Initial working directory of Octave interpreter</source>
        <translation>Oktáv-tolmács kezdeti munkakönyvtára</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Restore last working directory of previous session</source>
        <translation>Előző munkamenet utolsó munkakönyvtárának visszaállítása</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Command</source>
        <translation>Parancs</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Set focus to Command Window when running a command from within another widget</source>
        <translation>Fókusz beállítása a parancsablakhoz, amikor parancs futtatása egy másik vezérlőből</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Print debug location in Command Window in addition to the marker in the editor</source>
        <translation>Hibakeresési hely kinyomtatása a parancsablakban a szerkesztőben lévő jelölő mellett</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Command Window Colors</source>
        <translation>Parancsablakok színei</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>Number size as difference to editor font</source>
        <translation>A szám mérete a szerkesztő betűtípusától való eltérés</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Highlight current line (color adjustable below with editor styles)</source>
        <translation>Jelenlegi sor kiemelése (a szín alul állítható szerkesztőstílusokkal)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Highlight all occurrences of a word selected by a double click</source>
        <translation>Dupla kattintással jelölje ki a szó összes előfordulását</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Tabs</source>
        <translation>Lapok</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Rotate tabs: Vertical when at top or bottom and horizontal when left or right. The close button is not shown in rotated tabs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lapok elforgatása: Függőleges, ha felül vagy alul van, és vízszintes, ha balra vagy jobbra van. A bezárás gomb nem jelenik meg az elforgatott lapokon.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Comments (Octave)</source>
        <translation>Megjegyzések (Oktáv)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Strings considered for uncommenting text</source>
        <translation>A megjegyzés nélküli szöveg karakterláncai</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>String used for commenting selected text</source>
        <translation>A kiválasztott szöveg megjegyzéséhez használt karakterlánc</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Long lines</source>
        <translation>Sorhosszúság</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Break long lines at line length</source>
        <translation>Sor hasítása sorhosszon</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Break lines only in comments</source>
        <translation>Sor hasítása csak megjegyzésekben</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Line length</source>
        <translation>Sorhossz</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Long line marker</source>
        <translation>Hosszú vonal jelölő</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Line</source>
        <translation>Sor</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Background</source>
        <translation>Háttér</translation>
    </message>
    <message>
        <location line="+191"/>
        <source>Auto insert after &quot;if&quot; etc.</source>
        <translation>Automatikus beszúrás az „if” (ha) után stb.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Nothing</source>
        <translation>Semmi</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&quot;endif&quot; etc.</source>
        <translation>„endif” (végeha) stb.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&quot;end&quot;</source>
        <translation>„end” (vége)</translation>
    </message>
    <message>
        <location line="+238"/>
        <source>Debugging</source>
        <translation>Hibakeresés</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Always show debug breakpoints and pointers (opens related file if closed)</source>
        <translation>Mindig jelenítse meg a hibakeresési töréspontokat és mutatókat (bezárva megnyitja a kapcsolódó fájlt)</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>File handling</source>
        <translation>Fájlkezelés</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Restore editor tabs from previous session on startup or when editor is shown again after closing</source>
        <translation>A szerkesztő lapok visszaállítása az előző munkamenetből indításkor, vagy amikor a szerkesztő újra megjelenik a bezárás után</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Close all files when the editor widget is closed/hidden</source>
        <translation>Minden fájl bezárása, amikor a szerkesztővezérlő bezárásakor/elrejtésekor</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Force newline at end when saving file</source>
        <translation>Újsor kényszerítése a végére a fájl mentésekor</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove trailing spaces when saving file</source>
        <translation>A fájl mentésekor távolítsa el a záró szóközöket</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select font, font size (as a difference from the default size), font style (&lt;b&gt;b&lt;/b&gt;old, &lt;b&gt;i&lt;/b&gt;talic, &lt;b&gt;u&lt;/b&gt;nderline), text color, and background color (for the latter, the color magenta (255,0,255) is a placeholder for the default background color).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Betűkészlet, betűméret (eltérés az alapértelmezett mérettől), betűstílus kijelölése (&lt;b&gt;f&lt;/b&gt;élkövér, &lt;b&gt;d&lt;/b&gt;őlt, &lt;b&gt;a&lt;/b&gt;láhúzott), szövegszín, and háttérszín (utóbbi esetében a bíbor szín (255,0,255) az alapértelmezett háttérszín helyőrzője).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Behavior</source>
        <translation>Viselkedés</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Restore last directory of previous session</source>
        <translation>Előző munkamenet utolsó könyvtárának visszaállítása</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Extensions of files to be opened in the default text editor (separated by &quot;;&quot;):</source>
        <translation>Alapértelmezett szövegszerkesztőben megnyitandó fájlkiterjesztések („;” elválasztva):</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Workspace</source>
        <translation>Munkaterület</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Colors for variable attributes</source>
        <translation>Változóattribútumok színei</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Use Command Window font</source>
        <translation>Parancsablak betűkészlet használata</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>Import shortcut set</source>
        <translation>Gyorsítóparancskészlet importálása</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Export current shortcut set</source>
        <translation>Jelenlegi gyorsítóparancskészlet exportálása</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reset shortcuts to their defaults</source>
        <translation>Gyorsítóparancsok visszaállása az alapértelmezett értékekre</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+80"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+232"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ok&lt;/span&gt; - close dialog and apply settings&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Apply - &lt;/span&gt;apply settings but leave dialog open&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Cancel - &lt;/span&gt;close dialog and discard changes not yet applied&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Reset -&lt;/span&gt; reload settings discarding changes not yet applied&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Rendben&lt;/span&gt; - párbeszédpanel bezárása és a beállítások alkalmazása&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Alkalmazás - &lt;/span&gt;beállítások alkalmazása és a párbeszédpanel nyitva hagyása&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Mégse&lt;/span&gt; - párbeszédpanel bezárása és a még nem alkalmazott módosítások elvetése&lt;br&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Visszaállítás&lt;/span&gt; - beállítások újratöltése a még nem alkalmazott módosítások elvetése&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="-227"/>
        <source>Actual</source>
        <translation>Tényleges</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Network</source>
        <translation>Hálózat</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Allow Octave to connect to the Octave web site to display current news and information</source>
        <translation>Engedélyezze az Oktávnak, hogy csatlakozzon az Oktáv honlaphoz, hogy megjelenítse a jelenlegi híreket és információkat</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Proxy Server</source>
        <translation>Proxykiszolgáló</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select &lt;span style=&quot; font-style:italic;&quot;&gt;HttpProxy&lt;/span&gt;, &lt;span style=&quot; font-style:italic;&quot;&gt;Sock5Proxy&lt;/span&gt; or &lt;span style=&quot; font-style:italic;&quot;&gt;Environment Variables&lt;/span&gt;. With the last selection, the proxy is taken from the first non-empty environment variable ALL_PROXY, HTTP_PROXY or HTTPS_PROXY .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Válassza ki &lt;span style=&quot; font-style:italic;&quot;&gt;HTTP-proxy&lt;/span&gt;, &lt;span style=&quot; font-style:italic;&quot;&gt;Sock5Proxy&lt;/span&gt; vagy &lt;span style=&quot; font-style:italic;&quot;&gt;Környezeti változók&lt;/span&gt;. Az utolsó kiválasztással a proxy az első nem üres környezeti változóból (ALL_PROXY, HTTP_PROXY vagy HTTPS_PROXY) kerül átvételre.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Use proxy server</source>
        <translation>Proxykiszolgáló használata</translation>
    </message>
    <message>
        <location line="-523"/>
        <source>Variable Editor</source>
        <translation>Változószerkesztő</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Default row height</source>
        <translation>Alapértelmezett sormagasság</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Default column width</source>
        <translation>Alapértelmezett oszlopszélesség</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Variable Editor Colors</source>
        <translation>Változószerkesztő színek</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Use alternating row colors</source>
        <translation>Változó sorszínek használata</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Disable global shortcuts in order to prevent
interference with readline key strokes.
Exceptions: Ctrl-C for interrupting the interpreter
and the shortcuts for switching to other widgets.</source>
        <translation>Globális billentyűparancsok letiltása a
ReadLine billentyűleütésekkel való zavar elkerülése érdekében.
Kivételek: Ctrl-C az értelmező megszakítására és a
billentyűparancsok más vezérlőkre való váltásra.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Disable global shortcuts when Command Window has focus</source>
        <translation>Globális gyorsítóparancsok letiltása, ha a Parancsablak fókuszba kerül</translation>
    </message>
    <message>
        <location line="-392"/>
        <source>Synchronize Octave working directory with file browser</source>
        <translation>Oktáv munkakönyvtár összehangolása a fájltallózóval</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Initial file browser directory (only if not synchronized with initial working directory of Octave)</source>
        <translation>Kezdeti fájltallózókönyvtár (csak ha nincs összehangolva az Oktáv kezdeti munkakönyvtárával)</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>Shortcuts</source>
        <translation>Gyorsítóparancs</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Disable menu accelerators in order to prevent
interference with readline key strokes.</source>
        <translation>Globális billentyűparancsok letiltása a
ReadLine billentyűleütésekkel való zavar elkerülése érdekében.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Disable menu accelerators of main window menus when Command Window has focus</source>
        <translation>A főablak menüinek menügyorsítóinak letiltása, ha a parancsablak fókuszban van</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Export</source>
        <translation>Exportálás</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Import</source>
        <translation>Importálás</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Edit a shortcut by double-clicking in Actual column</source>
        <translation>Parancsikon szerkesztése dupla kattintással a Tényleges oszlopban</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Action</source>
        <translation>Művelet</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Hostname:</source>
        <translation>Állomásnév:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Proxy type:</source>
        <translation>Proxy típusa:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Port:</source>
        <translation>Kikötő:</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Username:</source>
        <translation>Felhasználónév:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Password:</source>
        <translation>Jelszó:</translation>
    </message>
</context>
<context>
    <name>shortcuts</name>
    <message>
        <location filename="__octave_temp_gui_sources__/src/gui-preferences-sc.cc" line="+227"/>
        <source>Undock/Dock Widget</source>
        <translation>Vezérlő rögzítése/rögzítésének feloldása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close Widget</source>
        <translation>Vezérlő bezárása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New File</source>
        <translation>Új fájl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Function</source>
        <translation>Új függvény</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Figure</source>
        <translation>Új ábra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open File</source>
        <translation>Fájl megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Load Workspace</source>
        <translation>Munkaterület megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save Workspace As</source>
        <translation>Munkaterület mentése másként</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exit Octave</source>
        <translation>Kilépés az Oktávból</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Undo</source>
        <translation>Visszavonás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Clipboard</source>
        <translation>Vágólap kiürítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find in Files</source>
        <translation>Keresés a fájlokban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Command Window</source>
        <translation>Parancsablak kiürítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Command History</source>
        <translation>Parancselőzmények kiürítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Workspace</source>
        <translation>Munkaterület kiürítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set Path</source>
        <translation>Elérési útvonal beállítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+119"/>
        <source>Preferences</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location line="-116"/>
        <source>Step</source>
        <translation>Lépés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Step In</source>
        <translation>Előrelépés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Step Out</source>
        <translation>Visszalépés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Continue</source>
        <translation>Folytatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit Debug Mode</source>
        <translation>Hibakeresési mód kilépése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start/Stop Profiler Session</source>
        <translation>Profilkészítő munkamenet indítása/leállítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resume Profiler Session</source>
        <translation>Profilkészítő munkamenet folytatása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Profile Data</source>
        <translation>Profiladatok megjelenítése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show Command Window</source>
        <translation>Parancsablak megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Command History</source>
        <translation>Parancselőzmények megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show File Browser</source>
        <translation>Fájltallózó megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Workspace</source>
        <translation>Munkaterület megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Editor</source>
        <translation>Szerkesztő megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Documentation</source>
        <translation>Dokumentáció megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Variable Editor</source>
        <translation>Változószerkesztő megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command Window</source>
        <translation>Parancsablak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command History</source>
        <translation>Parancselőzmények</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File Browser</source>
        <translation>Fájltallózó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Workspace</source>
        <translation>Munkaterület</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Editor</source>
        <translation>Szerkesztő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Documentation</source>
        <translation>Dokumentáció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Variable Editor</source>
        <translation>Változószerkesztő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Widget</source>
        <translation>Előző vezérlő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset Default Window Layout</source>
        <translation>Alapértelmezett ablakelrendezés visszaállítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show On-disk Documentation</source>
        <translation>Lemezen lévő dokumentáció megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Online Documentation</source>
        <translation>Online dokumentáció megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Report Bug</source>
        <translation>Hibabejelentés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Octave Packages</source>
        <translation>Oktáv-csomagok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contribute to Octave</source>
        <translation>Oktáv hozzájárulása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Octave Developer Resources</source>
        <translation>Oktáv fejlesztői erőforrások</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About Octave</source>
        <translation>Az Oktáv névjegye</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Release Notes</source>
        <translation>Kiadási megjegyzések</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Community News</source>
        <translation>Közösségi hírek</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Close Tab</source>
        <translation>Lap bezárása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close All Tabs</source>
        <translation>Az összes lap bezárása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close Other Tabs</source>
        <translation>Más lapok bezárása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch to Left Tab</source>
        <translation>Váltás a bal oldali lapra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch to Right Tab</source>
        <translation>Váltás a jobb oldali lapra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Tab Left</source>
        <translation>Lap mozgassa balra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move Tab Right</source>
        <translation>Lap mozgassa jobbra</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Zoom In</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom Out</source>
        <translation>Kicsinyítés</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Zoom Normal</source>
        <translation>Tényleges méret</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Edit Function</source>
        <translation>Függvény szerkesztése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save File</source>
        <translation>Fájl mentése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save File As</source>
        <translation>Fájl mentése másként</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Redo</source>
        <translation>Ismét</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find and Replace</source>
        <translation>Keresés és csere</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Next</source>
        <translation>Következő találat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find Previous</source>
        <translation>Előző találat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to Start of Word</source>
        <translation>Törlés a szó elejéig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to End of Word</source>
        <translation>Törlés a szó végéig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to Start of Line</source>
        <translation>Törlés a sor elejéig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete to End of Line</source>
        <translation>Törlés a sor végéig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete Line</source>
        <translation>Sor törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Line</source>
        <translation>Sor másolása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut Line</source>
        <translation>Sor kivágása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Duplicate Selection/Line</source>
        <translation>Kijelölés/sor megkettőzése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transpose Line</source>
        <translation>Sor transzponálása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Completion List</source>
        <translation>Befejezési lista megjelenítése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment Selection</source>
        <translation>Megjegyzés hozzáfűzése a kijelölt részhez</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uncomment Selection</source>
        <translation>Kijelölt rész megjegyzésének törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment Selection (Choosing String)</source>
        <translation>Megjegyzés hozzáfűzése a kijelölt részhez (karakterlánc kijelölése)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uppercase Selection</source>
        <translation>Nagybetűs kijelölése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Lowercase Selection</source>
        <translation>Kisbetűs kijelölése</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Indent Selection Rigidly</source>
        <translation>Kijelölés behúzásának növekedése rögzítettként</translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+3"/>
        <source>Unindent Selection Rigidly</source>
        <translation>Kijelölés behúzásának csökkentése rögzítettként</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Indent Code</source>
        <translation>Kód behúzása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Convert Line Endings to Windows</source>
        <translation>Sorvégek átalakítása Windowsra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Convert Line Endings to Unix</source>
        <translation>Sorvégek átalakítása Unix-ra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Convert Line Endings to Mac</source>
        <translation>Sorvégek átalakítása Mac-re</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Goto Line</source>
        <translation>Ugrás sorra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to Matching Brace</source>
        <translation>Ugrás az egyező zárójelre</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select to Matching Brace</source>
        <translation>Egyező zárójel kijelölése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle Bookmark</source>
        <translation>Könyvjelző kapcsolása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Bookmark</source>
        <translation>Következő könyvjelző</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Bookmark</source>
        <translation>Előző könyvjelző</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove All Bookmark</source>
        <translation>Összes könyvjelző eltávolítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Styles Preferences</source>
        <translation>Stílusok beállításai</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Line Numbers</source>
        <translation>Sorok számának megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Whitespace Characters</source>
        <translation>Elválasztó karakterek (szóköz, sortörés vagy tabulátor) megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Line Endings</source>
        <translation>Sorvégek megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Indentation Guides</source>
        <translation>Behúzási segédvonalainak megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Long Line Marker</source>
        <translation>Hosszú vonal jelölő megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Toolbar</source>
        <translation>Eszköztár megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Statusbar</source>
        <translation>Állapotsor megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Horizontal Scrollbar</source>
        <translation>Vízszintes görgetősáv megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort Tabs Alphabetically</source>
        <translation>Lapok rendezése betűrendben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Toggle Breakpoint</source>
        <translation>Töréspont kapcsolása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Breakpoint</source>
        <translation>Következő töréspont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Breakpoint</source>
        <translation>Előző töréspont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove All Breakpoints</source>
        <translation>Összes töréspont eltávolítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run File</source>
        <translation>Fájl futtatása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Selection</source>
        <translation>Kijelölés futtatása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Tests</source>
        <translation>Tesztek futtatása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Demos</source>
        <translation>Bemutatók futtatása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help on Keyword</source>
        <translation>Kulcsszó súgóban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document on Keyword</source>
        <translation>Kulcsszó a dokumentációban</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Go to Homepage</source>
        <translation>Ugrás a kezdőlapra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go Back one Page</source>
        <translation>Vissza egy lappal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go Forward one Page</source>
        <translation>Előre egy lappal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bookmark this Page</source>
        <translation>Oldal megjelölése könyvjelzővel</translation>
    </message>
</context>
</TS>
